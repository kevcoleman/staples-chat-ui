//
//  BOTChatActionCollectionViewTest.m
//  Staples
//
//  Created by Reid Weber on 1/23/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Expecta/Expecta.h>

#import "BOTChatActionCollectionView.h"

@interface BOTChatActionCollectionView ();

@property (nonatomic) NSArray *selectionTitles;

@end

@interface BOTChatActionCollectionViewTest : XCTestCase

@property (nonatomic) NSArray *selectionTitles;

@end

@implementation BOTChatActionCollectionViewTest

- (void)testSelectionTitleInit
{
    BOTChatActionCollectionView *collection = [BOTChatActionCollectionView chatActionCollectionViewWithTitles:@[ BOTActionScanSupplyList, BOTActionRewardsSummary, BOTActionScanSupplyList ]];
    
    expect(collection.selectionTitles).toNot.beNil();
    for (NSString *title in collection.selectionTitles) {
        BOOL isAcceptedTitle = NO;
        if ([title isEqualToString:BOTActionTrackShipment] ||
            [title isEqualToString:BOTActionRewardsSummary] ||
            [title isEqualToString:BOTActionScanSupplyList]) {
            isAcceptedTitle = YES;
        }
        expect(isAcceptedTitle).to.beTruthy();
    }
}

- (void)testSelectionTitleChange
{
    BOTChatActionCollectionView *collection = [BOTChatActionCollectionView chatActionCollectionViewWithTitles:@[]];
    [collection updateWithSelectionTitle:@[ BOTActionScanSupplyList, BOTActionRewardsSummary,BOTActionScanSupplyList ]];
    
    expect(collection.selectionTitles).toNot.beNil();
    for (NSString *title in collection.selectionTitles) {
        BOOL isAcceptedTitle = NO;
        if ([title isEqualToString:BOTActionTrackShipment] ||
            [title isEqualToString:BOTActionRewardsSummary] ||
            [title isEqualToString:BOTActionScanSupplyList]) {
            isAcceptedTitle = YES;
        }
        expect(isAcceptedTitle).to.beTruthy();
    }
}

@end
