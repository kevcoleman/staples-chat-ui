//
//  BOTLayerManagerTest.m
//  Staples
//
//  Created by Kevin Coleman on 12/26/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BOTLayerManager.h"
#import <Expecta/Expecta.h>

@interface BOTLayerManagerTest : XCTestCase

@property (nonatomic) BOTLayerManager *manager;
@property (nonatomic) NSString *conversationID;

@end

@implementation BOTLayerManagerTest

- (void)setUp
{
    [super setUp];
    [Expecta setAsynchronousTestTimeout:10];

    NSURL *appID = [NSURL URLWithString:@"layer:///apps/staging/3b2b723a-e1ce-11e5-a7a8-23ab7c095bad"];
    self.manager = [BOTLayerManager managerWithAppID:appID];
    
    NSArray *participants = @[@"test", @"test2"];
    
    __block BOOL created = NO;
    [self.manager createConversationWithParticipants:participants completion:^(NSDictionary *data, NSError *error) {
        self.conversationID = [data[@"id"] stringByReplacingOccurrencesOfString:@"layer:///conversations/" withString:@""];
        created = YES;
    }];
    expect(created).will.beTruthy();
}

- (void)testCreatingConversation
{
    NSArray *participants = @[@"test", @"test2"];
    
    __block BOOL created = NO;
    [self.manager createConversationWithParticipants:participants completion:^(NSDictionary *data, NSError *error) {
        expect(error).to.beNil();
        created = YES;
    }];
    expect(created).will.beTruthy();
}

- (void)testSendingMessage
{
    __block BOOL sent = NO;
    [self.manager sendMessageText:@"test" toConversation:self.conversationID asSender:@"test" completion:^(NSDictionary *data, NSError *error) {
        expect(error).to.beNil();
        sent = YES;
    }];
    expect(sent).will.beTruthy();
}

- (void)testSendingMultiPartMessage
{
    __block BOOL sent = NO;
    NSArray *parts = @[@{@"body": @"test", @"mime_type": @"application/json"}];
    [self.manager sendMessageParts:parts toConversation:self.conversationID asSender:@"test" completion:^(NSDictionary *data, NSError *error) {
        expect(error).to.beNil();
        sent = YES;
    }];
    expect(sent).will.beTruthy();
}

@end
