//
//  BOTOnboardingManagerTest.m
//  Staples
//
//  Created by Kevin Coleman on 12/26/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BOTOnboardingManager.h"
#import "BOTLayerManager.h"
#import <Expecta/Expecta.h>

@interface BOTOnboardingManagerTest : XCTestCase

@property (nonatomic) BOTOnboardingManager *manager;
@property (nonatomic) BOTLayerManager *layerManager;
@property (nonatomic) NSString *conversationID;

@end

@implementation BOTOnboardingManagerTest

- (void)setUp
{
    [super setUp];
    [Expecta setAsynchronousTestTimeout:10];

    NSURL *appID = [NSURL URLWithString:@"layer:///apps/7b1d70d4-704c-11e6-8cc3-181c7d0c5222"];
    self.layerManager = [BOTLayerManager managerWithAppID:appID];
    
    NSArray *participants = @[@"test", @"test2"];
    
    __block BOOL created = NO;
    [self.layerManager createConversationWithParticipants:participants completion:^(NSDictionary *data, NSError *error) {
        self.conversationID = data[@"id"];
        created = YES;
    }];
    expect(created).will.beTruthy();
    
    NSURL *conversationIDURL = [NSURL URLWithString:self.conversationID];
    self.manager = [BOTOnboardingManager managerWithLayerManager:self.layerManager conversationID:conversationIDURL];
}

- (void)testExecutingOnboardingPhaseWelcome
{
    __block BOOL created = NO;
    [self.manager executeOnboardingPhase:0 completion:^(BOOL success, NSError *error) {
        created = YES;
    }];
    expect(created).will.beTruthy();
}
     
@end
