//
//  BOTChatTest.m
//  Staples
//
//  Created by Kevin Coleman on 12/6/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BOTChat.h"
#import <Expecta/Expecta.h>
#import "BOTPromiseManager.h"

extern NSString *const BOTEasyServiceConfigurationKey;

@interface BOTChatTest : XCTestCase

@end

@implementation BOTChatTest

- (void)setUp
{
    [super setUp];
    [self resetState];
}

- (void)testUpdatingEasySystemEnvironment
{
    XCTestExpectation *updateENV = [self expectationWithDescription:@"Should update ENV"];
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        expect(error).beNil();
        expect([BOTChat sharedInstance].easySystemBaseURL).toNot.beNil();
        expect([BOTChat sharedInstance].serviceManager.tenantID).toNot.beNil();
        expect([BOTChat sharedInstance].serviceManager.apiKey).toNot.beNil();
        [updateENV fulfill];
    }];
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}

/**
 * Conversation Logic
 */

- (void)testUpdatingLoggedInUserDetailsAfterLoggingInAndExpectSameConversation
{
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentLocal completion:^(NSError *error) {
        expect(error).beNil();
        NSString *userID1 = @"Tay@gmail.com";
        NSString *wcToken1 = @"1234";
        NSString *wcTrustedToken1 = @"543321";
        NSString *zipCode1 = @"98101";
        NSString *storeID1 = @"1001";
        NSString *catalog1 = @"catalog1";
        NSString *locale1 = @"local1";
        
        NSString *userID2 = @"Tay2@gmail.com";
        NSString *wcToken2 = @"12345";
        NSString *wcTrustedToken2 = @"9999";
        NSString *zipCode2 = @"90888";
        NSString *storeID2 = @"1";
        NSString *catalog2 = @"catalog99";
        NSString *locale2 = @"local99";
        
        __block NSString *firstConversation = nil;
        
        XCTestExpectation *userDetailsShift = [self expectationWithDescription:@"Should update ENV"];
        [[BOTChat sharedInstance] connectWithUserID:userID1
                                            wcToken:wcToken1
                                     wcTrustedToken:wcTrustedToken1
                                            zipCode:zipCode1
                                            storeID:storeID1
                                          catalogID:catalog1
                                             locale:locale1
                                         completion:^(NSError *error) {
                                             expect(error).beNil();
                                             NSLog(@"UserID: %@", [BOTChat sharedInstance].layerClient.authenticatedUser.userID);
                                             NSLog(@"ConversationID: %@", [BOTChat sharedInstance].conversation.identifier);
                                             expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                             expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                             expect([BOTChat sharedInstance].userID).will.equal(userID1);
                                             expect([BOTChat sharedInstance].wcToken).will.equal(wcToken1);
                                             expect([BOTChat sharedInstance].wcTrustedToken).will.equal(wcTrustedToken1);
                                             expect([BOTChat sharedInstance].zipCode).will.equal(zipCode1);
                                             expect([BOTChat sharedInstance].storeID).will.equal(storeID1);
                                             expect([BOTChat sharedInstance].catalogID).will.equal(catalog1);
                                             expect([BOTChat sharedInstance].locale).will.equal(locale1);
                                             firstConversation = [[BOTChat sharedInstance].conversation.identifier absoluteString];
                                             
                                             [[BOTChat sharedInstance] connectWithUserID:userID2
                                                                                 wcToken:wcToken2
                                                                          wcTrustedToken:wcTrustedToken2
                                                                                 zipCode:zipCode2
                                                                                 storeID:storeID2
                                                                               catalogID:catalog2
                                                                                  locale:locale2
                                                                              completion:^(NSError *error) {
                                                                                  expect(error).beNil();
                                                                                  expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                                                                  expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                                                                  expect([BOTChat sharedInstance].userID).will.equal(userID2);
                                                                                  expect([BOTChat sharedInstance].wcToken).will.equal(wcToken2);
                                                                                  expect([BOTChat sharedInstance].wcTrustedToken).will.equal(wcTrustedToken2);
                                                                                  expect([BOTChat sharedInstance].zipCode).will.equal(zipCode2);
                                                                                  expect([BOTChat sharedInstance].storeID).will.equal(storeID2);
                                                                                  expect([BOTChat sharedInstance].catalogID).will.equal(catalog2);
                                                                                  expect([BOTChat sharedInstance].locale).will.equal(locale2);
                                                                                  
                                                                                  // Must have different conversation IDs
                                                                                  NSString *newConversationID = [[BOTChat sharedInstance].conversation.identifier absoluteString];
                                                                                  expect(firstConversation).willNot.beNil();
                                                                                  XCTAssertTrue([firstConversation isEqualToString:newConversationID]);
                                                                                  [userDetailsShift fulfill];
                                                                              }];
                                         }];
    }];
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}

- (void)testGoingFromNonAuthUserToAuthUserKeepsConversationID
{
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        expect(error).beNil();
        
        NSString *userID2 = @"T$";
        NSString *wcToken2 = @"12345";
        NSString *wcTrustedToken2 = @"9999";
        NSString *zipCode2 = @"90888";
        NSString *storeID2 = @"1";
        NSString *catalog2 = @"catalog99";
        NSString *locale2 = @"local99";
        
        __block NSString *firstConversation = nil;
        
        XCTestExpectation *userDetailsShift = [self expectationWithDescription:@"Should update ENV"];
        [[BOTChat sharedInstance] connectWithUserID:nil
                                            wcToken:nil
                                     wcTrustedToken:nil
                                            zipCode:nil
                                            storeID:nil
                                          catalogID:nil
                                             locale:nil
                                         completion:^(NSError *error) {
                                             expect(error).beNil();
                                             NSLog(@"UserID: %@", [BOTChat sharedInstance].layerClient.authenticatedUser.userID);
                                             NSLog(@"ConversationID: %@", [BOTChat sharedInstance].conversation.identifier);
                                             expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                             expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                             expect([BOTChat sharedInstance].userID).beNil();
                                             expect([BOTChat sharedInstance].wcToken).beNil();
                                             expect([BOTChat sharedInstance].wcTrustedToken).beNil();
                                             expect([BOTChat sharedInstance].zipCode).beNil();
                                             expect([BOTChat sharedInstance].storeID).beNil();
                                             expect([BOTChat sharedInstance].catalogID).beNil();
                                             expect([BOTChat sharedInstance].locale).beNil();
                                             firstConversation = [[BOTChat sharedInstance].conversation.identifier absoluteString];
                                             
                                             [[BOTChat sharedInstance] connectWithUserID:userID2
                                                                                 wcToken:wcToken2
                                                                          wcTrustedToken:wcTrustedToken2
                                                                                 zipCode:zipCode2
                                                                                 storeID:storeID2
                                                                               catalogID:catalog2
                                                                                  locale:locale2
                                                                              completion:^(NSError *error) {
                                                                                  expect(error).beNil();
                                                                                  expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                                                                  expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                                                                  expect([BOTChat sharedInstance].userID).will.equal(userID2);
                                                                                  expect([BOTChat sharedInstance].wcToken).will.equal(wcToken2);
                                                                                  expect([BOTChat sharedInstance].wcTrustedToken).will.equal(wcTrustedToken2);
                                                                                  expect([BOTChat sharedInstance].zipCode).will.equal(zipCode2);
                                                                                  expect([BOTChat sharedInstance].storeID).will.equal(storeID2);
                                                                                  expect([BOTChat sharedInstance].catalogID).will.equal(catalog2);
                                                                                  expect([BOTChat sharedInstance].locale).will.equal(locale2);
                                                                                  
                                                                                  // Must have different conversation IDs
                                                                                  NSString *newConversationID = [[BOTChat sharedInstance].conversation.identifier absoluteString];
                                                                                  expect(firstConversation).willNot.beNil();
                                                                                  XCTAssertTrue([firstConversation isEqualToString:newConversationID]);
                                                                                  [userDetailsShift fulfill];
                                                                              }];
                                         }];
    }];
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}

- (void)testGoingFromAuthUserTononAuthUserKeepsConversationID
{
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        expect(error).beNil();
        
        NSString *userID2 = @"T$";
        NSString *wcToken2 = @"12345";
        NSString *wcTrustedToken2 = @"9999";
        NSString *zipCode2 = @"90888";
        NSString *storeID2 = @"1";
        NSString *catalog2 = @"catalog99";
        NSString *locale2 = @"local99";
        
        __block NSString *firstConversation = nil;
        
        XCTestExpectation *userDetailsShift = [self expectationWithDescription:@"Should update ENV"];
        [[BOTChat sharedInstance] connectWithUserID:userID2
                                            wcToken:wcToken2
                                     wcTrustedToken:wcTrustedToken2
                                            zipCode:zipCode2
                                            storeID:storeID2
                                          catalogID:catalog2
                                             locale:locale2
                                         completion:^(NSError *error) {
                                             expect(error).beNil();
                                             NSLog(@"UserID: %@", [BOTChat sharedInstance].layerClient.authenticatedUser.userID);
                                             NSLog(@"ConversationID: %@", [BOTChat sharedInstance].conversation.identifier);
                                             expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                             expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                             expect([BOTChat sharedInstance].userID).will.equal(userID2);
                                             expect([BOTChat sharedInstance].wcToken).will.equal(wcToken2);
                                             expect([BOTChat sharedInstance].wcTrustedToken).will.equal(wcTrustedToken2);
                                             expect([BOTChat sharedInstance].zipCode).will.equal(zipCode2);
                                             expect([BOTChat sharedInstance].storeID).will.equal(storeID2);
                                             expect([BOTChat sharedInstance].catalogID).will.equal(catalog2);
                                             expect([BOTChat sharedInstance].locale).will.equal(locale2);
                                             firstConversation = [[BOTChat sharedInstance].conversation.identifier absoluteString];
                                             
                                             [[BOTChat sharedInstance] connectWithUserID:nil
                                                                                 wcToken:nil
                                                                          wcTrustedToken:nil
                                                                                 zipCode:nil
                                                                                 storeID:nil
                                                                               catalogID:nil
                                                                                  locale:nil
                                                                              completion:^(NSError *error) {
                                                                                  expect(error).beNil();
                                                                                  expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                                                                  expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                                                                  expect([BOTChat sharedInstance].userID).will.equal(nil);
                                                                                  expect([BOTChat sharedInstance].wcToken).will.equal(nil);
                                                                                  expect([BOTChat sharedInstance].wcTrustedToken).will.equal(nil);
                                                                                  expect([BOTChat sharedInstance].zipCode).will.equal(nil);
                                                                                  expect([BOTChat sharedInstance].storeID).will.equal(nil);
                                                                                  expect([BOTChat sharedInstance].catalogID).will.equal(nil);
                                                                                  expect([BOTChat sharedInstance].locale).will.equal(nil);
                                                                                  
                                                                                  // Must have different conversation IDs
                                                                                  NSString *newConversationID = [[BOTChat sharedInstance].conversation.identifier absoluteString];
                                                                                  expect(firstConversation).willNot.beNil();
                                                                                  XCTAssertTrue([firstConversation isEqualToString:newConversationID]);
                                                                                  [userDetailsShift fulfill];
                                                                              }];
                                         }];
    }];
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}

/**
 Testing Connectivity
 */

// Testing connecting only once
- (void)testConnectingOnce
{
    NSDate *start = [NSDate date];
    XCTestExpectation *connectExpectation = [self expectationWithDescription:@"Should connect"];
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        expect(error).beNil();
        expect([BOTChat sharedInstance].serviceManager.environment).to.equal(BOTEnvironmentDevelopment);
        [[BOTChat sharedInstance] connectWithUserID:nil
                                            wcToken:nil
                                     wcTrustedToken:nil
                                            zipCode:nil
                                            storeID:nil
                                          catalogID:nil
                                             locale:nil
                                         completion:^(NSError *error) {
                                             expect(error).beNil();
                                             expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                             expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                             expect([BOTChat sharedInstance].userID).will.equal(nil);
                                             expect([BOTChat sharedInstance].wcToken).will.equal(nil);
                                             expect([BOTChat sharedInstance].wcTrustedToken).will.equal(nil);
                                             expect([BOTChat sharedInstance].zipCode).will.equal(nil);
                                             expect([BOTChat sharedInstance].storeID).will.equal(nil);
                                             expect([BOTChat sharedInstance].catalogID).will.equal(nil);
                                             expect([BOTChat sharedInstance].locale).will.equal(nil);
                                             // Time profile
                                             NSDate *end = [NSDate date];
                                             NSTimeInterval executionTime = [end timeIntervalSinceDate:start];
                                             NSLog(@"Layer full connection time = %f", executionTime);
                                             
                                             [connectExpectation fulfill];
                                         }];
    }];
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}

/**
 * Retry Connect
 */

// Note - to test this thing actually retrying, turn off your wifi and look for the retry logs. It still
// needs to hit the completion block
- (void)testConnectingWithRetryCount
{
    NSDate *start = [NSDate date];
    XCTestExpectation *connectExpectation = [self expectationWithDescription:@"Should connect"];
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        expect(error).beNil();
        expect([BOTChat sharedInstance].serviceManager.environment).to.equal(BOTEnvironmentDevelopment);
        [[BOTChat sharedInstance] connectWithUserID:nil
                                            wcToken:nil
                                     wcTrustedToken:nil
                                            zipCode:nil
                                            storeID:nil
                                          catalogID:nil
                                             locale:nil
                                         retryCount:3
                                         completion:^(NSError *error) {
                                             if (error.code == RetryCountExhaustedFailureCode) {
                                                 [connectExpectation fulfill];
                                             } else {
                                                 expect(error).beNil();
                                                 expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                                 expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                                 expect([BOTChat sharedInstance].userID).will.equal(nil);
                                                 expect([BOTChat sharedInstance].wcToken).will.equal(nil);
                                                 expect([BOTChat sharedInstance].wcTrustedToken).will.equal(nil);
                                                 expect([BOTChat sharedInstance].zipCode).will.equal(nil);
                                                 expect([BOTChat sharedInstance].storeID).will.equal(nil);
                                                 expect([BOTChat sharedInstance].catalogID).will.equal(nil);
                                                 expect([BOTChat sharedInstance].locale).will.equal(nil);
                                                 // Time profile
                                                 NSDate *end = [NSDate date];
                                                 NSTimeInterval executionTime = [end timeIntervalSinceDate:start];
                                                 NSLog(@"Layer full connection time = %f", executionTime);
                                                 
                                                 [connectExpectation fulfill];
                                             }
                                         }];
    }];
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}

/**
 Tests to make sure:
 - Blocks are dispatched serially
 - Blocks run serially
 - Both blocks ensure connectivity
 */
- (void)testConnectingConcurrently
{
    NSDate *start = [NSDate date];
    XCTestExpectation *connectExpectation = [self expectationWithDescription:@"Should connect"];
    dispatch_group_t completionGroup = dispatch_group_create();
    
    __block BOOL blockOneIsExecuting = YES;
    dispatch_group_enter(completionGroup);
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        expect(error).beNil();
        expect([BOTChat sharedInstance].serviceManager.environment).to.equal(BOTEnvironmentDevelopment);
        [[BOTChat sharedInstance] connectWithUserID:nil
                                            wcToken:nil
                                     wcTrustedToken:nil
                                            zipCode:nil
                                            storeID:nil
                                          catalogID:nil
                                             locale:nil
                                         completion:^(NSError *error) {
                                             expect(error).beNil();
                                             expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                             expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                             expect([BOTChat sharedInstance].userID).will.equal(nil);
                                             expect([BOTChat sharedInstance].wcToken).will.equal(nil);
                                             expect([BOTChat sharedInstance].wcTrustedToken).will.equal(nil);
                                             expect([BOTChat sharedInstance].zipCode).will.equal(nil);
                                             expect([BOTChat sharedInstance].storeID).will.equal(nil);
                                             expect([BOTChat sharedInstance].catalogID).will.equal(nil);
                                             expect([BOTChat sharedInstance].locale).will.equal(nil);
                                             expect([BOTChat sharedInstance].layerClient.isConnected).will.beTruthy();
                                             
                                             expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                             expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                             blockOneIsExecuting = NO;
                                             dispatch_group_leave(completionGroup);
                                         }];
    }];
    
    __block BOOL blockTwoIsExecuting = YES;
    dispatch_group_enter(completionGroup);
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        expect(error).beNil();
        expect([BOTChat sharedInstance].serviceManager.environment).to.equal(BOTEnvironmentDevelopment);
        [[BOTChat sharedInstance] connectWithUserID:nil
                                            wcToken:nil
                                     wcTrustedToken:nil
                                            zipCode:nil
                                            storeID:nil
                                          catalogID:nil
                                             locale:nil
                                         completion:^(NSError *error) {
                                             expect(error).beNil();
                                             expect(blockOneIsExecuting).will.beFalsy();
                                             expect([BOTChat sharedInstance].layerClient.isConnected).will.beTruthy();
                                             expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                             expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                             blockTwoIsExecuting = NO;
                                             dispatch_group_leave(completionGroup);
                                         }];
    }];
    
    // Synchronizes the completion
    dispatch_group_notify(completionGroup, dispatch_get_main_queue(), ^{
        // Time profile
        NSDate *end = [NSDate date];
        NSTimeInterval executionTime = [end timeIntervalSinceDate:start];
        NSLog(@"Layer full connection time = %f", executionTime);
        
        expect(blockOneIsExecuting).will.beFalsy();
        expect(blockTwoIsExecuting).will.beFalsy();
        [connectExpectation fulfill];
    });

    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}

- (void)testSwitchingEnvsWhenLoggedOut
{
    XCTestExpectation *connectExpectation = [self expectationWithDescription:@"Switch ENVs"];
    dispatch_group_t completionGroup = dispatch_group_create();
    expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
    dispatch_group_enter(completionGroup);
    XCTAssertTrue([BOTChat sharedInstance].serviceManager.environment == BOTEnvironmentDevelopment);
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        expect(error).beNil();
        XCTAssertTrue([BOTChat sharedInstance].serviceManager.environment == BOTEnvironmentDevelopment);
        expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
        expect(error).to.beNil();
        [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentProduction completion:^(NSError *error) {
            expect(error).beNil();
            XCTAssertTrue([BOTChat sharedInstance].serviceManager.environment == BOTEnvironmentProduction);
            expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
            expect(error).to.beNil();
            [connectExpectation fulfill];
        }];
    }];
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}

- (void)testSwitchingEnvsWhenLoggedIn
{
    XCTestExpectation *connectExpectation = [self expectationWithDescription:@"Should connect"];
    dispatch_group_t completionGroup = dispatch_group_create();
    expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
    dispatch_group_enter(completionGroup);
    XCTAssertTrue([BOTChat sharedInstance].serviceManager.environment == BOTEnvironmentDevelopment);
    [[BOTChat sharedInstance] connectWithUserID:nil
                                        wcToken:nil
                                 wcTrustedToken:nil
                                        zipCode:nil
                                        storeID:nil
                                      catalogID:nil
                                         locale:nil
                                     completion:^(NSError *error) {
                                         expect(error).beNil();
                                         XCTAssertTrue([BOTChat sharedInstance].serviceManager.environment == BOTEnvironmentDevelopment);
                                         expect([BOTChat sharedInstance].layerClient.isConnected).will.beTruthy();
                                         expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                         expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                         
                                         [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentLocal completion:^(NSError *error) {
                                             expect(error).beNil();
                                             XCTAssertTrue([BOTChat sharedInstance].serviceManager.environment == BOTEnvironmentLocal);
                                             expect([BOTChat sharedInstance].layerClient.isConnected).will.beTruthy();
                                             expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                             expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                             [connectExpectation fulfill];
                                         }];
    }];
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}


/**
 Deauthentication
 */


/**
 Single Deauthentication Test
 */
- (void)testDeauthentication
{
    XCTestExpectation *connectExpectation =  [self expectationWithDescription:@"Should deauth"];
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        [[BOTChat sharedInstance] connectWithUserID:nil
                                            wcToken:nil
                                     wcTrustedToken:nil
                                            zipCode:nil
                                            storeID:nil
                                          catalogID:nil
                                             locale:nil
                                         completion:^(NSError *error) {
                                             expect(error).beNil();
                                             XCTAssertTrue([BOTChat sharedInstance].serviceManager.environment == BOTEnvironmentDevelopment);
                                             expect([BOTChat sharedInstance].layerClient.isConnected).will.beTruthy();
                                             expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                             expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                             
                                             [[BOTChat sharedInstance] deauthenticateWithCompletion:^(NSError *error) {
                                                 expect(error).beNil();
                                                 expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
                                                 expect(error).to.beNil();
                                                 [connectExpectation fulfill];
                                             }];
                                         }];

    }];

    [self waitForExpectationsWithTimeout:550.0 handler:nil];
}

/**
 Single Deauthentication Test. Testing to make sure redundant calls
 do not cause an error or state issue
 */
- (void)testDeauthenticationTwice
{
    XCTestExpectation *connectExpectation =  [self expectationWithDescription:@"Should deauth"];
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        [[BOTChat sharedInstance] connectWithUserID:nil
                                            wcToken:nil
                                     wcTrustedToken:nil
                                            zipCode:nil
                                            storeID:nil
                                          catalogID:nil
                                             locale:nil
                                         completion:^(NSError *error) {
                                             expect(error).beNil();
                                             XCTAssertTrue([BOTChat sharedInstance].serviceManager.environment == BOTEnvironmentDevelopment);
                                             expect([BOTChat sharedInstance].layerClient.isConnected).will.beTruthy();
                                             expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                             expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                             
                                             [[BOTChat sharedInstance] deauthenticateWithCompletion:^(NSError *error) {
                                                 expect(error).beNil();
                                                 expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
                                                 expect(error).to.beNil();
                                                 
                                                 // Should behave the same and not raise an error
                                                 [[BOTChat sharedInstance] deauthenticateWithCompletion:^(NSError *error) {
                                                     expect(error).beNil();
                                                     expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
                                                     expect(error).to.beNil();
                                                     [connectExpectation fulfill];
                                                 }];
                                             }];
                                         }];
    }];
    [self waitForExpectationsWithTimeout:550.0 handler:nil];
}

/**
 Testing that concurrent calls to deauth get called seaially, 
 and do not execute concurrently
 */
- (void)testDeauthenticationAndConcurrently
{
    NSDate *start = [NSDate date];
    XCTestExpectation *connectExpectation = [self expectationWithDescription:@"Should deauth concurrently"];
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        [[BOTChat sharedInstance] connectWithUserID:nil
                                            wcToken:nil
                                     wcTrustedToken:nil
                                            zipCode:nil
                                            storeID:nil
                                          catalogID:nil
                                             locale:nil
                                         completion:^(NSError *error) {
                                             expect(error).beNil();
                                             XCTAssertTrue([BOTChat sharedInstance].serviceManager.environment == BOTEnvironmentDevelopment);
                                             expect([BOTChat sharedInstance].layerClient.isConnected).will.beTruthy();
                                             expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                             expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                             
                                             dispatch_group_t completionGroup = dispatch_group_create();
                                             
                                             __block BOOL blockOneIsExecuting = YES;
                                             dispatch_group_enter(completionGroup);
                                             [[BOTChat sharedInstance] deauthenticateWithCompletion:^(NSError *error) {
                                                 expect(error).beNil();
                                                 expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
                                                 expect(error).to.beNil();
                                                 
                                                 blockOneIsExecuting = NO;
                                                 dispatch_group_leave(completionGroup);
                                             }];
                                             
                                             __block BOOL blockTwoIsExecuting = YES;
                                             dispatch_group_enter(completionGroup);
                                             [[BOTChat sharedInstance] deauthenticateWithCompletion:^(NSError *error) {
                                                 expect(error).beNil();
                                                 expect(blockOneIsExecuting).will.beFalsy();
                                                 expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
                                                 expect(error).to.beNil();
                                                 
                                                 blockTwoIsExecuting = NO;
                                                 dispatch_group_leave(completionGroup);
                                             }];
                                             
                                             
                                             // Synchronizes the completion
                                             dispatch_group_notify(completionGroup, dispatch_get_main_queue(), ^{
                                                 // Time profile
                                                 NSDate *end = [NSDate date];
                                                 NSTimeInterval executionTime = [end timeIntervalSinceDate:start];
                                                 NSLog(@"Layer disconnect time = %f", executionTime);
                                                 
                                                 expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
                                                 expect(blockOneIsExecuting).will.beFalsy();
                                                 expect(blockTwoIsExecuting).will.beFalsy();
                                                 [connectExpectation fulfill];
                                             });
                                         }];
    }];
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}

/**
 Doing 10 auth and deauths to see if any collide or
 cause any issues w/ eachother.
 */
- (void)testDeauthenticationAndAuthConcurrently
{
    NSDate *start = [NSDate date];
    XCTestExpectation *connectExpectation = [self expectationWithDescription:@"Should connect"];
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        dispatch_group_t completionGroup = dispatch_group_create();
        
        for (int i = 0; i < 10; i++) {
            // Connect
            dispatch_group_enter(completionGroup);
            [[BOTChat sharedInstance] connectWithUserID:nil
                                                wcToken:nil
                                         wcTrustedToken:nil
                                                zipCode:nil
                                                storeID:nil
                                              catalogID:nil
                                                 locale:nil
                                             completion:^(NSError *error) {
                                                 expect(error).beNil();
                                                 expect([BOTChat sharedInstance].layerClient.isConnected).will.beTruthy();
                                                 expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beTruthy();
                                                 expect([BOTChat sharedInstance].conversation).willNot.beNil();
                                                 dispatch_group_leave(completionGroup);
                                             }];
            
            
            // Deauth
            dispatch_group_enter(completionGroup);
            [[BOTChat sharedInstance] deauthenticateWithCompletion:^(NSError *error) {
                expect(error).beNil();
                expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
                expect(error).to.beNil();
                
                dispatch_group_leave(completionGroup);
            }];
        }
        
        // Synchronizes the completion
        dispatch_group_notify(completionGroup, dispatch_get_main_queue(), ^{
            // Time profile
            NSDate *end = [NSDate date];
            NSTimeInterval executionTime = [end timeIntervalSinceDate:start];
            NSLog(@"Layer disconnect time = %f", executionTime);
            
            expect([BOTChat sharedInstance].layerClient.authenticatedUser).will.beNil();
            [connectExpectation fulfill];
        });
    }];
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}


#pragma mark - Helpers

- (void)resetState
{
    XCTestExpectation *connectExpectation = [self expectationWithDescription:@"Should connect"];
    [[BOTChat sharedInstance] deauthenticateWithCompletion:^(NSError *error) {
        if (!([error.domain isEqualToString:NSPOSIXErrorDomain] && error.code == 50)) {
            expect(error).beNil();
        }
        [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
            if (error.domain == NSPOSIXErrorDomain && error.code == 50) {
                [connectExpectation fulfill];
            } else {
                expect(error).beNil();
                [connectExpectation fulfill];
            }
        }];
    }];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:BOTEasyServiceConfigurationKey];
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}

@end
