//
//  BOTEasyServiceManagerTest.m
//  Staples
//
//  Created by Kevin Coleman on 12/6/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BOTEasyServiceManager.h"
#import <Expecta/Expecta.h>

@interface BOTEasyServiceManagerTest : XCTestCase

@property (nonatomic) BOTEasyServiceManager *manager;
@end

@implementation BOTEasyServiceManagerTest

- (void)setUp
{
    [super setUp];
    [Expecta setAsynchronousTestTimeout:5];
    self.manager = [BOTEasyServiceManager managerWithEnvironment:BOTEnvironmentLocal];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testFetchingIdentityToken
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"Conversation Creation Expectation"];
    [self.manager appConfigurationWithDeviceID:@"test" staplesUserID:@"test" nonce:nil completion:^(BOTAppConfiguration *config, NSError *error) {
        expect(error).to.beNil();
        expect(config.identityToken).to.beNil();
        expect(config.conversationID).notTo.beNil();
        [expectation fulfill];
    }];
    [self waitForExpectationWithTimeout:2];
}

- (void)testExecutingOnboardingPhase
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"Conversation Creation Expectation"];
    NSString *conversationID = @"layer:///conversations/94c37066-f971-4a23-bff5-6c6d39f29b02";
    [self.manager executeOnboardingPhase:@"onboarding-phase-welcome" conversationID:conversationID completion:^(NSDictionary *json, NSError *error) {
        expect(error).to.beNil();
        expect(json[@"next_pahse"]).toNot.beNil();
        [expectation fulfill];
    }];
    [self waitForExpectationWithTimeout:10];
}

- (void)testGuidedListItems
{
    expect([self.manager guidedListItems]).toNot.beNil();
}

- (void)testFetchingAppConfigurationWithABadEnv
{
    expect(^{
        self.manager = [BOTEasyServiceManager managerWithEnvironment:45];
    }).to.raise(NSInternalInconsistencyException);
}

- (void)testSupplyingBadInputParametersToIdentityTokenRequest
{
    __block BOOL errored = NO;
    [self.manager appConfigurationWithDeviceID:nil staplesUserID:nil nonce:nil completion:^(BOTAppConfiguration *config, NSError *error) {
        expect(error).toNot.beNil();
        expect(config.identityToken).to.beNil();
        expect(config.conversationID).to.beNil();
        errored = YES;
    }];
    expect(errored).will.beTruthy();
}

- (void)waitForExpectationWithTimeout:(NSUInteger)timeout
{
    [self waitForExpectationsWithTimeout:timeout handler:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}
- (void)testGettingGuidedListItems
{
    NSArray *guidedListItems = [self.manager guidedListItems];
    expect(guidedListItems).toNot.beNil();
    expect(guidedListItems.count).toNot.equal(0);
}

@end
