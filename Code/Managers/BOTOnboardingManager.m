//
//  BOTOnboardingManager.m
//  Staples
//
//  Created by Kevin Coleman on 12/26/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTOnboardingManager.h"
#import "BOTLayerManager.h"
#import "BOTChat.h"
#import "BOTUtilities.h"

#import <LayerKit/LayerKit.h>

@interface BOTOnboardingManager ();

@property (nonatomic) BOTLayerManager *manager;
@property (nonatomic) NSString *conversationID;
@property (nonatomic) NSString *buttonName;
@property (nonatomic) NSString *nextPhase;
@end

@implementation BOTOnboardingManager

NSString *const BOTOnboardingActionNotification = @"BOTOnboardingActionNotification";
NSString *const BOTOnboardingActionKey = @"BOTOnboardingAction";
NSString *const BOTOnboardingButtonNameKey = @"BOTOnboardingButtonName";
NSString *const BOTNextOnboardingPhase = @"BOTNextOnboardingPhase";

+ (id)managerWithLayerManager:(BOTLayerManager *)manager conversationID:(NSURL *)conversationID
{
    return [[self alloc] initWithLayerManager:manager conversationID:conversationID];
}

- (id)initWithLayerManager:(BOTLayerManager *)manager conversationID:(NSURL *)conversationID;
{
    self = [super init];
    if (self) {
        _manager = manager;
        
        NSString *conversationIDString = conversationID.absoluteString;
        _conversationID = [conversationIDString stringByReplacingOccurrencesOfString:@"layer:///conversations/" withString:@""];
    }
    return self;
}

#pragma mark - Public Methods

- (void)executeOnboardingPhase:(NSString *)phase data:(NSDictionary *)data completion:(void(^)(BOOL success, NSError *))completion
{
    NSString *nextPhase;
    if (phase) {
        nextPhase = phase;
    } else {
        nextPhase = [[NSUserDefaults standardUserDefaults] valueForKey:BOTNextOnboardingPhase];
    }
    
    if (!nextPhase) {
        return;
    }
    
    // Build Payload.
    NSMutableDictionary *mutableData = [data mutableCopy];
    mutableData[@"conversation_id"] = self.conversationID;
    mutableData[@"phase"] = nextPhase;
    
    [[BOTChat sharedInstance].serviceManager executeOnboardingPhase:nextPhase payload:mutableData completion:^(NSDictionary *json, NSError *error) {
        NSString *nextPhase = json[@"next_phase"];
        if (nextPhase) {
            [[NSUserDefaults standardUserDefaults] setValue:nextPhase forKey:BOTNextOnboardingPhase];
        } else {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:BOTNextOnboardingPhase];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}

- (void)updateButtonName:(NSString *)buttonName
{
    self.buttonName = buttonName;
}

#pragma mark - Message Sending

- (void)sendMessages:(NSArray *)messages delay:(NSUInteger)delay completion:(void (^)(BOOL, NSError *))completion
{
    for (id message in messages) {
        if ([message isKindOfClass:[NSString class]]) {
            [self sendTextMessage:message];
        }
        if ([message isKindOfClass:[NSDictionary class]]) {
            [self sendPayloadMessage:message];
        }
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:delay]];
    }
    completion(YES, nil);
}

- (void)sendTextMessage:(NSString *)textMessage
{
    [self.manager sendMessageText:textMessage toConversation:self.conversationID asSender:@"ESPSystem" completion:^(NSDictionary *data, NSError *error) {
        if (error) {
            NSLog(@"fucked");
        }
        NSLog(@"Not fucked");
    }];
}

- (void)sendPayloadMessage:(NSDictionary *)payload
{
    NSDictionary *body = payload[@"body"];
    NSDictionary *mimeType = payload[@"mime_type"];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:body options:NSJSONWritingPrettyPrinted error:nil];
    NSString *bytes = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSArray *parts = @[@{@"body": bytes, @"mime_type": mimeType}];
    [self.manager sendMessageParts:parts toConversation:self.conversationID asSender:@"ESPSystem" completion:^(NSDictionary *data, NSError *error) {
        if (error) {
            NSLog(@"fucked");
        }
        NSLog(@"Not fucked");
    }];
}

#pragma mark - Phase Message Building

- (NSArray *)messagesForWelcomeOnboardingPhase
{
    return @[
             @"Hey. I’m the Staples automated ‘bot.",
             @"I can help you get started with your new Easy Button. ",
             @"Good news: I can see your new Easy Button.",
             @"Would you like to pair your new Easy Button with your Staples Business Advantage account?",
             @{ @"body": [self pairButtonMessage], @"mime_type": BOTOnboardingButtonMIMEType }
             ];
}

- (NSArray *)messagesForPowerUpOnboardingPhase
{
    return @[
             @"Ok. Let's get started",
             @{ @"body": [self powerUpMessage], @"mime_type": BOTOnboardingCardMIMEType },
             @"Is the light on your button pulsing?",
             @{ @"body": [self pulsingButtonMessage], @"mime_type": BOTOnboardingButtonMIMEType },
             ];
}

- (NSArray *)messagesForScanButtonOnboardingPhase
{
    return @[
             @"Great",
             @"Next, let's identify your button.",
             @{ @"body": [self scanYourButtonMessage], @"mime_type": BOTOnboardingCardMIMEType },
             ];
}

- (NSArray *)messagesForNameButtonOnboardingPhase
{
    return @[
             @"Got it.",
             @"Now, one last step: naming your connected Easy Button.",
             @{ @"body": [self nameButtonMessage], @"mime_type": BOTOnboardingCardMIMEType },
             ];
}

- (NSArray *)messagesForNameConfirmationOnboardingPhase
{
    return @[
             [NSString stringWithFormat:@"Ok, I'll call your button %@, is that right?", self.buttonName],
             @{ @"body": [self nameConfirmationButtonMessage], @"mime_type": BOTOnboardingButtonMIMEType },
             ];
}

#pragma mark - Message formatting.

NSString *const BOTEasyButtonURL = @"http://www.staples-3p.com/s7/is/image/Staples/s0105150_sc7?$splssku$";

- (NSDictionary *)powerUpMessage
{
    return @{
            BOTOnboardingTitle : @"Power Up",
            BOTOnboardingImageURL: BOTEasyButtonURL,
            BOTOnboardingInstruction: @"First lets get your new Easy Button connected. Plug it in. When the Button's light is pulsing, you're set to begin connecting it to your account.",
            };
}

- (NSDictionary *)scanYourButtonMessage
{
    return @{
            BOTOnboardingTitle : @"Scan Your Button ID",
            BOTOnboardingImageURL: BOTEasyButtonURL,
            BOTOnboardingInstruction: @"Using your phone's camera, scan the sticker on the bottom of your Easy Button.",
            BOTOnboardingButtonText: @"Lauch Camera"
            };
}

- (NSDictionary *)nameButtonMessage
{
    return @{
            BOTOnboardingTitle : @"Create Name or Location",
            BOTOnboardingImageURL: BOTEasyButtonURL,
            BOTOnboardingInstruction: @"First, create a name - or a location - for this Easy Button. You can type it below. Or, you can speak it. Say \"Call this button _____\", (ex: \'Break Room\' or \'Ernie\'",
            BOTOnboardingPlaceholder: @"Your name or location"
            };
}

- (NSDictionary *)pairButtonMessage
{
    return @{BOTLeftButtonTitle : @"No, try again.", BOTRightButtonTitle: @"Yes"};
}

- (NSDictionary *)pulsingButtonMessage
{
    return @{BOTLeftButtonTitle : @"No/Troubleshoot", BOTRightButtonTitle: @"Yes"};
}

- (NSDictionary *)nameConfirmationButtonMessage
{
    return @{BOTLeftButtonTitle : @"No, try again.", BOTRightButtonTitle: @"Yes"};
}


@end
