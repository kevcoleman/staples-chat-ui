
//
//  BOTLayerManager.m
//  Staples
//
//  Created by Kevin Coleman on 12/26/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTLayerManager.h"

@interface BOTLayerManager ()

@property (nonatomic) NSURLSession *URLSession;
@property (nonatomic) NSURL *baseURL;
@property (nonatomic) NSString *appID;

@end

@implementation BOTLayerManager

+ (instancetype)managerWithAppID:(NSURL *)appID
{
    return [[self alloc] initWithAppID:appID];
}

- (id)initWithAppID:(NSURL *)appID
{
    self = [super init];
    if (self) {
        _URLSession = [self defaultURLSession];
        _appID = appID.absoluteString;
        
        NSURL *url = [NSURL URLWithString:@"https://api.layer.com"];
        NSString *appIDString = [appID.absoluteString stringByReplacingOccurrencesOfString:@"layer:///apps/staging/" withString:@""];
        _baseURL = [url URLByAppendingPathComponent:[NSString stringWithFormat:@"/apps/%@", appIDString]];
    }
    return self;
}

- (NSURLSession *)defaultURLSession
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    configuration.HTTPAdditionalHeaders = @{ @"Accept": @"application/vnd.layer+json; version=2.0",
                                             @"Content-Type": @"application/json",
                                             @"Authorization": @"Bearer 3sD62pYpAb6AR8Q2s26q2yeGsx1y4iiRFRjoIHRHvkCUfBy2"};
    return [NSURLSession sessionWithConfiguration:configuration];
}

- (void)createConversationWithParticipants:(NSArray *)participants completion:(void(^)(NSDictionary *data, NSError *error))completion
{
    NSURL *URL = [self.baseURL URLByAppendingPathComponent:@"/conversations"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    
    NSDictionary *parameters = @{ @"participants": participants, @"distinct" : @(YES)};
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    [self executeRequest:request expectedStatus:200 completion:completion];
}

- (void)sendMessageText:(NSString *)message
         toConversation:(NSString *)conversation
               asSender:(NSString *)sender
             completion:(void (^)(NSDictionary *, NSError *))completion
{
    NSString *conversationURL = [NSString stringWithFormat:@"/conversations/%@/messages", conversation];
    NSURL *URL = [self.baseURL URLByAppendingPathComponent:conversationURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    
    NSArray *parts = @[@{@"body": message, @"mime_type": @"text/plain"}];
    NSDictionary *parameters = @{ @"sender_id": [NSString stringWithFormat:@"layer:///identities/%@", sender], @"parts": parts};
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    [self executeRequest:request expectedStatus:201 completion:completion];
}

- (void)sendMessageParts:(NSArray *)parts
          toConversation:(NSString *)conversation
                asSender:(NSString *)sender
              completion:(void(^)(NSDictionary *data, NSError *error))completion;
{
    NSString *conversationURL = [NSString stringWithFormat:@"/conversations/%@/messages", conversation];
    NSURL *URL = [self.baseURL URLByAppendingPathComponent:conversationURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    
    NSDictionary *parameters = @{ @"sender_id": [NSString stringWithFormat:@"layer:///identities/%@", sender], @"parts": parts};
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    [self executeRequest:request expectedStatus:201 completion:completion];
}

- (void)executeRequest:(NSURLRequest *)request expectedStatus:(NSUInteger)status completion:(void(^)(NSDictionary *data, NSError *error))completion
{
    [[self.URLSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                return completion(nil, error);
            }
            
            NSError *parseError;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            if (parseError) {
                return completion(nil, parseError);
            }
            
            if ([(NSHTTPURLResponse *)response statusCode] != status) {
                
                NSString *errorString = [NSString stringWithFormat:@"Failed to fetch configuration with error: %@", json];
                NSError *responseError = [NSError errorWithDomain:@""
                                                             code:1
                                                         userInfo:@{NSLocalizedDescriptionKey: errorString}];
                return completion(nil, responseError);
            }
            
            completion(json, nil);
        });
    }] resume];
}

@end
