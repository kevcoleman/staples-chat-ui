
//
//  BOTPromiseManager.m
//  Staples
//
//  Created by Kevin Coleman on 1/12/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTPromiseManager.h"

NSString *const EasyServiceLayerAppIDKey = @"LayerAppID";

@implementation BOTPromiseManager

#pragma mark - Connect Promises

/**
 Attempts to retreive a auth-nonce from Layer's backend
 
 @return Promise(NSString *)nonce
 */
+ (AnyPromise *)noncePromiseWithClient:(LYRClient *)client
{
    return [AnyPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        if (!client) {
            NSString *errReason = @"Layer client cannot be nil";
            return resolve([NSError errorWithDomain:NSURLErrorDomain code:100 userInfo:@{NSLocalizedDescriptionKey: errReason}]);
        }
        
        [client requestAuthenticationNonceWithCompletion:^(NSString * _Nullable nonce, NSError * _Nullable error) {
            if (error) {
                return resolve(error);
            }
            resolve(nonce);
        }];
    }];
}

/**
 Attempts to connect to the easy system to
 authticate the nonce received from layer. Handles
 the scenario where we're authenticated, but EasySystem is down

 @param nonce Nonce provided by Layer to sign by our backend
 @param userID UserID of the layer user
 @return Promise(BOTAppConfiguration *)appConfig
 */
+ (AnyPromise *)appConfigurationPromiseWithDeviceID:(NSString *)deviceID
                                      staplesUserID:(NSString *)staplesUserID
                                              nonce:(NSString *)nonce
                                             client:(LYRClient *)client
                                     serviceManager:(BOTEasyServiceManager *)manager
{
    return [AnyPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        if (!deviceID || !manager) {
            NSString *errReason = @"DeviceID and Manager must be present";
            return resolve([NSError errorWithDomain:NSURLErrorDomain code:100 userInfo:@{NSLocalizedDescriptionKey: errReason}]);
        }
        [manager appConfigurationWithDeviceID:deviceID staplesUserID:staplesUserID nonce:nonce completion:^(BOTAppConfiguration *config, NSError *error) {
            if (error) {
                // Handle a potential where easy system is down,
                // but we have connection to internet and a previous conversation ID
                // Nonce param also needs to be nil, because we can't return an identity token if so
                if (!nonce && error.domain == NSURLErrorDomain && client.authenticatedUser) {
                    LYRConversation *conv = [self existingConversationForLayerIdentity:client.authenticatedUser client:client];
                    
                    // We have a conversation, resolve with it
                    if (conv) {
                        BOTAppConfiguration *offlineConfig = [BOTAppConfiguration credentialsWithIdentityToken:nil conversationID:conv.identifier];
                        return resolve(offlineConfig);
                    } else {
                        // We do not have a conversation, let's return the error
                        return resolve(error);
                    }
                } else {
                    // We either need a nonce, are not authticated with layer, or do not have a previous conversaiton
                    return resolve(error);
                }
            }
            
            // We're good. Resolve config
            resolve(config);
        }];
    }];
}

/**
 Sends our signed JWT to layer for authtication

 @param deviceID        Device ID
 @param identityToken   Signed token by our backend
 @return Promise(LYRIdentity *)authenticatedUser
 */
+ (AnyPromise *)authenticationPromiseWithDeviceID:(NSString *)deviceID IdentityToken:(NSString *)identityToken client:(LYRClient *)client;
{
    return [AnyPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        if (!client || !identityToken || !deviceID) {
            NSString *errReason = @"No parameters can be nil";
            return resolve([NSError errorWithDomain:NSURLErrorDomain code:100 userInfo:@{NSLocalizedDescriptionKey: errReason}]);
        }
        
        // Due to the lagging calls from the delegate, this method can be called when we're already auth'd. If
        // that happens and we're the same user, resolve that user.
        // If it differs, we need to chain the deauth process, then resolve appropriatley
        if (client.authenticatedUser) {
            if (deviceID == client.authenticatedUser.userID) {
                return resolve(client.authenticatedUser);
            } else {
                return resolve([self deauthenticationPromiseWithClient:client].then(^() {
                    return [self authenticationPromiseWithDeviceID:deviceID IdentityToken:identityToken client:client];
                }));
            }
        }
            
        // Send toke to Layer to auth
        NSLog(@"Class: %@", [identityToken class]);
        [client authenticateWithIdentityToken:identityToken completion:^(LYRIdentity * _Nullable authenticatedUser, NSError * _Nullable error) {
            if (error) {
                return resolve(error);
            }
            resolve(authenticatedUser);
        }];
    }];
}


/**
 Ensures the given conversation ID is synchronized with this client

 @param conversationID Layer Conversation ID
 @return Promise(LYRConversation *)conversation
 */
+ (AnyPromise *)coversationPromiseWithID:(NSURL *)conversationID layerIdentity:(LYRIdentity *)identity client:(LYRClient *)client
{
    return [AnyPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        if (!client || !identity) {
            NSString *errReason = @"Client and Identity must not be nil";
            return resolve([NSError errorWithDomain:NSURLErrorDomain code:100 userInfo:@{NSLocalizedDescriptionKey: errReason}]);
        }

        LYRConversation *conversation = [self existingConversationForLayerIdentity:identity client:client];
        if (conversation) {
            return resolve(conversation);
        }
        [client waitForCreationOfObjectWithIdentifier:conversationID timeout:10.0f completion:^(id  _Nullable conversation, NSError * _Nullable error) {
            if (error){
                return resolve(error);
            }
            resolve(conversation);
        }];
    }];
}

#pragma mark - Deauthenticate Promise

/**
 Deauthenticates the user

 @return Promise
 */
+ (AnyPromise *)deauthenticationPromiseWithClient:(LYRClient *)client
{
    static int deauthRetryCount = 0;
    return [AnyPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        if (!client) {
            NSString *errReason = @"Client cannot be nil";
            return resolve([NSError errorWithDomain:NSURLErrorDomain code:100 userInfo:@{NSLocalizedDescriptionKey: errReason}]);
        }

        if (!client.authenticatedUser) {
            return resolve(nil);
        }
        
        [client deauthenticateWithCompletion:^(BOOL success, NSError * _Nullable error) {
            if (error) {
                if (error.code == 4009 && deauthRetryCount == 0) {
                    deauthRetryCount = 1;
                    // Almost as stupid as the 'isConnecting' error. Maybe dumber actually since you can't
                    // query it to see if its deauthenticating...
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        resolve([self deauthenticationPromiseWithClient:client]);
                    });
                } else {
                    deauthRetryCount = 0;
                    return resolve(error);
                }
            }
            deauthRetryCount = 0;
            resolve(nil);
        }];
    }];
}

#pragma mark - Helpers


/**
 Looks for the existing conversation

 @return *LYRConversation
 */
+ (LYRConversation *)existingConversationForLayerIdentity:(LYRIdentity *)identity client:(LYRClient *)client
{
    NSSet *participants = [NSSet setWithObjects:identity.userID, nil];
    
    LYRQuery *query = [LYRQuery queryWithQueryableClass:[LYRConversation class]];
    query.predicate = [LYRPredicate predicateWithProperty:@"participants" predicateOperator:LYRPredicateOperatorIsIn value:participants];
    NSError *error;
    NSOrderedSet *conversations = [client executeQuery:query error:&error];
    if (error) {
        return nil;
    }
    return conversations.firstObject;
}

@end
