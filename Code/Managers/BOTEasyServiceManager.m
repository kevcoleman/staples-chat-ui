//
//  BOTEasyServiceManager.m
//  MyLayerChat
//
//  Created by Mindstix Software on 11/02/16.
//  Copyright © 2016 Mindstix Software. All rights reserved.
//

#import "BOTEasyServiceManager.h"

NSString *const BOTEnvironmentKey = @"BOTEnvironmentKey";
NSString *const BOTEasyServiceConfigurationKey = @"BOTEasyServiceConfigurationKey";
NSString *const BOTLayerAppIDKey = @"LayerAppID";
NSString *const BOTGuidedResponseListKey = @"guided_response_list";
NSString *const BOTIdentityToken = @"identity_token";
NSString *const BOTDataKey = @"data";
NSString *const BOTConversationKey = @"conversation_id";
NSString *const BOTEasyServiceErrorDomain = @"BOTEasyServiceErrorDomain";
NSString *const BOTBTSInstructionKey = @"message";

// Credentials Implementation

@interface BOTAppConfiguration()

@property (nonatomic, strong) NSString *identityToken;
@property (nonatomic, strong) NSURL *conversationID;

@end

@implementation BOTAppConfiguration

+ (instancetype)credentialsWithIdentityToken:(NSString *)identityToken conversationID:(NSURL *)conversationID
{
    BOTAppConfiguration *cred = [[BOTAppConfiguration alloc] init];
    cred.identityToken = identityToken;
    cred.conversationID = conversationID;
    return cred;
}

@end

// Servicer Manager Implementation

@interface BOTEasyServiceManager () <NSURLSessionDelegate>

@property (nonatomic) BOTEnvironment environment;
@property (nonatomic, readwrite) NSURL *baseURL;
@property (nonatomic, readwrite) NSString *tenantID;
@property (nonatomic, readwrite) NSString *apiKey;
@property (nonatomic, readwrite) NSArray *guidedResponseList;
@property (atomic, readwrite) NSString *layerAppID;

@end

@implementation BOTEasyServiceManager

+ (instancetype)managerWithEnvironment:(BOTEnvironment)environment
{
    return [[self alloc] initWithEnvironment:environment];
}

- (id)initWithEnvironment:(BOTEnvironment)environment
{
    self = [super init];
    if (self) {
        [self configureWithEnvironment:environment];
        _environment = environment;
        _URLSession = [self defaultURLSession];
    }
    return self;
}

- (void)configureWithEnvironment:(BOTEnvironment)environment
{
    NSAssert(environment <= BOTEnvironmentLocal, @"Suppled an invalid BOTEnvironment");
    
    switch (environment) {
        case BOTEnvironmentProduction:
            self.baseURL = [NSURL URLWithString:@"https://stpls-es-prod.labs.mindstix.com/mobile"];
            self.layerAppID = @"layer:///apps/staging/efa4048a-297b-11e6-a85f-2f6904000f97";
            break;
            
        case BOTEnvironmentDevelopment:
            self.baseURL = [NSURL URLWithString:@"https://dev.easysystem.staples.com/mobile"];
            self.layerAppID = @"layer:///apps/staging/3b2b723a-e1ce-11e5-a7a8-23ab7c095bad";
            break;
            
        case BOTEnvironmentLocal:
            self.baseURL = [NSURL URLWithString:@"http://localhost:8080/mobile"];
            self.layerAppID = @"layer:///apps/staging/3b2b723a-e1ce-11e5-a7a8-23ab7c095bad";
            break;
            
        default:
            self.baseURL = [NSURL URLWithString:@"https://stpls-es-prod.labs.mindstix.com/mobile"];
            self.layerAppID = @"layer:///apps/staging/efa4048a-297b-11e6-a85f-2f6904000f97";
            break;
    }
    
    self.tenantID = @"fcb4e510-2d61-11e6-9c0a-63a085f551ef";
    self.apiKey = @"5757f05b92f25e382e516a9a";
    
    BOTEnvironment currentEnvironment = [[[NSUserDefaults standardUserDefaults] objectForKey:BOTEnvironmentKey] integerValue];
    if (currentEnvironment != environment) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:BOTEasyServiceConfigurationKey];
        [[NSUserDefaults standardUserDefaults] setObject:@(environment) forKey:BOTEnvironmentKey];
    }
    self.environment = environment;
}

- (id)init
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Failed to call designated initializer." userInfo:nil];
}

- (NSURLSession *)defaultURLSession
{
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    configuration.HTTPAdditionalHeaders = @{ @"Accept": @"application/json",
                                             @"Content-Type": @"application/json",
                                             @"tenantId": self.tenantID,
                                             @"apiKey": self.apiKey,
                                             @"appVersion": appVersion};
    return [NSURLSession sessionWithConfiguration:configuration];
}

#pragma mark - Configuration

- (NSArray *)guidedListItems
{
    NSArray *guidedListItems = self.guidedResponseList;
    if (!guidedListItems || ![guidedListItems count]) {
        guidedListItems = [self defaultList];
    }
    return guidedListItems;
}

- (NSArray *)defaultList
{
    return @[
             @{@"action" : @"textMessage", @"flag": @{}, @"title": @"Track my shipment"},
             @{@"action" : @"textMessage", @"flag": @{}, @"title": @"Rewards summary"},
             @{@"action" : @"pictureMessage", @"flag": @{}, @"title": @"Scan supply list"},
             ];
}

- (NSArray *)guidedListTitles
{
    NSArray *guidedList = [self guidedListItems];
    NSMutableArray *titles = [NSMutableArray new];
    for (NSDictionary *data in guidedList) {
        NSString *title = data[@"title"];
        if (title) {
            [titles addObject:title];
        }
    }
    return titles;
}

#pragma mark - Registration

- (void)appConfigurationWithDeviceID:(NSString *)deviceID
                       staplesUserID:(NSString *)staplesUserID
                               nonce:(NSString *)nonce
                          completion:(void (^)(BOTAppConfiguration *config, NSError *error))completion
{
    if (!deviceID) {
        NSString *descrition = [NSString stringWithFormat:@"Failed to fetch identity token. Device ID must be supplied"];
        NSError *error = [NSError errorWithDomain:BOTEasyServiceErrorDomain code:BOTServiceErrorInvalidInput userInfo:@{NSLocalizedDescriptionKey : descrition}];
        return completion(nil, error);
    }
    
    //Change the address to point to identity token generator.
    NSURL *URL = [self.baseURL URLByAppendingPathComponent:@"/authenticate"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:deviceID forKey:@"user_id"];
    [parameters setValue:staplesUserID forKey:@"staples_user_id"];
    [parameters setValue:nonce forKey:@"nonce"];
    
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    [[self.URLSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                if (completion) {
                    return completion(nil, error);
                }
            }
            
            NSError *parseError;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            if (parseError) {
                if (completion) {
                    return completion(nil, parseError);
                }
            }
            
            if ([(NSHTTPURLResponse *)response statusCode] == 200) {
                NSDictionary *data = json[BOTDataKey];
                NSString *identityToken = data[BOTIdentityToken];
                NSString *conversationID = data[BOTConversationKey];
                NSURL *conversationURLID = [NSURL URLWithString:conversationID];
                self.guidedResponseList = data[BOTGuidedResponseListKey];
                BOTAppConfiguration *cred = [BOTAppConfiguration credentialsWithIdentityToken:identityToken conversationID:conversationURLID];
                return completion(cred, nil);
            } else {
                NSString *errorString = [NSString stringWithFormat:@"Failed to fetch identity token with error: %@", json];
                NSError *responseError = [NSError errorWithDomain:BOTEasyServiceErrorDomain
                                                             code:BOTServiceErrorIdentityTokenFailure
                                                         userInfo:@{NSLocalizedDescriptionKey: errorString}];
                if (completion) {
                    completion(nil, responseError);
                }
            }
        });
    }] resume];
}

- (void)loginWithAccountNumber:(NSString *)accountNumber userID:(NSString *)userID password:(NSString *)password completion:(void (^)(NSError *))completion{
    
    if (!accountNumber || !userID || !password) {
        NSString *descrition = [NSString stringWithFormat:@"Failed to Login. Must supply valid parameters"];
        NSError *error = [NSError errorWithDomain:BOTEasyServiceErrorDomain code:BOTServiceErrorInvalidInput userInfo:@{NSLocalizedDescriptionKey : descrition}];
        return completion(error);
    }
    
    NSURL *URL = [self.baseURL URLByAppendingPathComponent:@"/customer/login"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"companyID"] = accountNumber;
    parameters[@"userID"] = userID;
    parameters[@"password"] = password;
    
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:kNilOptions error:nil];
    
    [[self.URLSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                return completion(error);
            }
            
            NSError *parseError;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            if (parseError) {
                return completion(parseError);
            }
            
            if ([(NSHTTPURLResponse *)response statusCode] == 200) {
                completion(nil);
            } else {
                NSString *errorString = [NSString stringWithFormat:@"Failed to Login with error: %@", json];
                NSError *responseError = [NSError errorWithDomain:BOTEasyServiceErrorDomain
                                                             code:BOTServiceErrorLoginFailure
                                                         userInfo:@{NSLocalizedDescriptionKey: errorString}];
                completion(responseError);
            }
        });
    }] resume];
}

- (void)loginSAtoEasySystem:(NSString *)accountNumber userID:(NSString *)userID password:(NSString *)password completion:(void (^)(NSDictionary *json, NSError *error))completion
{
    if (!accountNumber || !userID || !password) {
        NSString *descrition = [NSString stringWithFormat:@"Failed to Login. Must supply valid parameters"];
        NSError *error = [NSError errorWithDomain:BOTEasyServiceErrorDomain code:BOTServiceErrorInvalidInput userInfo:@{NSLocalizedDescriptionKey : descrition}];
        return completion(nil, error);
    }
    
    NSString *url = [self.baseURL.absoluteString stringByReplacingOccurrencesOfString:@"/mobile" withString:@""];
    NSURL *baseURL = [NSURL URLWithString:url];
    NSURL *URL = [baseURL URLByAppendingPathComponent:@"/customer/login"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"companyID"] = accountNumber;
    parameters[@"userID"] = userID;
    parameters[@"password"] = password;
    
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:kNilOptions error:nil];
    
    [[self.URLSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                return completion(nil, error);
            }
            
            NSError *parseError;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            if (parseError) {
                return completion(nil, parseError);
            }
            
            if ([(NSHTTPURLResponse *)response statusCode] == 200) {
                completion(json, nil);
            } else {
                NSString *errorString = [NSString stringWithFormat:@"Failed to Login with error: %@", json];
                NSError *responseError = [NSError errorWithDomain:BOTEasyServiceErrorDomain
                                                             code:BOTServiceErrorLoginFailure
                                                         userInfo:@{NSLocalizedDescriptionKey: errorString}];
                completion(nil, responseError);
            }
        });
    }] resume];
}

- (void)loginEOAtoEasySystem:(NSString *)userName password:(NSString *)password completion:(void (^)(NSDictionary *json, NSError *error))completion
{
    if (!userName || !password) {
        NSString *descrition = [NSString stringWithFormat:@"Failed to Login. Must supply valid parameters"];
        NSError *error = [NSError errorWithDomain:BOTEasyServiceErrorDomain code:BOTServiceErrorInvalidInput userInfo:@{NSLocalizedDescriptionKey : descrition}];
        return completion(nil, error);
    }
    
    NSURL *URL = [self.baseURL URLByAppendingPathComponent:@"/eoa/customer/login"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"user_name"] = userName;
    parameters[@"password"] = password;
    
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:kNilOptions error:nil];
    
    [[self.URLSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                return completion(nil, error);
            }
            
            NSError *parseError;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            if (parseError) {
                return completion(nil, parseError);
            }
            
            if ([(NSHTTPURLResponse *)response statusCode] == 200) {
                completion(json, nil);
            } else {
                NSString *errorString = [NSString stringWithFormat:@"Failed to Login with error: %@", json];
                NSError *responseError = [NSError errorWithDomain:BOTEasyServiceErrorDomain
                                                             code:BOTServiceErrorLoginFailure
                                                         userInfo:@{NSLocalizedDescriptionKey: errorString}];
                completion(nil, responseError);
            }
        });
    }] resume];
}

- (void)executeOnboardingPhase:(NSString *)phase payload:(NSDictionary *)payload completion:(void (^)(NSDictionary *json, NSError *error))completion;
{
    NSURL *URL = [self.baseURL URLByAppendingPathComponent:@"/onboarding"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:payload options:kNilOptions error:nil];
    
    [[self.URLSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                return completion(nil, error);
            }
            
            NSError *parseError;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            if (parseError) {
                return completion(nil, parseError);
            }
            
            if ([(NSHTTPURLResponse *)response statusCode] == 200) {
                completion(json, nil);
            } else {
                NSString *errorString = [NSString stringWithFormat:@"Failed to Login with error: %@", json];
                NSError *responseError = [NSError errorWithDomain:BOTEasyServiceErrorDomain
                                                             code:BOTServiceErrorLoginFailure
                                                         userInfo:@{NSLocalizedDescriptionKey: errorString}];
                completion(nil, responseError);
            }
        });
    }] resume];
}

@end
