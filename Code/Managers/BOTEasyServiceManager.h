//
//  BOTEasyServiceManager.h
//  MyLayerChat
//
//  Created by Mindstix Software on 11/02/16.
//  Copyright © 2016 Mindstix Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BOTEasyServiceManager.h"

typedef NS_ENUM(NSUInteger, BOTEnvironment) {
    BOTEnvironmentProduction,
    BOTEnvironmentDevelopment,
    BOTEnvironmentLocal
};

typedef NS_ENUM(NSUInteger, BOTServiceError) {
    BOTServiceErrorConfigurationFailure = 12001,
    BOTServiceErrorIdentityTokenFailure = 12002,
    BOTServiceErrorConversationFailure = 12003,
    BOTServiceErrorInvalidInput = 12004,
    BOTServiceErrorLoginFailure = 12005,
};

extern NSString *const BOTLayerAppIDKey;

// Fwd decl.
@class BOTAppConfiguration;

/**
 @abstract The `BOTEasyServiceManager` class provides an interface for interacting with the Easy Server JSON API.
 */
@interface BOTEasyServiceManager : NSObject

///--------------------------------
/// @name Initializing a Manager
///--------------------------------

+ (instancetype)managerWithEnvironment:(BOTEnvironment)environment;

/**
 @abstract The environment of the manager
 */
@property (nonatomic, readonly) BOTEnvironment environment;

/**
 @abstract The base URL for the environment supplied in the initailizer.
 */
@property (nonatomic, readonly) NSURL *baseURL;

/**
 @abstract The Layer App ID for the environment supplied in the initailizer.
 */
@property (atomic, readonly) NSString *layerAppID;

/**
 @abstract The tenantID for the environment supplied in the initailizer.
 */
@property (nonatomic, readonly) NSString *tenantID;

/**
 @abstract The apiKey for the environment supplied in the initailizer.
 */
@property (nonatomic, readonly) NSString *apiKey;

/**
 @abstract The currently configured URL session.
 */
@property (nonatomic) NSURLSession *URLSession;

/**
 @abstract The guided list response dictionary.
 */
- (NSArray *)guidedListItems;

/**
 @abstract The guided list titles.
 */
- (NSArray *)guidedListTitles;

/**
 @abstract Fetches a Layer identity token for a given userName and nonce.
 @param deviceID        A identifier for the device
 @param staplesUserID   (optional) A Staples issued user identifier. If none (Guest), leave nil.
 @param nonce           (optional) A nonce value obtained via a call to `requestAuthenticationNonceWithCompletion:` on `LYRClient`.
 @param completion completion The block to execute upon completion of the asynchronous user registration operation. The block has no return value and accepts two arguments: An identity token that was obtained upon successful registration (or nil in the event of a failure) and an `NSError` object that describes why the operation failed (or nil if the operation was successful).
 */
- (void)appConfigurationWithDeviceID:(NSString *)deviceID
                       staplesUserID:(NSString *)staplesUserID
                               nonce:(NSString *)nonce
                          completion:(void (^)(BOTAppConfiguration *config, NSError *error))completion;

/**
 @abstract Login into SA.
 @param accountNumber An 'NSString' object representing the company ID of the user attempting to Login.
 @param userID An `NSString` object representing the User ID of the user attempting to Login.
 @param password An `NSString` object representing the password of the user attempting to Login.
 @param completion completion The block to execute upon completion of the asynchronous user Login operation.
 The block has no return value and accepts one arguments: An `NSError` object that describes why the operation failed (or nil if the
 operation was successful).
 */
- (void)loginSAtoEasySystem:(NSString *)accountNumber userID:(NSString *)userID password:(NSString *)password completion:(void (^)(NSDictionary *json, NSError *error))completion;

/**
 @abstract Executes an onboarding phase.
 @param phase An 'NSString' object representing an oboarding phase.
 @param conversationID An `NSString` object representing conversation to which onboarding message should be sent.
 @param completion completion The block to execute upon completion of the asynchronous operation.
 */
- (void)executeOnboardingPhase:(NSString *)phase payload:(NSDictionary *)payload completion:(void (^)(NSDictionary *json, NSError *error))completion;

@end


/**
 @abstract The `BOTAppConfiguration` class provides an single object for the identityToken response from the Easy System.
 */
@interface BOTAppConfiguration : NSObject

@property (readonly, nonatomic, strong) NSString *identityToken;
@property (readonly, nonatomic, strong) NSURL *conversationID;

+ (instancetype)credentialsWithIdentityToken:(NSString *)identityToken
                              conversationID:(NSURL *)conversationID;

@end
