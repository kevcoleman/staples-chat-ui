//
//  BOTLayerManager.h
//  Staples
//
//  Created by Kevin Coleman on 12/26/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BOTLayerManager : NSObject

+ (instancetype)managerWithAppID:(NSURL *)appID;

- (void)createConversationWithParticipants:(NSArray *)participants
                                completion:(void(^)(NSDictionary *data, NSError *error))completion;

- (void)sendMessageText:(NSString *)message
         toConversation:(NSString *)conversation
               asSender:(NSString *)sender
             completion:(void(^)(NSDictionary *data, NSError *error))completion;

- (void)sendMessageParts:(NSArray *)parts
          toConversation:(NSString *)conversation
                asSender:(NSString *)sender
              completion:(void(^)(NSDictionary *data, NSError *error))completion;

@end
