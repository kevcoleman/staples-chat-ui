//
//  BOTOnboardingManager.h
//  Staples
//
//  Created by Kevin Coleman on 12/26/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BOTLayerManager.h"

typedef NS_ENUM(NSUInteger, BOTOnboardingPhase) {
    BOTOnboardingPhaseWelcome,
    BOTOnboardingPhasePowerUp,
    BOTOnboardingPhaseScanButton,
    BOTOnboardingPhaseNameButton,
    BOTOnboardingPhaseNameConfirmation,
};

typedef NS_ENUM(NSUInteger, BOTOnboardingAction) {
    BOTOnboardingActionMore,
    BOTOnboardingActionLaunchCamera,
    BOTOnboardingActionNameButton,
    BOTOnboardingActionConfirm,
    BOTOnboardingActionDeny,
};

extern NSString *const BOTOnboardingActionNotification;
extern NSString *const BOTOnboardingActionKey;
extern NSString *const BOTOnboardingButtonNameKey;

@interface BOTOnboardingManager : NSObject

+ (id)managerWithLayerManager:(BOTLayerManager *)manager conversationID:(NSURL *)conversationID;

@property (nonatomic) BOTOnboardingPhase currentPhase;

- (void)executeOnboardingPhase:(NSString *)phase data:(NSDictionary *)data completion:(void(^)(BOOL success, NSError *))completion;

- (void)updateButtonName:(NSString *)buttonName;

@end
