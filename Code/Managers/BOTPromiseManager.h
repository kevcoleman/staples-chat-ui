//
//  BOTPromiseManager.h
//  Staples
//
//  Created by Kevin Coleman on 1/12/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PromiseKit/AnyPromise.h>
#import <LayerKit/LayerKit.h>
#import "BOTEasyServiceManager.h"

@interface BOTPromiseManager : NSObject

/**
 Attempts to retreive a auth-nonce from Layer's backend
 
 @return Promise(NSString *)nonce
 */
+ (AnyPromise *)noncePromiseWithClient:(LYRClient *)client;

/**
 Attempts to connect to the easy system to
 authticate the nonce received from layer
 
 @param deviceID        Device ID
 @param userID          UserID of the layer user
 @param staplesUserID   Staples User ID
 @param nonce           Nonce provided by Layer to sign by our backend
 @param client          Layer Client (Optional)
 @param manager         Service Manager
 @return Promise(BOTAppConfiguration *)config
 */
+ (AnyPromise *)appConfigurationPromiseWithDeviceID:(NSString *)deviceID
                                      staplesUserID:(NSString *)staplesUserID
                                              nonce:(NSString *)nonce
                                             client:(LYRClient *)client
                                     serviceManager:(BOTEasyServiceManager *)manager;

/**
 Sends our signed JWT to layer for authtication
 
 @param deviceID        Device ID
 @param identityToken   Signed token by our backend
 @return Promise(LYRIdentity *)authenticatedUser
 */
+ (AnyPromise *)authenticationPromiseWithDeviceID:(NSString *)userID IdentityToken:(NSString *)identityToken client:(LYRClient *)client;

/**
 Ensures the given conversation ID is synchronized with this client
 
 @param conversationID Layer Conversation ID
 @return Promise(LYRConversation *)conversation
 */
+ (AnyPromise *)coversationPromiseWithID:(NSURL *)conversationID layerIdentity:(LYRIdentity *)identity client:(LYRClient *)client;

/**
 Deauthenticates the user
 
 @return Promise
 */
+ (AnyPromise *)deauthenticationPromiseWithClient:(LYRClient *)client;

@end
