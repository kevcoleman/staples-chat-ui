//
//  BOTOnboardingButtonCollectionViewCell.h
//  Staples
//
//  Created by Kevin Coleman on 12/27/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Atlas/Atlas.h>
#import "BOTOnboardingManager.h"

@interface BOTOnboardingButtonCollectionViewCell : UICollectionViewCell <ATLMessagePresenting>

@property (strong, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) IBOutlet UIButton *leftButton;

@property (strong, nonatomic) IBOutlet UIButton *rightButton;

+ (NSString *)reuseIdentifier;

+ (CGFloat)cellHeightForMessage:(LYRMessage *)message;

@end
