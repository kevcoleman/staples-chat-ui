//
//  BOTChatActionCollectionViewCell.h
//  Staples
//
//  Created by Kevin Coleman on 10/31/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BOTChatActionCollectionViewCell;

extern NSString *const BOTCollectionViewFooterCellReuseIdentifier;

/**
 @abstract Cell presented with the BOTChatActionCollectionView to display relevant actions
 @discussion The corresponding action connected to each instance of the BOTChatActionCollectionViewCell is identified by it's title string. These strings are declared in BOTChatActionCollectionView.h and each title has a corresponding BOTChatAction enum value.
 */

@protocol BOTCollectionViewFooterCellDelegate <NSObject>

/**
 @abstract Sends the instance of the cell and it's title to the delegate
 @discussion The delegate will match the cell's title string to one of the constants defined in BOTChatActionCollectionView.h and pass the appropriate action along to its delegate (an instance of ATLConversationViewController) so it can be converted into a layer message
 */

- (void)collectionViewFooterCell:(BOTChatActionCollectionViewCell *)cell actionPressed:(NSString *)actionTitle;

@end

@interface BOTChatActionCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id<BOTCollectionViewFooterCellDelegate> delegate;

- (void)updateWithTitle:(NSString *)title;

+ (NSString *)reuseIdentifier;

+ (CGRect)sizeForCellWithText:(NSString *)text;

@end
