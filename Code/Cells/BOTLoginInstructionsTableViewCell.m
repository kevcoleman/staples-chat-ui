//
//  BOTLoginInstructionsTableViewCell.m
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTLoginInstructionsTableViewCell.h"
#import "BOTUtilities.h"

@interface BOTLoginInstructionsTableViewCell ()

@property (strong, nonatomic) IBOutlet UITextView *instructionsTextView;

@end

@implementation BOTLoginInstructionsTableViewCell

NSString * const BOTLoginInstructionsTableViewCellReuseIdentifier = @"BOTLoginInstructionsTableViewCellReuseIdentifier";

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.instructionsTextView.text = @"Your Easy Button makes managing your office... well, a lot easier. Log in below to get started with your Button, connected to your Staples Business Advantage account.";
    [self.instructionsTextView setFont:BOTLightFont(16.0f)];
    [self.instructionsTextView setTextColor:BOTInstructionGrayColor()];
    self.instructionsTextView.editable = NO;
    self.instructionsTextView.showsVerticalScrollIndicator = NO;
}

+ (CGFloat)cellHeight
{
    return 108;
}

+ (NSString *)reuseIdentifier
{
    return BOTLoginInstructionsTableViewCellReuseIdentifier;
}

@end
