//
//  BOTLogoutTableViewCell.m
//  iPhoneCFA
//
//  Created by Jayashree on 26/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import "BOTLogoutTableViewCell.h"
#import "BOTUtilities.h"

NSString *const BOTLogoutTableViewCellIdentifier = @"BOTLogoutTableViewCellIdentifier";

@interface BOTLogoutTableViewCell ()

@property (strong, nonatomic) IBOutlet UILabel *logoutLabel;
@property (strong, nonatomic) IBOutlet UIView *borderView;


@end

@implementation BOTLogoutTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureUI];
}

- (void)configureUI
{
    [self.logoutLabel setText:@"Log Out"];
    [self.logoutLabel setTextColor:BOTBlueColor()];
    [self.logoutLabel setFont:BOTRegularFont(12)];
    
    [self.borderView setBackgroundColor:BOTBorderGreyColor()];
}

#pragma mark - Public Methods

+ (CGFloat)cellHeight
{
    return 60;
}

+ (NSString *)reuseIdentifier
{
    return BOTLogoutTableViewCellIdentifier;
}

@end
