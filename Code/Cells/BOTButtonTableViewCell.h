//
//  BOTButtonTableViewCell.h
//  iPhoneCFA
//
//  Created by Jayashree on 26/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @abstract A simple table cell that contains a UIButton
 @discussion 'BOTButtonTableViewCell' passes itself and its button through to its delegate to allow for UI customization and let the reciever pass along the relevant action.
 */

@class BOTButtonTableViewCell;

@protocol BOTButtonTableViewCellDelegate
@required
- (void)buttonTableViewCell:(BOTButtonTableViewCell *)cell didTapActionButton:(UIButton *)button;

@end

@interface BOTButtonTableViewCell : UITableViewCell

@property (weak, nonatomic) id<BOTButtonTableViewCellDelegate> delegate;

- (void)updateTopLabelTitle:(NSString *)title;
- (void)updateButtonTitle:(NSString *)title;

+ (CGFloat)cellHeight;
+ (NSString *)reuseIdentifier;

@end
