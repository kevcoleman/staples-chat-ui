//
//  STRewardCollectionViewCell.m
//  Staples
//
//  Created by Kevin Coleman on 8/22/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTRewardCollectionViewCell.h"
#import "BOTReward.h"
#import "BOTUtilities.h"

NSString *const BOTRewardCollectionViewCellTitle= @"Reward Cell";
NSString *const BOTRewardCollectionViewCellReuseIdentifier = @"BOTRewardCollectionViewCellReuseIdentifier";

@interface BOTRewardCollectionViewCell ()

@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *memberTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *ammountLabel;
@property (strong, nonatomic) IBOutlet UILabel *rewardTypeLabel;
@property (strong, nonatomic) IBOutlet UIButton *viewButton;
@property (strong, nonatomic) IBOutlet UIImageView *barcodeImage;
@property (strong, nonatomic) IBOutlet UILabel *barcodeNumber;
@property (weak, nonatomic) IBOutlet UIButton *addToAppleWallet;
- (IBAction)didSelectAddtoAppleWallet:(id)sender;

@end

@implementation BOTRewardCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.view.layer.cornerRadius = 4.0f;
    self.view.layer.masksToBounds = NO;
    self.view.layer.borderWidth = 1.0f;
    self.view.layer.borderColor = BOTLightGrayColor().CGColor;
    
    [self.viewButton setTitle:@"View" forState:UIControlStateNormal];
    [self.viewButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    self.viewButton.hidden = NO;
    self.viewButton.userInteractionEnabled = NO;
    
    self.addToAppleWallet.tintColor = BOTBlueColor();
}

+ (NSString *)reuseIdentifier
{
    return BOTRewardCollectionViewCellReuseIdentifier;
}

+ (CGFloat)cellHeight
{
    return 288;
}

- (void)setReward:(BOTReward *)reward
{
    self.titleLabel.text = @"Staples Rewards \u00AE";
    self.nameLabel.text = reward.userName;
    self.memberTypeLabel.text = reward.memberType;
    self.ammountLabel.text = [NSString stringWithFormat:@"$%@", reward.totalAmount];
    self.rewardTypeLabel.text = @"Redeemable";
    self.barcodeNumber.text = [NSString stringWithFormat:@"#%@",reward.number];
    [self setBarCode:reward.number];
}

- (void) setBarCode:(NSString *)barCode
{
    if (barCode) {
        NSData *stringData = [barCode dataUsingEncoding:NSISOLatin1StringEncoding];
        CIFilter *qrFilter = [CIFilter filterWithName:@"CICode128BarcodeGenerator"];
        [qrFilter setValue:stringData forKey:@"inputMessage"];

        [EAGLContext setCurrentContext:nil];
        CIImage *outputImage = [qrFilter outputImage];
        UIImage *image = [UIImage imageWithCIImage:outputImage scale:5.0 orientation:UIImageOrientationUp];
        [self.barcodeImage setImage:image];
    }
}

- (IBAction)didSelectAddtoAppleWallet:(id)sender {
    if([self.delegate respondsToSelector:@selector(rewardCollectionViewCellDidSelectAddtoAppleWallet:)]){
        [self.delegate rewardCollectionViewCellDidSelectAddtoAppleWallet:self];
    }
}

@end
