//
//  BOTEasyButtonTableViewCell.h
//  iPhoneCFA
//
//  Created by Jayashree on 23/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BOTEasyButtonTableViewCell : UITableViewCell

- (void)updateButtonName:(NSString *)buttonName;

+ (CGFloat)cellHeight;

+ (NSString *)reuseIdentifier;

@end
