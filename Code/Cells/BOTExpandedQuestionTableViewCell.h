//
//  BOTExpandedQuestionTableViewCell.h
//  Staples
//
//  Created by Reid Weber on 1/26/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BOTExpandedQuestionTableViewCell;

/**
 @abstract 'BOTExpandedQuestionTableViewCellDelegate' sends the answer data to the reciever.
 @discussion The sender will contain the common answer data requested via the tap gesture the answer data will be sent up a delegate chain to the relevant listeners.
*/
@protocol BOTExpandedQuestionTableViewCellDelegate
@required
- (void)expandedQuestionCell:(BOTExpandedQuestionTableViewCell *)cell didTapActionButton:(UIButton *)sender;

@end

/**
 @abstract Simple UITableViewCell to display common answers.
 */
@interface BOTExpandedQuestionTableViewCell : UITableViewCell

@property (weak, nonatomic) id<BOTExpandedQuestionTableViewCellDelegate> delegate;

+ (NSString *)reuseIdentifier;
+ (CGFloat)cellHeight;

- (void)setButtonText:(NSString *)text;

@end
