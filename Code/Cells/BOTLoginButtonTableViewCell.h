//
//  BOTLoginButtonTableViewCell.h
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The `BOTLoginButtonTableViewCellDelegate` protocol provides for informing object when selections have been made in the `BOTLoginButtonTableViewCell`.
 */
@protocol BOTLoginButtonTableViewCellDelegate;

@interface BOTLoginButtonTableViewCell : UITableViewCell

+ (CGFloat)cellHeight;

+ (NSString *)reuseIdentifier;

/**
 Selection delegate for the `BOTLoginButtonTableViewCell`.
 */
@property (nonatomic, weak) id <BOTLoginButtonTableViewCellDelegate> delegate;

@end

@protocol BOTLoginButtonTableViewCellDelegate <NSObject>

@optional
- (void)loginButtonTableViewCellDidSelectLogin:(BOTLoginButtonTableViewCell *)cell;

@end
