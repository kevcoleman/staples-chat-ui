//
//  BOTChatActionCollectionViewCell.m
//  Staples
//
//  Created by Kevin Coleman on 10/31/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTChatActionCollectionViewCell.h"
#import "BOTActionButton.h"
#import "BOTUtilities.h"

NSString *const BOTCollectionViewFooterCellReuseIdentifier = @"BOTCollectionViewFooterCellReuseIdentifier";
CGFloat const BOTActionCollectionViewCellHeight = 36;
CGFloat const BOTActionCollectionViewCellPadding = 30;
CGFloat const BOTActionCollectionViewCellLeftPadding = 10;

@interface BOTChatActionCollectionViewCell ();

@property (nonatomic) UIButton *button;

@end

@implementation BOTChatActionCollectionViewCell

- (void)updateWithTitle:(NSString *)title
{
    CGRect buttonFrame = [[self class] sizeForCellWithText:title];
    [self setUpButtonWithFrame:buttonFrame andTitle:title];
}

+ (NSString *)reuseIdentifier
{
    return BOTCollectionViewFooterCellReuseIdentifier;
}

+ (CGRect)sizeForCellWithText:(NSString *)text
{
    CGSize titleSize = [text sizeWithAttributes:@{ NSFontAttributeName : BOTLightFont(14)}];
    CGFloat cellWidth = titleSize.width + (BOTActionCollectionViewCellPadding * 2) + BOTActionCollectionViewCellLeftPadding;
    return CGRectMake(0, 0, cellWidth, BOTActionCollectionViewCellHeight);
}

- (void)setUpButtonWithFrame:(CGRect)frame andTitle:(NSString *)title
{
    // Adjust frame for padding.
    frame.origin.x = BOTActionCollectionViewCellLeftPadding;
    frame.size.width -= BOTActionCollectionViewCellLeftPadding;
    
    if (!self.button) {
        self.button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [self.button setFrame:frame];
        [self.button.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.button setTitleColor:BOTRedColor() forState:UIControlStateNormal];
        [self.button setBackgroundColor:[UIColor clearColor]];
        [self.button.titleLabel setFont:BOTLightFont(14)];
        
        [self.button layer].cornerRadius = self.frame.size.height/2;
        [[self.button layer] setBorderColor:BOTRedColor().CGColor];
        [[self.button layer] setBorderWidth:1.0];
    
        [self.button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:self.button];
    }
    
    [self.button setFrame:frame];
    [self.button setTitle:title forState:UIControlStateNormal];
}

- (void)buttonPressed:(UIButton *)sender
{
    if ([(NSObject *)self.delegate respondsToSelector:@selector(collectionViewFooterCell:actionPressed:)]) {
        [self.delegate collectionViewFooterCell:self actionPressed:self.button.titleLabel.text];
    }
}

- (void)configureButtonConstraints
{
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.button attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.button attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.button attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.button attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0]];
}

@end
