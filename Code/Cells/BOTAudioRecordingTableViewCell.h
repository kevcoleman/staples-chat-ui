//
//  BOTAudioRecordingCollectionViewCell.h
//  Staples
//
//  Created by Taylor Halliday on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BOTAudioRecording.h"
#import "BOTAudioRecordingSummaryView.h"

@protocol BOTAudioRecordingTableViewCellDelegate <BOTAudioRecordingSummaryViewDelegate>

- (void)audioRecording:(BOTAudioRecording *)audioRecording feedbackWasTappedWithApproval:(BOOL)approval;

@end

@interface BOTAudioRecordingTableViewCell : UITableViewCell

@property (nonatomic, strong) BOTAudioRecording *audioRecording;
@property (nonatomic, strong) id <BOTAudioRecordingTableViewCellDelegate> delegate;

/**
 Reuse Identifier
 */
+ (NSString *)reuseIdentifier;

/**
 Cell Height
 */
+ (CGFloat)cellHeight;

@end
