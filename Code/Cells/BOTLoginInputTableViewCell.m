//
//  BOTLoginInputTableViewCell.m
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTLoginInputTableViewCell.h"
#import "BOTUtilities.h"

@interface BOTLoginInputTableViewCell ()

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
@property (strong, nonatomic) IBOutlet UIButton *leftButton;

@end

@implementation BOTLoginInputTableViewCell

NSString * const BOTLoginInputTableViewCellReuseIdentifier = @"BOTLoginInputTableViewCellReuseIdentifier";

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.rightButton setTitleColor:BOTDarkBlueColor() forState:UIControlStateNormal];
    [self.rightButton.titleLabel setFont:BOTRegularFont(12.0f)];
    [self.rightButton addTarget:self action:@selector(rightButtonWasTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.leftButton setTitleColor:BOTDarkBlueColor() forState:UIControlStateNormal];
    [self.leftButton.titleLabel setFont:BOTRegularFont(12.0f)];
    [self.leftButton addTarget:self action:@selector(leftButtonWasTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.inputTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [self.inputTextField.layer setBorderWidth:1.0];
    [self.inputTextField.layer setCornerRadius:10.0f];
    [self.inputTextField.layer setBorderColor:BOTBorderGreyColor().CGColor];
}

- (void)updateTitleLabel:(NSString *)titleText
{
    [self.titleLabel setText:titleText];
}

- (void)updateWithPlaceHolder:(NSString *)placeHolder
{
    NSDictionary *attributes = @{NSForegroundColorAttributeName: BOTBorderGreyColor(), NSFontAttributeName: BOTRegularFont(16.0f)};
    NSAttributedString *attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolder attributes:attributes];
    [self.inputTextField setAttributedPlaceholder:attributedPlaceholder];
}

- (void)updateWithLeftButtonText:(NSString *)leftButtonText enabled:(BOOL)enabled
{
    if([leftButtonText isEqualToString:@"Password is case sensitive"]){
       [self.leftButton setTitleColor:BOTBorderGreyColor() forState:UIControlStateNormal];  
    }
    [self.leftButton setTitle:leftButtonText forState:UIControlStateNormal];
    if (enabled) {
        self.leftButton.enabled = NO;
    }
    
    if (leftButtonText.length == 0) {
        self.leftButton.hidden = YES;
    }
}

- (void)updateWithRightButtonText:(NSString *)rightButtonText
{
    [self.rightButton setTitle:rightButtonText forState:UIControlStateNormal];
}

+ (CGFloat)cellHeight
{
    return 86;
}

+ (NSString *)reuseIdentifier
{
    return BOTLoginInputTableViewCellReuseIdentifier;
}

#pragma mark - Action Handlers

- (void)leftButtonWasTapped:(UIButton *)sender
{
    
}

- (void)rightButtonWasTapped:(UIButton *)sender
{
    
}

@end
