//
//  BOTLoginHeroImageTableViewCell.h
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The `BOTLoginHeroImageTableViewCellDelegate` protocol provides for informing object when selections have been made in the `BOTLoginHeroImageTableViewCell`.
 */
@protocol BOTLoginHeroImageTableViewCellDelegate;

@interface BOTLoginHeroImageTableViewCell : UITableViewCell

+ (CGFloat)cellHeight;

+ (NSString *)reuseIdentifier;

/**
 Selection delegate for the `BOTLoginButtonTableViewCell`.
 */
@property (nonatomic, weak) id <BOTLoginHeroImageTableViewCellDelegate> delegate;

@end

@protocol BOTLoginHeroImageTableViewCellDelegate <NSObject>

@optional
- (void)loginHeroImageTableViewCellDidSelectClose:(BOTLoginHeroImageTableViewCell *)cell;

@end
