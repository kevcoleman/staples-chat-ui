//
//  BOTAddedAnswerTableViewCell.m
//  Staples
//
//  Created by Reid Weber on 2/1/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTAddedAnswerTableViewCell.h"

NSString *const BOTAddedAnswerTableViewCellIdentifier = @"BOTAddedAnswerTableViewCellIdentifier";

@interface BOTAddedAnswerTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UIButton *editButton;

@end

@implementation BOTAddedAnswerTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setAnswerLabelText:(NSString *)text
{
    [self.answerLabel setText:text];
}

- (IBAction)editButtonPressed:(UIButton *)sender
{
    if ([(NSObject *)self.delegate respondsToSelector:@selector(addedAnswerTableViewCell:didTapEditButton:)]) {
        [self.delegate addedAnswerTableViewCell:self didTapEditButton:sender];
    }
}

+ (NSString *)reuseIdentifier
{
    return BOTAddedAnswerTableViewCellIdentifier;
}

@end
