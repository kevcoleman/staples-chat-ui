//
//  BOTLogoutTableViewCell.h
//  iPhoneCFA
//
//  Created by Jayashree on 26/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BOTLogoutTableViewCell : UITableViewCell

+ (CGFloat)cellHeight;

+ (NSString *)reuseIdentifier;

@end
