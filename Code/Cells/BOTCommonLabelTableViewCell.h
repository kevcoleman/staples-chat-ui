//
//  BOTCommonLabelTableViewCell.h
//  Staples
//
//  Created by Reid Weber on 1/26/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @abstract A simple UITableViewCell for titles
 @discussion Very simiar to the standard UITableViewCell but allows for more customization via the .xib file.
 */
@interface BOTCommonLabelTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mainLabel;

+ (NSString *)reuseIdentifier;
+ (CGFloat)cellHeight;

@end
