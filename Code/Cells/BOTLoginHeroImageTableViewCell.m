//
//  BOTLoginHeroImageTableViewCell.m
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTLoginHeroImageTableViewCell.h"
#import "BOTUtilities.h"

@interface BOTLoginHeroImageTableViewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *heroImageView;

- (IBAction)didSelectClose:(id)sender;

@end

@implementation BOTLoginHeroImageTableViewCell

NSString * const BOTLoginHeroImageTableViewCellReuseIdentifier = @"BOTLoginHeroImageTableViewCellReuseIdentifier";

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.heroImageView.image = [UIImage imageNamed:@"easyButton" inBundle:StaplesUIBundle() compatibleWithTraitCollection:nil];
}

+ (CGFloat)cellHeight
{
    return 192;
}

+ (NSString *)reuseIdentifier
{
    return BOTLoginHeroImageTableViewCellReuseIdentifier;
}

- (IBAction)didSelectClose:(id)sender {
    if([self.delegate respondsToSelector:@selector(loginHeroImageTableViewCellDidSelectClose:)]){
        [self.delegate loginHeroImageTableViewCellDidSelectClose:self];
    }
}
@end
