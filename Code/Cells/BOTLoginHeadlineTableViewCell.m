//
//  BOTLoginHeadlineTableViewCell.m
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTLoginHeadlineTableViewCell.h"
#import "BOTUtilities.h"

@interface BOTLoginHeadlineTableViewCell ()

@property (strong, nonatomic) IBOutlet UITextField *headlineTextField;

@end

@implementation BOTLoginHeadlineTableViewCell

NSString * const BOTLoginHeadlineTableViewCellReuseIdentifier = @"BOTLoginHeadlineTableViewCellReuseIdentifier";

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.headlineTextField.text = @"Let's start up your Easy Button.";
    [self.headlineTextField setFont:BOTLightFont(20.0f)];
    [self.headlineTextField setTextColor:BOTHeadlineGrayColor()];
    self.headlineTextField.enabled = NO;
}

+ (CGFloat)cellHeight
{
    return 46;
}

+ (NSString *)reuseIdentifier
{
    return BOTLoginHeadlineTableViewCellReuseIdentifier;
}

@end
