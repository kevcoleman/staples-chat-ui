//
//  BOTAudioRecordingTableViewCell.m
//  Staples
//
//  Created by Taylor Halliday on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTAudioRecordingTableViewCell.h"
#import "BOTAudioRecordingSummaryView.h"
#import <EDColor/EDColor.h>

NSString *const BOTAudioRecordgingCollectionViewCellReuseIdentifier = @"BOTAudioRecordgingCollectionViewCellReuseIdentifier";
NSString *const BOTAudioRecordingCellBGColor = @"#4E92DF";
NSUInteger const BOTAudioRecordingRevealOffset = 100.0f;

@interface BOTAudioRecordingTableViewCell()

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) BOTAudioRecordingSummaryView *summaryView;

@end

@implementation BOTAudioRecordingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor colorWithHexString:BOTAudioRecordingCellBGColor];
    
    // Layout ScrollView and Summary View
    [self layoutScrollView];
    [self layoutSummaryView];
}

+ (NSString *)reuseIdentifier
{
    return BOTAudioRecordgingCollectionViewCellReuseIdentifier;
}

+ (CGFloat)cellHeight
{
    return 150.0f;
}

#pragma mark - Setters / Getters

- (void)setAudioRecording:(BOTAudioRecording *)audioRecording
{
    if (_audioRecording != audioRecording) {
        _audioRecording = audioRecording;
        [self.summaryView setAudioRecording:audioRecording];
    }
}

- (void)setDelegate:(id<BOTAudioRecordingTableViewCellDelegate>)delegate
{
    _delegate = delegate;
    self.summaryView.delegate = _delegate;
}

#pragma mark - ScrollView Layout and Adjustments

- (void)layoutScrollView
{
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self layoutScrollViewConstraints];
}

- (void)layoutSummaryView
{
    self.summaryView = [[BOTAudioRecordingSummaryView alloc] initWithFrame:self.bounds];
    self.summaryView.translatesAutoresizingMaskIntoConstraints = NO;
    self.summaryView.delegate = self.delegate;
    [self.scrollView addSubview:self.summaryView];
    [self layoutSummaryViewConstraints];
}

- (void)adjustScrollViewContentArea
{
    CGSize contentSize = CGSizeMake(self.bounds.size.width + BOTAudioRecordingRevealOffset, self.bounds.size.height);
    self.scrollView.contentSize = contentSize;
}

#pragma mark - Layout Overrides

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self adjustScrollViewContentArea];
}

#pragma mark - Layout Constraints

- (void)layoutScrollViewConstraints
{
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.scrollView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.scrollView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.scrollView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.scrollView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [self addConstraints:@[trailing, top, leading, bottom]];
}

- (void)layoutSummaryViewConstraints
{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.summaryView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.summaryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.summaryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.summaryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [self addConstraints:@[width, top, leading, bottom]];
}

@end
