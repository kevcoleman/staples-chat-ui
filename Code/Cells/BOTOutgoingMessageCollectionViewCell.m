//
//  BOTOutgoingMessageColectionViewCell.m
//  Staples
//
//  Created by Reid Weber on 1/24/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTOutgoingMessageCollectionViewCell.h"
#import "BOTOutgoingMessageBubbleView.h"
#import "BOTUtilities.h"

NSString *const BOTOutgoingMessageCellIdentifier = @"BOTOutgoingMessageCellIdentifier";

@interface ATLBaseCollectionViewCell()

- (void)configureLayoutConstraints;

@property (nonatomic) NSLayoutConstraint *bubbleWithAvatarLeadConstraint;
@property (nonatomic) NSLayoutConstraint *bubbleWithoutAvatarLeadConstraint;
@property (nonatomic) NSLayoutConstraint *bubbleViewWidthConstraint;

@end

@interface BOTOutgoingMessageCollectionViewCell ()

@property (strong, nonatomic) UIImageView *drawingImageView;

@end

@implementation BOTOutgoingMessageCollectionViewCell

@synthesize bubbleView             = _bubbleView;
@synthesize bubbleViewColor        = _bubbleViewColor;
@synthesize bubbleViewCornerRadius = _bubbleViewCornerRadius;
@synthesize avatarImageView        = _avatarImageView;

@synthesize bubbleWithAvatarLeadConstraint    = _bubbleWithAvatarLeadConstraint;
@synthesize bubbleWithoutAvatarLeadConstraint = _bubbleWithoutAvatarLeadConstraint;

+ (CGFloat)cellHeightForMessage:(LYRMessage *)message inView:(UIView *)view
{
    CGFloat superHeight = [super cellHeightForMessage:message inView:view];
    return superHeight + (1.5 * BOTBubbleSpikeHeight);
}

- (void)lyr_baseInit
{
    // Default UIAppearance
    _bubbleViewColor = ATLBlueColor();
    _bubbleViewCornerRadius = 0.0f;
    _bubbleViewColor = [UIColor clearColor];
    
    CGRect frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height - (BOTBubbleSpikeHeight * 1.5));
    
    _bubbleView = [[BOTOutgoingMessageBubbleView alloc] initWithFrame:frame];
    _bubbleView.translatesAutoresizingMaskIntoConstraints = NO;
    _bubbleView.layer.cornerRadius = _bubbleViewCornerRadius;
    _bubbleView.backgroundColor = _bubbleViewColor;
    [self.contentView addSubview:_bubbleView];
    
    _avatarImageView = [[ATLAvatarImageView alloc] init];
    _avatarImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:_avatarImageView];
    [self configureLayoutConstraints];
}

- (void)configureLayoutConstraints
{
    _bubbleWithAvatarLeadConstraint = [NSLayoutConstraint new];
    _bubbleWithoutAvatarLeadConstraint = [NSLayoutConstraint new];
    
    CGFloat maxBubbleWidth = ATLMaxCellWidth() + ATLMessageBubbleLabelHorizontalPadding * 2;
    self.bubbleViewWidthConstraint = [NSLayoutConstraint constraintWithItem:self.bubbleView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:maxBubbleWidth];
    [self.contentView addConstraint:self.bubbleViewWidthConstraint];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.bubbleView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.bubbleView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.avatarImageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self setNeedsDisplay];
    [self.bubbleView setNeedsDisplay];
}

+ (NSString *)reuseIdentifier
{
    return BOTOutgoingMessageCellIdentifier;
}

@end
