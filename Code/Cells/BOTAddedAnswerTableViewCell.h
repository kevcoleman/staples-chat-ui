//
//  BOTAddedAnswerTableViewCell.h
//  Staples
//
//  Created by Reid Weber on 2/1/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BOTAddedAnswerTableViewCell;

/**
 @abstract 'BOTAddedAnswerTableViewCellDelegate' lets the reciever know when the edit button was pressed.
 @discussion This is a simple tableviewcell for displaying custom answers added by the user. The cell has an edit button to change the Answer data. The cell also supports swipe to delete.
 */
@protocol BOTAddedAnswerTableViewCellDelegate

- (void)addedAnswerTableViewCell:(BOTAddedAnswerTableViewCell *)cell didTapEditButton:(UIButton *)sender;

@end

/**
 @abstract A simple UITableViewCell that contains custom answer data added by the user.
 */
@interface BOTAddedAnswerTableViewCell : UITableViewCell

@property (weak, nonatomic) id<BOTAddedAnswerTableViewCellDelegate> delegate;
- (void)setAnswerLabelText:(NSString *)text;
+ (NSString *)reuseIdentifier;

@end
