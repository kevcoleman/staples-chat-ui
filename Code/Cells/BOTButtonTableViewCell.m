//
//  BOTButtonTableViewCell.m
//  iPhoneCFA
//
//  Created by Jayashree on 26/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import "BOTButtonTableViewCell.h"
#import "BOTUtilities.h"

NSString *const BOTButtonTableViewCellIdentifier = @"BOTButtonTableViewCellIdentifier";

@interface BOTButtonTableViewCell ()

@property (strong, nonatomic) IBOutlet UILabel *instructionLabel;
@property (strong, nonatomic) IBOutlet UIButton *addButton;

@end
@implementation BOTButtonTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureUI];
}

- (void)configureUI
{
    [self.instructionLabel setText:@"  Need to add another? It's easy."];
    [self.instructionLabel setFont:BOTRegularFont(14)];
    [self.instructionLabel setTextColor:BOTInstructionGrayColor()];
    
    [self.addButton setTitle:@"Add New Button" forState:UIControlStateNormal];
    [self.addButton setBackgroundColor:BOTBlueColor()];
    [self.addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.addButton.titleLabel setFont:BOTBoldFont(16)];
    [self.addButton addTarget:self action:@selector(didTapAddButton:) forControlEvents:UIControlEventTouchUpInside];
    self.addButton.layer.cornerRadius = 10.0f;
}

#pragma mark - Public Methods

+ (CGFloat)cellHeight
{
    return 86;
}

+ (NSString *)reuseIdentifier
{
    return BOTButtonTableViewCellIdentifier;
}

- (void)updateButtonTitle:(NSString *)title
{
    [self.instructionLabel setText:title];
}

- (void)updateTopLabelTitle:(NSString *)title
{
    [self.addButton setTitle:title forState:UIControlStateNormal];
}

#pragma mark - Actions

- (void)didTapAddButton:(UIButton *)sender
{
    if ([(NSObject *)self.delegate respondsToSelector:@selector(buttonTableViewCell:didTapActionButton:)]) {
        [self.delegate buttonTableViewCell:self didTapActionButton:sender];
    }
}

@end
