//
//  BOTEasyButtonTableViewCell.m
//  iPhoneCFA
//
//  Created by Jayashree on 23/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import "BOTEasyButtonTableViewCell.h"
#import "BOTUtilities.h"
#import "BOTEasyButtonIconView.h"

@interface BOTEasyButtonTableViewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *buttonImageView;
@property (strong, nonatomic) IBOutlet UILabel *buttonNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *editIconImageView;
@property (nonatomic, strong) BOTEasyButtonIconView *icon;

@end

@implementation BOTEasyButtonTableViewCell

NSString * const BOTEasyButtonTableViewCellIdentifier = @"BOTEasyButtonTableViewCellIdentifier";

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureUI];
}

- (void)configureUI
{
    self.icon = [BOTEasyButtonIconView initWithSize:24];
    self.icon.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:self.icon];
    
    self.buttonImageView.backgroundColor = [UIColor clearColor];
    self.buttonNameLabel.font = BOTRegularFont(20);
    self.buttonNameLabel.textColor = [UIColor darkGrayColor];
    
    self.editIconImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.editIconImageView setImage:[UIImage imageNamed:@"pencil"]];
    [self configureConstraints];
}

- (void)configureConstraints
{
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.icon attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.buttonImageView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.icon attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.buttonImageView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
}
#pragma mark - Public Methods

- (void)updateButtonName:(NSString *)buttonName
{
    self.buttonNameLabel.text = buttonName;
}

+ (CGFloat)cellHeight
{
    return 60;
}

+ (NSString *)reuseIdentifier
{
    return BOTEasyButtonTableViewCellIdentifier;
}

- (void)setSelected:(BOOL)selected
{
    
}

@end
