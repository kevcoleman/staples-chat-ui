//
//  BOTMyCustomAnswersTableViewCell.h
//  Staples
//
//  Created by Reid Weber on 1/26/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BOTMyCustomAnswersTableViewCell, BOTAddedAnswerTableViewCell;

/**
 @abstract 'BOTMyCustomAnswersTableViewCellDelegate' notifies recievers of user gestures and changes in the owner's table view
 @discussion The customAnswersTableViewCellDeletedTableViewRow: is so the reciever can adjust the height of the BOTMyCustomAnswersTableViewCell being used in the reciever's table view
 */
@protocol BOTMyCustomAnswersTableViewCellDelegate

- (void)customAnswersTableViewCell:(BOTMyCustomAnswersTableViewCell *)cell answerCellSelectedForEdit:(BOTAddedAnswerTableViewCell *)answerCell;
- (void)customAnswersTableViewCell:(BOTMyCustomAnswersTableViewCell *)cell answerCellSelected:(BOTAddedAnswerTableViewCell *)answerCell;
- (void)customAnswersTableViewCellDeletedTableViewRow:(BOTMyCustomAnswersTableViewCell *)cell;

@end

/**
 @abstract 'BOTMyCustomAnswersTableViewCell' a UITableViewCell that contains it's own UITableView that displays al of the Users Custom Answers
 @discussion This class will ping the server to find all of a given user's custom answers and coordinate with the local Core Data store to display the latest collection of custom answers.
 */
@interface BOTMyCustomAnswersTableViewCell : UITableViewCell

@property (weak, nonatomic) id<BOTMyCustomAnswersTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *customAnswersLabel;
@property (weak, nonatomic) IBOutlet UILabel *noCustomAnswersLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;

/**
 Class methods for returning the reuseIdentifier and the starting height of the cell. The starting height is the height of the main label and the height of the "No Custom Answers Label". the table view is also hidden;
 */
+ (NSString *)reuseIdentifier;
+ (CGFloat)startingCellHeight;

/**
 Public methods to control the custom answer array, get the total number of answers, and get the current height based on how many answers are there.
 */
- (CGFloat)cellHeight;
- (NSUInteger)numberOfAnswers;
- (void)addNewAnswer:(NSString *)answer;
- (void)updateAnswersLabel:(NSString *)text;

@end
