//
//  BOTOutgoingMessageColectionViewCell.h
//  Staples
//
//  Created by Reid Weber on 1/24/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <Atlas/Atlas.h>

/**
 @abstract Overrides ATLOutgoingMessageCollectionViewCell to allow custom drawing on the bubbleView property.
 @discussion This class overrides lyr_baseInit and configureLayoutConstraints from ATLOutgoingMessageCollectionViewCell to allow the frame used for it's bubleView to be shorter than its own frame. This allows the space for the custom drawing that happens in BOTOutgoingMessageBubbleView
 */

@interface BOTOutgoingMessageCollectionViewCell : ATLOutgoingMessageCollectionViewCell

+ (NSString *)reuseIdentifier;
+ (CGFloat)cellHeightForMessage:(LYRMessage *)message inView:(UIView *)view;

@end
