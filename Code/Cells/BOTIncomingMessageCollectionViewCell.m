//
//  BOTIncomingMessageCollectionViewCell.m
//  Staples
//
//  Created by Reid Weber on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTIncomingMessageCollectionViewCell.h"
#import "BOTParticipant.h"

NSString *const BOTIncomingMessageCellIdentifier = @"BOTIncomingMessageCellIdentifier";

@interface ATLBaseCollectionViewCell()

@property (nonatomic) BOOL shouldDisplayAvatar;

- (void)configureLayoutConstraints;

@property (nonatomic) NSLayoutConstraint *bubbleWithAvatarLeadConstraint;
@property (nonatomic) NSLayoutConstraint *bubbleWithoutAvatarLeadConstraint;
@property (nonatomic) NSLayoutConstraint *bubbleViewWidthConstraint;

@end

@implementation BOTIncomingMessageCollectionViewCell

@synthesize bubbleView             = _bubbleView;
@synthesize bubbleViewColor        = _bubbleViewColor;
@synthesize bubbleViewCornerRadius = _bubbleViewCornerRadius;
@synthesize avatarImageView        = _avatarImageView;

+ (NSString *)reuseIdentifier
{
    return BOTIncomingMessageCellIdentifier;
}

- (void)lyr_baseInit
{
    // Default UIAppearance
    _bubbleViewColor = ATLBlueColor();
    _bubbleViewCornerRadius = 0.0f;
    _bubbleViewColor = [UIColor clearColor];
    
    _bubbleView = [[ATLMessageBubbleView alloc] init];
    _bubbleView.translatesAutoresizingMaskIntoConstraints = NO;
    _bubbleView.layer.cornerRadius = _bubbleViewCornerRadius;
    _bubbleView.backgroundColor = _bubbleViewColor;
    [self.contentView addSubview:_bubbleView];
    
    _avatarImageView = [[ATLAvatarImageView alloc] init];
    _avatarImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [_avatarImageView setImageViewBackgroundColor:[UIColor clearColor]];
    [_avatarImageView setContentMode:UIViewContentModeScaleAspectFit];
    
    [self.contentView addSubview:_avatarImageView];
    [self configureLayoutConstraints];
}

- (void)configureLayoutConstraints
{
    CGFloat maxBubbleWidth = ATLMaxCellWidth() + ATLMessageBubbleLabelHorizontalPadding * 2;
    self.bubbleViewWidthConstraint = [NSLayoutConstraint constraintWithItem:self.bubbleView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:maxBubbleWidth];
    [self.contentView addConstraint:self.bubbleViewWidthConstraint];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.bubbleView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.bubbleView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.avatarImageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
}

- (void)updateWithSender:(id<ATLParticipant>)sender
{
    if (sender) {
        BOTParticipant *participant = [BOTParticipant participantWithSender:sender];
        [super updateWithSender:participant];
    }
}

- (void)configureCellForType:(ATLCellType)cellType
{
    if ([self.contentView.constraints containsObject:self.bubbleWithAvatarLeadConstraint]) {
        [self.contentView removeConstraint:self.bubbleWithAvatarLeadConstraint];
    }
    
    if ([self.contentView.constraints containsObject:self.bubbleWithoutAvatarLeadConstraint]) {
        [self.contentView removeConstraint:self.bubbleWithoutAvatarLeadConstraint];
    }
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.avatarImageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0f]];
    self.bubbleWithAvatarLeadConstraint = [NSLayoutConstraint constraintWithItem:self.bubbleView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.avatarImageView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0f];
    [self.contentView addConstraint:self.bubbleWithAvatarLeadConstraint];
    self.bubbleWithoutAvatarLeadConstraint = [NSLayoutConstraint constraintWithItem:self.bubbleView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:ATLMessageCellHorizontalMargin];
    [self shouldDisplayAvatarItem:self.shouldDisplayAvatar];
}

@end
