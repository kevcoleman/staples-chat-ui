//
//  BOTMyCustomAnswersTableViewCell.m
//  Staples
//
//  Created by Reid Weber on 1/26/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTMyCustomAnswersTableViewCell.h"
#import "BOTAddedAnswerTableViewCell.h"

CGFloat const startingCellHeight   = 20;
CGFloat const noAnswersLabelHeight = 50;
CGFloat const answerCellHeight     = 30;

NSString *const BOTMyCustomAnswersTableViewCellIdentifier = @"BOTMyCustomAnswersTableViewCellIdentifier";

@interface BOTMyCustomAnswersTableViewCell () <UITableViewDelegate, UITableViewDataSource, BOTAddedAnswerTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *myAnswers;

@end

@implementation BOTMyCustomAnswersTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.myAnswers = [NSMutableArray new];
    
    UINib *cellNib = [UINib nibWithNibName:@"BOTAddedAnswerTableViewCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[BOTAddedAnswerTableViewCell reuseIdentifier]];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    if ([self.myAnswers count] <= 0) {
        self.tableView.hidden = true;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

#pragma mark - UITableViewDataSource and UiTableViewDelegateMethods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOTAddedAnswerTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[BOTAddedAnswerTableViewCell reuseIdentifier]];
    cell.delegate = self;
    [cell setAnswerLabelText:[self.myAnswers objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return answerCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([(NSObject *)self.delegate respondsToSelector:@selector(customAnswersTableViewCell:answerCellSelected:)]) {
        [self.delegate customAnswersTableViewCell:self answerCellSelected:[self.tableView cellForRowAtIndexPath:indexPath]];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.myAnswers count];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.myAnswers removeObjectAtIndex:indexPath.row];
        
        if ([self.myAnswers count] <= 0) {
            tableView.hidden = true;
        }
        
        [tableView reloadData];
        
        if ([(NSObject *)self.delegate respondsToSelector:@selector(customAnswersTableViewCellDeletedTableViewRow:)]) {
            [self.delegate customAnswersTableViewCellDeletedTableViewRow:self];
        }
    }
}

#pragma mark - Public Methods

- (void)updateAnswersLabel:(NSString *)text
{
    [self.noCustomAnswersLabel setText:text];
}

- (NSUInteger)numberOfAnswers
{
    return [self.myAnswers count];
}

- (void)addNewAnswer:(NSString *)answer
{
    if ([self.myAnswers count] <= 0) {
        self.tableView.hidden = false;
    }
    [self.myAnswers addObject:answer];
    [self.tableView reloadData];
}

- (void)addedAnswerTableViewCell:(BOTAddedAnswerTableViewCell *)cell didTapEditButton:(UIButton *)sender
{
    if ([(NSObject *)self.delegate respondsToSelector:@selector(customAnswersTableViewCell:answerCellSelectedForEdit:)]) {
        [self.delegate customAnswersTableViewCell:self answerCellSelectedForEdit:cell];
    }
}

+ (CGFloat)startingCellHeight
{
    return startingCellHeight + noAnswersLabelHeight;
}

- (CGFloat)cellHeight
{
    return startingCellHeight + ([self.myAnswers count] * answerCellHeight);
}

+ (NSString *)reuseIdentifier
{
    return BOTMyCustomAnswersTableViewCellIdentifier;
}

@end
