//
//  BOTOnboardingCollectionViewCell.m
//  Staples
//
//  Created by Kevin Coleman on 12/27/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTOnboardingCollectionViewCell.h"
#import "BOTUtilities.h"
#import "UIImageView+WebCache.h"
#import "BOTOnboardingInputView.h"

NSUInteger const BOTCardCellWidth = 260;

CGSize BOTPlainTextSize(NSString *text, NSDictionary *attributes)
{
    UITextView *textView = [UITextView new];
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:attributes];
    [textView setAttributedText:attributedText];
    
    return [textView sizeThatFits:CGSizeMake(BOTCardCellWidth, MAXFLOAT)];
}

NSDictionary *BOTInstructionTextAttributes()
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineHeightMultiple = 20.0f;
    paragraphStyle.maximumLineHeight = 20.0f;
    paragraphStyle.minimumLineHeight = 20.0f;
    
    return @{
             NSFontAttributeName : [UIFont systemFontOfSize:16 weight:UIFontWeightLight],
             NSParagraphStyleAttributeName : paragraphStyle,
             NSForegroundColorAttributeName: BOTChatTextGrayColor(),
             };
}

@interface BOTOnboardingCollectionViewCell () <BOTOnboardingInputViewDelegate>

@property (nonnull) UIButton *onboardingActionButton;
@property (nonnull) BOTOnboardingInputView *inputView;

@end

@implementation BOTOnboardingCollectionViewCell

NSString *const BOTOnboardingCollectionViewCellReuseIdentifier = @"BOTOnboardingCollectionViewCellReuseIdentifier";

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureUI];
}

- (void)configureUI
{
    self.containerView.layer.cornerRadius = 4.0f;
    self.containerView.layer.borderColor = BOTLightGrayColor().CGColor;
    self.containerView.layer.borderWidth = 1.0f;
    self.containerView.clipsToBounds = YES;
    
    self.titleTextField.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    self.titleTextField.textColor = BOTChatTextGrayColor();
    self.titleTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [self.moreButton setTitle:@"More" forState:UIControlStateNormal];
    [self.moreButton addTarget:self action:@selector(didTapMoreButton:) forControlEvents:UIControlEventTouchUpInside];
    
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.clipsToBounds = YES;
    
    self.onboardingActionButton = [UIButton new];
    self.onboardingActionButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.onboardingActionButton setBackgroundColor:BOTBlueColor()];
    [self.onboardingActionButton addTarget:self action:@selector(didTapActionButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:self.onboardingActionButton];
    
    self.inputView = [BOTOnboardingInputView new];
    self.inputView.translatesAutoresizingMaskIntoConstraints = NO;
    self.inputView.delegate = self;
    [self.containerView addSubview:self.inputView];
    
    [self configureConstraints];
}

#pragma mark - Public Methods

+ (CGFloat)cellHeightForMessage:(LYRMessage *)message;
{
    NSDictionary *payload = onboardingPartData(message.parts[0]);
    NSString *instructions = payload[BOTOnboardingInstruction];
    
    NSUInteger titleTextFieldHeight = 52;
    NSUInteger imageViewHeight = 100;
    NSUInteger instructionHeight = BOTPlainTextSize(instructions, BOTInstructionTextAttributes()).height + 12;
    
    NSUInteger height = titleTextFieldHeight + imageViewHeight + instructionHeight;
    
    NSString *buttonText = payload[BOTOnboardingButtonText];
    NSString *placeHolderText = payload[BOTOnboardingPlaceholder];
    if (buttonText || placeHolderText) {
        height += 52;
    }
    
    return height;
}

+ (NSString *)reuseIdentifier
{
    return BOTOnboardingCollectionViewCellReuseIdentifier;
}

#pragma mark - ATLMessagePresenting

- (void)presentMessage:(LYRMessage *)message
{
    NSDictionary *payload = onboardingPartData(message.parts[0]);
    
    // Parse Title
    self.titleTextField.text = payload[BOTOnboardingTitle];
    
    // Parse Image
    [self setProductImageURL:payload[BOTOnboardingImageURL]];

    // Parse Instructions
    NSString *instructions = payload[BOTOnboardingInstruction];
    [self.instructionTextView setAttributedText:[self attributedTextForInstructions:instructions]];
    
    // Parse Button
    NSString *buttonText = payload[BOTOnboardingButtonText];
    if (buttonText) {
        self.onboardingActionButton.alpha = 1.0;
        [self.onboardingActionButton setTitle:buttonText forState:UIControlStateNormal];
    } else {
        self.onboardingActionButton.alpha = 0.0;
    }
    
    // Parse Button
    NSString *placeHolderText = payload[BOTOnboardingPlaceholder];
    if (placeHolderText) {
        self.inputView.alpha = 1.0;
        [self.inputView updatePlaceholderText:placeHolderText];
    } else {
        self.inputView.alpha = 0.0;
    }
    
}

- (void)updateWithSender:(nullable id<ATLParticipant>)sender
{
    // Nothing to do.
}

- (void)shouldDisplayAvatarItem:(BOOL)shouldDisplayAvatarItem
{
    // Nothing to do.
}

- (void)setProductImageURL:(NSString *)imageURL
{
    if (imageURL) {
        NSString *url = [imageURL stringByReplacingOccurrencesOfString:@"http://" withString:@"https://"];
        NSURL *picURL = [NSURL URLWithString:url];
        __weak typeof(self) wSelf = self;
        [self.imageView sd_setImageWithURL:picURL
                              placeholderImage:[UIImage imageNamed:@"GhostImage" inBundle:StaplesUIBundle() compatibleWithTraitCollection:nil]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                         if (image && cacheType == SDImageCacheTypeNone) {
                                             [UIView animateWithDuration:0.3
                                                              animations:^{
                                                                  wSelf.imageView.alpha = 1.0;
                                                              }];
                                         }
                                     }];
    } else {
        [self.imageView setImage:[UIImage imageNamed:@"GhostImage" inBundle:StaplesUIBundle() compatibleWithTraitCollection:nil]];
    }
}

- (NSAttributedString *)attributedTextForInstructions:(NSString *)instructions
{
    return [[NSAttributedString alloc] initWithString:instructions attributes:BOTInstructionTextAttributes()];
}

- (void)didTapMoreButton:(UIButton *)sender
{
    NSDictionary *userInfo = @{BOTOnboardingActionKey: @(BOTOnboardingActionMore)};
    [[NSNotificationCenter defaultCenter] postNotificationName:BOTOnboardingActionNotification object:nil userInfo:userInfo];
}

- (void)didTapActionButton:(UIButton *)sender
{
    NSDictionary *userInfo = @{BOTOnboardingActionKey: @(BOTOnboardingActionLaunchCamera)};
    [[NSNotificationCenter defaultCenter] postNotificationName:BOTOnboardingActionNotification object:nil userInfo:userInfo];
}

- (void)onboardingInputView:(BOTOnboardingInputView *)onboardingInputView didTapConfirmationButtonWithText:(NSString *)text
{
    NSDictionary *userInfo = @{BOTOnboardingActionKey: @(BOTOnboardingActionNameButton), BOTOnboardingButtonNameKey: text};
    [[NSNotificationCenter defaultCenter] postNotificationName:BOTOnboardingActionNotification object:nil userInfo:userInfo];
}

- (void)configureConstraints
{
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.onboardingActionButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0]];
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.onboardingActionButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]];
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.onboardingActionButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.onboardingActionButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:52.0]];
    
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.inputView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0]];
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.inputView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]];
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.inputView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.inputView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:52.0]];
}

@end
