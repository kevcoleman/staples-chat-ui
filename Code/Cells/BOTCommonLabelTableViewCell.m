//
//  BOTCommonQuestionsTableViewCell.m
//  Staples
//
//  Created by Reid Weber on 1/26/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTCommonLabelTableViewCell.h"

NSString *const BOTCommonLabelTableViewCellIdentifier = @"BOTCommonLabelTableViewCellIdentifier";

@implementation BOTCommonLabelTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

+ (CGFloat)cellHeight
{
    return 44;
}

+ (NSString *)reuseIdentifier
{
    return BOTCommonLabelTableViewCellIdentifier;
}

@end
