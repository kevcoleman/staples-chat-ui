//
//  BOTEasyButtonHeaderTableViewCell.m
//  iPhoneCFA
//
//  Created by Jayashree on 23/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import "BOTEasyButtonHeaderTableViewCell.h"
#import "BOTUtilities.h"

NSString *const BOTEasyButtonHeaderTableViewCellIdentifier = @"BOTEasyButtonHeaderTableViewCellIdentifier";

@interface BOTEasyButtonHeaderTableViewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *heroImageView;
@property (strong, nonatomic) IBOutlet UITextView *instructionView;
@property (strong, nonatomic) IBOutlet UIView *descriptionBackgroundView;

@end

@implementation BOTEasyButtonHeaderTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureUI];
}

- (void)configureUI
{
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.contentView.layer.cornerRadius = 10.0f;
    self.contentView.clipsToBounds = YES;
    
    self.descriptionBackgroundView.backgroundColor = [UIColor whiteColor];
    self.instructionView.font = BOTLightFont(16);
    self.instructionView.textColor = BOTInstructionGrayColor();
    self.instructionView.text = @"Activate new Easy Buttons or edit info on any Button you've already activated.";
}

#pragma mark - Public Methods

+ (CGFloat)cellHeight
{
    return 160;
}

+ (NSString *)reuseIdentifier
{
    return BOTEasyButtonHeaderTableViewCellIdentifier;
}

- (void)updateInstructionText:(NSString *)text
{
    [self.instructionView setText:text];
}


@end
