//
//  STMultipleProductsBaseCollectionViewCell.m
//  Staples
//
//  Created by Taylor Halliday on 8/19/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTMultipleCardBaseCollectionViewCell.h"
#import "BOTMultipleProductsCollectionViewLayout.h"

// Cells
#import "BOTProductCollectionViewCell.h"
#import "BOTRewardCollectionViewCell.h"
#import "BOTOrderCollectionViewCell.h"
#import "BOTOrderStatusCollectionViewCell.h"
#import "BOTStoreLocationCollectionViewCell.h"
#import "BOTActionCollectionViewCell.h"

// Model
#import "BOTProduct.h"
#import "BOTOrder.h"
#import "BOTStore.h"
#import "BOTUtilities.h"

NSString *const BOTMessageParAddToCarButtonKey = @"addToCartButton";

NSDictionary *parseDataForPart(LYRMessagePart *part)
{
    NSString *dataString = [[NSString alloc] initWithData:part.data encoding:NSUTF8StringEncoding];
    NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    if (error) {
        return nil;
    }
    return json;
}

BOOL shouldDisplayAddToCartButtonsForMessage(LYRMessage *message)
{
    if (message.parts.count > 1) {
        LYRMessagePart *part = message.parts[1];
        NSDictionary *json = parseDataForPart(part);
        NSString *addToCart = json[BOTMessageParAddToCarButtonKey];
        if (addToCart) {
            return YES;
        }
        return NO;
    }
    return NO;
}

// Card Type Enum
typedef NS_ENUM(NSInteger, BOTCellType) {
    BOTCellTypeUndefined        = 0,
    BOTCellTypeBackToSchool     = 1,
    BOTCellTypeShipping         = 2,
    BOTCellTypeRewards          = 3,
    BOTCellTypeOrder            = 4,
    BOTCellTypeReorder          = 5,
    BOTCellTypeReturn           = 6,
    BOTCellTypeStoreLocation    = 7
};

// Reuse Identifier
NSString *const BOTMultipleProductBaseCollectionViewCellReuseIdentifier = @"BOTMultipleProductBaseCollectionViewCellReuseIdentifier";

// NSNotificationKeys
NSString *const BOTBackToSchoolViewAllSelectedNotification = @"BOTBackToSchoolViewAllSelectedNotification";
NSString *const BOTBackToSchoolItemSelectedNotification = @"BOTBackToSchoolItemSelectedNotification";
NSString *const BOTShipmentSelectedNotification = @"BOTShipmentSelectedNotification";
NSString *const BOTRewardSelectedNotification = @"BOTRewardSelectedNotification";
NSString *const BOTAddToCartNotification = @"BOTAddToCartNotification";
NSString *const BOTOrderNewSuppliesItemSelectedNotification = @"BOTOrderNewSuppliesItemSelectedNotification";
NSString *const BOTRewardAddtoAppleWalletSelectionNotification = @"BOTRewardAddtoAppleWalletSelectionNotification";
NSString *const BOTStoreSelectedNotification = @"BOTStoreSelectedNotification";

// Card Type Keys
NSString *const BOTCardTypeBTSItems = @"BTS_ITEMS";
NSString *const BOTCardTypeCartItems = @"CART_ITEMS";
NSString *const BOTCardTypeOrderItems = @"ORDER_ITEMS";
NSString *const BOTCardTypeReturnItems = @"RETURN_ITEMS";
NSString *const BOTCardTypeReorderItems = @"REORDER_ITEMS";

// JSON Keys
NSString *const BOTMessagePartCardTypeKey = @"cardType";
NSString *const BOTMessagePartCartItemsKey = @"cartItems";
NSString *const BOTMessagePartBTSItemsKey = @"btsItems";
NSString *const BOTMessagePartOrderItemsKey = @"orderItems";
NSString *const BOTMessagePartListItemsKey = @"listItems";
NSString *const BOTMessagePartHeaderTitle = @"headerTitle";
NSString *const BOTMessagePartShipmentTrackingListKey = @"shippmentTrackingList";
NSString *const BOTMessagePartRewardListKey = @"rewardslistItems";
NSString *const BOTMessagePartRewardSummaryListKey = @"rewardsSummaryList";
NSString *const BOTMessagePartReorderItemsKey = @"reorderItems";
NSString *const BOTMessagePartReturnItemsKey = @"returnItems";

@interface BOTMultipleCardBaseCollectionViewCell () <UICollectionViewDelegate, UICollectionViewDataSource, BOTProductCollectionViewCellDelegate, BOTRewardCollectionVeiwCellDelegate >

@property (nonatomic) LYRMessage *message;
@property (nonatomic) BOTCellType cellType;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) BOTMultipleProductsCollectionViewLayout *collectionViewLayout;
@property (nonatomic, strong) UILabel *btsHeaderLabel;
@property (nonatomic, strong) UIButton *viewAllButton;
@property (nonatomic, strong) NSLayoutConstraint *topCollectionViewConstraint;

@end

@implementation BOTMultipleCardBaseCollectionViewCell

CGFloat const BOTHeaderLabelTopInset = 8.0f;
CGFloat const BOTHeaderLabelHorizontalInset = 20.0f;
CGFloat const BOTCollectionViewTopInset = 26.0f;

#pragma mark - Initializers / Common Init

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.contentView.backgroundColor = [UIColor clearColor];
    [self layoutCollectionView];
    
    UINib *productNib = [UINib nibWithNibName:@"BOTProductCollectionViewCell" bundle:StaplesUIBundle()];
    [self.collectionView registerNib:productNib forCellWithReuseIdentifier:[BOTProductCollectionViewCell reuseIdentifier]];
    
    UINib *orderStatusNib = [UINib nibWithNibName:@"BOTOrderStatusCollectionViewCell" bundle:StaplesUIBundle()];
    [self.collectionView registerNib:orderStatusNib forCellWithReuseIdentifier:[BOTOrderStatusCollectionViewCell reuseIdentifier]];
    
    UINib *rewardNib = [UINib nibWithNibName:@"BOTRewardCollectionViewCell" bundle:StaplesUIBundle()];
    [self.collectionView registerNib:rewardNib forCellWithReuseIdentifier:[BOTRewardCollectionViewCell reuseIdentifier]];
    
    UINib *reorderNib = [UINib nibWithNibName:@"BOTOrderCollectionViewCell" bundle:StaplesUIBundle()];
    [self.collectionView registerNib:reorderNib forCellWithReuseIdentifier:[BOTOrderCollectionViewCell reuseIdentifier]];
    
    UINib *orderCell = [UINib nibWithNibName:@"BOTOrderCollectionViewCell" bundle:StaplesUIBundle()];
    [self.collectionView registerNib:orderCell forCellWithReuseIdentifier:[BOTOrderCollectionViewCell reuseIdentifier]];
    
    UINib *storeLocationCell = [UINib nibWithNibName:@"BOTStoreLocationCollectionViewCell" bundle:StaplesUIBundle()];
    [self.collectionView registerNib:storeLocationCell forCellWithReuseIdentifier:[BOTStoreLocationCollectionViewCell reuseIdentifier]];
}

- (void)layoutCollectionView
{
    self.btsHeaderLabel = [UILabel new];
    self.btsHeaderLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightThin];
    self.btsHeaderLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.btsHeaderLabel];
    
    self.viewAllButton = [UIButton new];
    self.viewAllButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.viewAllButton.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightThin];
    [self.viewAllButton setTitleColor:BOTBlueColor() forState:UIControlStateNormal];
    [self.viewAllButton addTarget:self action:@selector(viewAllButtonWasTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.viewAllButton];
    
    self.collectionViewLayout = [[BOTMultipleProductsCollectionViewLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.collectionViewLayout];
    self.collectionView.contentInset = UIEdgeInsetsMake(0.0, 11.0, 0.0, 1.0);
    self.collectionView.contentOffset = CGPointMake(16.0, 0.0);
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.collectionViewLayout = self.collectionViewLayout;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    
    [self addSubview:self.collectionView];
    [self addCollecitonViewConstraints];
}

+ (NSString *)reuseIdentifier
{
    return BOTMultipleProductBaseCollectionViewCellReuseIdentifier;
}

+ (CGFloat)cellHeightForMessage:(LYRMessage *)message
{
    LYRMessagePart *part = message.parts[0];
    
   if ([part.MIMEType isEqualToString:BOTProductListMIMEType]) {
        BOOL button = shouldDisplayAddToCartButtonsForMessage(message);
        return [BOTProductCollectionViewCell cellHeightWithButton:button];
    } else if ([part.MIMEType isEqualToString:BOTRewardMIMEType]) {
        return [BOTRewardCollectionViewCell cellHeight];
    } else if ([part.MIMEType isEqualToString:BOTShipmentMIMEType]) {
        return [BOTOrderStatusCollectionViewCell cellHeight];
    } else if ([part.MIMEType isEqualToString:BOTOrderMIMEType]) {
        BOOL button = shouldDisplayAddToCartButtonsForMessage(message);
        return [BOTProductCollectionViewCell cellHeightWithButton:button];
    }  else if ([part.MIMEType isEqualToString:BOTReorderMIMEType]) {
        return [BOTOrderCollectionViewCell cellHeight];
    } else if ([part.MIMEType isEqualToString:BOTReturnMIMEType]) {
        return [BOTOrderCollectionViewCell cellHeight];
    } else if ([part.MIMEType isEqualToString:BOTStoreLocationMimeType]) {
        return [BOTStoreLocationCollectionViewCell cellHeight];
    }
    return 260;
}

#pragma mark - Reuse

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self.viewAllButton setTitle:@"" forState:UIControlStateNormal];
    [self.viewAllButton sizeToFit];
    
    self.btsHeaderLabel.text = @"";
    [self.btsHeaderLabel sizeToFit];
    self.cellType = BOTCellTypeUndefined;
    self.topCollectionViewConstraint.constant = 0;
}

#pragma mark - ATLMessagePresenting

- (void)presentMessage:(LYRMessage *)message
{
    self.message = message;
    LYRMessagePart *part = message.parts[0];
    [self setCellTypeForMessagePart:part];
    
    self.items = [self cellItemsForMessage:message];
    [self.collectionView reloadData];
}

- (void)updateWithSender:(nullable id<ATLParticipant>)sender
{
    // Nothing to do.
}

- (void)shouldDisplayAvatarItem:(BOOL)shouldDisplayAvatarItem
{
    // Nothing to do.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //for BTS and Cart Items has display limit of 10
    if(self.cellType == 0){
        if(self.items.count>10){
            return 10;
        }else
            return self.items.count;
    }
    else
        return self.items.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = 0.9f;
    switch (self.cellType) {
        case BOTCellTypeRewards:
            width = 0.9f;
            break;
        case BOTCellTypeBackToSchool:
        case BOTCellTypeShipping:
            width = 0.9f;
        case BOTCellTypeOrder:
        case BOTCellTypeReorder:
        case BOTCellTypeReturn:
        default:
            break;
    }
    return CGSizeMake(collectionView.bounds.size.width * width, collectionView.bounds.size.height * 0.95f);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.cellType) {
        case BOTCellTypeBackToSchool: {
            NSNotification *notification = [NSNotification notificationWithName:BOTBackToSchoolItemSelectedNotification object:self.items[indexPath.row]];
            [[NSNotificationCenter defaultCenter] postNotification:notification];
        }
            break;
            
        case BOTCellTypeRewards: {
            NSNotification *notification = [NSNotification notificationWithName:BOTRewardSelectedNotification object:self.items[indexPath.row]];
            [[NSNotificationCenter defaultCenter] postNotification:notification];
        }
            break;
            
        case BOTCellTypeShipping: {
            NSNotification *notification = [NSNotification notificationWithName:BOTShipmentSelectedNotification object:self.items[indexPath.row]];
            [[NSNotificationCenter defaultCenter] postNotification:notification];
        }
            break;
            
        case BOTCellTypeOrder: {
            NSNotification *notification = [NSNotification notificationWithName:BOTOrderNewSuppliesItemSelectedNotification object:self.items[indexPath.row]];
            [[NSNotificationCenter defaultCenter] postNotification:notification];
        }
            break;
        case BOTCellTypeStoreLocation: {
            NSNotification *notification = [NSNotification notificationWithName:BOTStoreSelectedNotification object:self.items[indexPath.row]];
            [[NSNotificationCenter defaultCenter] postNotification:notification];
        }
        default:
            break;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *returnCell;
    switch (self.cellType) {
        case BOTCellTypeBackToSchool: {
            NSString *reuseIdentifier = [BOTProductCollectionViewCell reuseIdentifier];
            BOTProductCollectionViewCell *cell = (BOTProductCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
            BOTProduct *item = self.items[indexPath.row];
            
            BOOL button = shouldDisplayAddToCartButtonsForMessage(self.message);
            [cell setProductItem:item showAddToCartButton:button];
            cell.delegate = self;
            returnCell = cell;
        }
            
            break;
        case BOTCellTypeRewards: {
            NSString *reuseIdentifier = [BOTRewardCollectionViewCell reuseIdentifier];
            BOTRewardCollectionViewCell *cell = (BOTRewardCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
            cell.delegate = self;
            BOTReward *item = self.items[indexPath.row];
            [cell setReward:item];
            returnCell = cell;
        }
            
            break;
        case BOTCellTypeShipping: {
            NSString *reuseIdentifier = [BOTOrderStatusCollectionViewCell reuseIdentifier];
            BOTOrderStatusCollectionViewCell *cell = (BOTOrderStatusCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
            BOTShipment *shipment = self.items[indexPath.row];
            [cell setShipment:shipment];
            returnCell = cell;
        }
            break;
            
        case BOTCellTypeOrder: {
            NSString *reuseIdentifier = [BOTProductCollectionViewCell reuseIdentifier];
            BOTProductCollectionViewCell *cell = (BOTProductCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
            BOTProduct *item = self.items[indexPath.row];
            cell.delegate = self;
            
            BOOL button = shouldDisplayAddToCartButtonsForMessage(self.message);
            [cell setProductItem:item showAddToCartButton:button];
            returnCell = cell;
        }
            break;
            
        case BOTCellTypeReorder: {
            NSString *reuseIdentifier = [BOTOrderCollectionViewCell reuseIdentifier];
            BOTOrderCollectionViewCell *cell = (BOTOrderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
            BOTOrder *order = self.items[indexPath.row];
            [cell setOrder:order];
            returnCell = cell;
        }
            break;
            
        case BOTCellTypeReturn: {
            NSString *reuseIdentifier = [BOTOrderCollectionViewCell reuseIdentifier];
            BOTOrderCollectionViewCell *cell = (BOTOrderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
            BOTOrder *order = self.items[indexPath.row];
            [cell setOrder:order];
            returnCell = cell;
        }
            break;
        case BOTCellTypeStoreLocation: {
            NSString *reuseIdentifier = [BOTStoreLocationCollectionViewCell reuseIdentifier];
            BOTStoreLocationCollectionViewCell *cell = (BOTStoreLocationCollectionViewCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
            BOTStore *store = self.items[indexPath.row];
            [cell updateWithStore:store];
            
            returnCell = cell;
        }
            break;
        default:
            break;
    }
    
    return returnCell;
}

#pragma mark - Data Parsing

- (void)setCellTypeForMessagePart:(LYRMessagePart *)part
{
    if ([part.MIMEType isEqualToString:BOTProductListMIMEType]) {
        self.cellType = BOTCellTypeBackToSchool;
    } else if ([part.MIMEType isEqualToString:BOTRewardMIMEType]) {
        self.cellType = BOTCellTypeRewards;
    } else if ([part.MIMEType isEqualToString:BOTShipmentMIMEType]) {
        self.cellType = BOTCellTypeShipping;
    } else if ([part.MIMEType isEqualToString:BOTOrderMIMEType]) {
        self.cellType = BOTCellTypeOrder;
    } else if ([part.MIMEType isEqualToString:BOTReorderMIMEType]) {
        self.cellType = BOTCellTypeReorder;
    } else if ([part.MIMEType isEqualToString:BOTReturnMIMEType]) {
        self.cellType = BOTCellTypeReturn;
    } else if ([part.MIMEType isEqualToString:BOTStoreLocationMimeType]) {
        self.cellType = BOTCellTypeStoreLocation;
    }
}

- (NSArray *)cellItemsForMessage:(LYRMessage *)message
{
    LYRMessagePart *part = message.parts[0];
    NSDictionary *json = [self parseDataForMessagePart:part];
    NSDictionary *data = json[BOTMessagePartDataKey];
    
    // Parse Product list Data.
    NSMutableArray *items = [[NSMutableArray alloc] init];
    if ([part.MIMEType isEqualToString:BOTProductListMIMEType] || [part.MIMEType isEqualToString:BOTOrderMIMEType]) {
        if ([data[BOTMessagePartCardTypeKey] isEqualToString:BOTCardTypeBTSItems]) {
            items = [self itemsForBTSFlowWithMessage:message];
        } else  if ([data[BOTMessagePartCardTypeKey] isEqualToString:BOTCardTypeOrderItems]) {
            items = [self itemsForProductWithMessage:message];
        } else  if ([data[BOTMessagePartCardTypeKey] isEqualToString:BOTCardTypeCartItems]) {
            items = [self itemsForCartFlowWithMessage:message];
        }
    }
    
    // Parse Order
    if ([part.MIMEType isEqualToString:BOTReorderMIMEType] || [part.MIMEType isEqualToString:BOTReturnMIMEType]) {
        items = [self itemsForOrderWithMessage:message];
    }
    
    // Parse Reward Data.
    if ([part.MIMEType isEqualToString:BOTRewardMIMEType]) {
        NSArray *rewardJSON = data[BOTMessagePartRewardListKey];
        if (!rewardJSON) {
            rewardJSON = data[BOTMessagePartRewardSummaryListKey];
        }
        for (NSDictionary *itemData in rewardJSON) {
            BOTReward *reward = [BOTReward rewardWithData:itemData];
            [items addObject:reward];
        }
    }
    
    // Parse Shipping Data.
    if ([part.MIMEType isEqualToString:BOTShipmentMIMEType]) {
        NSArray *shipmentJSON = data[BOTMessagePartShipmentTrackingListKey];
        for (NSDictionary *itemData in shipmentJSON) {
            BOTShipment *item = [BOTShipment shipmentWithData:itemData];
            [items addObject:item];
        }
    }
    
    if ([part.MIMEType isEqualToString:BOTStoreLocationMimeType]) {
        BOTStore *store = [BOTStore storeLocaitonWithData:json];
        [items addObject:store];
    }
    return items;
}

#pragma mark - Data Parsing

- (NSDictionary *)parseDataForMessagePart:(LYRMessagePart *)part
{
    NSString *dataString = [[NSString alloc] initWithData:part.data encoding:NSUTF8StringEncoding];
    NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    if (error) {
        return nil;
    }
    return json;
}

- (NSMutableArray *)itemsForBTSFlowWithMessage:(LYRMessage *)message
{
    LYRMessagePart *part = message.parts[0];
    NSDictionary *json = [self parseDataForMessagePart:part];
    
    // Parse BTS Title
    NSDictionary *data = json[BOTMessagePartDataKey];
    NSDictionary *part1Data = data[BOTMessagePartBTSItemsKey];
    
    // Update View
    self.btsHeaderLabel.text = part1Data[BOTMessagePartHeaderTitle];
    [self.btsHeaderLabel sizeToFit];
    
    [self.viewAllButton setTitle:@"View All" forState:UIControlStateNormal];
    [self.viewAllButton sizeToFit];
    
    self.topCollectionViewConstraint.constant = BOTCollectionViewTopInset;
    
    //BTS items and cart items are coming in mupltiple part, so Handled number of parts Parse BTS Items
    NSMutableArray *items = [NSMutableArray new];
    for (int i = 1; i < self.message.parts.count; i++){
        LYRMessagePart *part = self.message.parts[i];
        NSDictionary *json = [self parseDataForMessagePart:part];
        
        NSDictionary *itemsData;
        NSDictionary *data = json[BOTMessagePartDataKey];
        if (data[BOTMessagePartListItemsKey]) {
            itemsData = data[BOTMessagePartListItemsKey];
        }
        for (NSDictionary *itemData in itemsData) {
            BOTProduct *item = [BOTProduct productWithData:itemData];
            [items addObject:item];
        }
    }
    return items;
}

- (NSMutableArray *)itemsForCartFlowWithMessage:(LYRMessage *)message
{
    LYRMessagePart *part = message.parts[0];
    NSDictionary *json = [self parseDataForMessagePart:part];
    
    // Parse BTS Title
    NSDictionary *data = json[BOTMessagePartDataKey];
    NSDictionary *part1Data = data[BOTMessagePartCartItemsKey];
    
    // Update View
    self.btsHeaderLabel.text = part1Data[BOTMessagePartHeaderTitle];
    [self.btsHeaderLabel sizeToFit];
    
    [self.viewAllButton setTitle:@"View All" forState:UIControlStateNormal];
    [self.viewAllButton sizeToFit];
    
    self.topCollectionViewConstraint.constant = BOTCollectionViewTopInset;
    
    //BTS items and cart items are coming in mupltiple part, so Handled number of parts Parse BTS Items
    NSMutableArray *items = [NSMutableArray new];
    for (int i = 0; i < self.message.parts.count; i++){
        LYRMessagePart *dataPart = self.message.parts[i];
        if ([dataPart.MIMEType isEqualToString:BOTActionMIMEType]) {
            continue;
        }
        
        NSDictionary *itemsData;
        NSDictionary *json = [self parseDataForMessagePart:dataPart];
        NSDictionary *data = json[BOTMessagePartDataKey];
        if (data[BOTMessagePartListItemsKey]) {
            itemsData = data[BOTMessagePartListItemsKey];
        }
        for (NSDictionary *itemData in itemsData) {
            BOTProduct *item = [BOTProduct productWithData:itemData];
            [items addObject:item];
        }
    }
    return items;
}

- (NSMutableArray *)itemsForProductWithMessage:(LYRMessage *)message
{
    //handled multiparts of the message
    NSMutableArray *items = [NSMutableArray new];
    for (int i = 0; i < self.message.parts.count; i++){
        LYRMessagePart *part = self.message.parts[i];
        NSDictionary *json = [self parseDataForMessagePart:part];
        
        NSDictionary *orderItems;
        NSDictionary *data = json[BOTMessagePartDataKey];
        if (data[BOTMessagePartOrderItemsKey]) {
            orderItems = data[BOTMessagePartOrderItemsKey];
        }
        for (NSDictionary *itemData in orderItems) {
            BOTProduct *item = [BOTProduct productWithData:itemData];
            [items addObject:item];
        }
    }
    return items;
}


- (NSMutableArray *)itemsForOrderWithMessage:(LYRMessage *)message
{
    //handled multiparts of the message
    NSMutableArray *items = [NSMutableArray new];
    for (int i = 0; i < self.message.parts.count; i++){
        LYRMessagePart *part = self.message.parts[i];
        NSDictionary *json = [self parseDataForMessagePart:part];
        
        NSDictionary *orderItems;
        NSDictionary *data = json[BOTMessagePartDataKey];
        if ([data[BOTMessagePartCardTypeKey] isEqualToString:BOTCardTypeReturnItems]) {
            orderItems = data[BOTMessagePartReturnItemsKey];
        } else if ([data[BOTMessagePartCardTypeKey] isEqualToString:BOTCardTypeReorderItems]) {
            orderItems = data[BOTMessagePartReorderItemsKey];
        }
        for (NSDictionary *itemData in orderItems) {
            BOTOrder *order = [BOTOrder orderWithData:itemData];
            [items addObject:order];
        }
    }
    return items;
}

#pragma mark -g BTS View All Button Tap

- (void)viewAllButtonWasTapped:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:BOTBackToSchoolViewAllSelectedNotification object:self.items];
}

#pragma mark - BOTProductCollectionViewCellDelegate method

- (void)productCollectionViewCellDidSelectAddToCart:(BOTProductCollectionViewCell *)cell{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    NSNotification *notification = [NSNotification notificationWithName:BOTAddToCartNotification object:self.items[indexPath.row]];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

#pragma mark - BOTRewardCollectionViewCellDelegate method

- (void)rewardCollectionViewCellDidSelectAddtoAppleWallet:(BOTRewardCollectionViewCell *)cell{
    NSNotification *notification = [NSNotification notificationWithName:BOTRewardAddtoAppleWalletSelectionNotification object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

#pragma mark - NSLayoutConstraints For UI

- (void)addCollecitonViewConstraints
{
    // Label constraints
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btsHeaderLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:BOTHeaderLabelTopInset]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btsHeaderLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:BOTHeaderLabelHorizontalInset]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.viewAllButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.btsHeaderLabel attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.viewAllButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-BOTHeaderLabelHorizontalInset]];
    
    // Collection View Constraints
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    self.topCollectionViewConstraint = [NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    [self addConstraint:self.topCollectionViewConstraint];
}

@end
