//
//  BOTSelectionTableViewCell.m
//  iPhoneCFA
//
//  Created by Jayashree on 26/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import "BOTSelectionTableViewCell.h"
#import "BOTUtilities.h"

NSString *const BOTSelectionTableViewCellIdentifier = @"BOTSelectionTableViewCellIdentifier";

NSString *const BOTArrowUpImageStringOff = @"arrow_down";
NSString *const BOTArrowUpImageStringOn  = @"arrow_right_on";

CGFloat const BOTAnimationDuration = 1.0;

@interface BOTSelectionTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *selectionLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *selectionIcon;

@end

@implementation BOTSelectionTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureContainerView];
    [self addGestureRecognizers];
}

- (void)configureContainerView
{
    self.containerView.layer.cornerRadius = 10.0f;
    self.containerView.clipsToBounds = YES;
    self.containerView.layer.borderWidth = 1.0;
    self.containerView.layer.borderColor = [UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:1].CGColor;
}

- (void)addGestureRecognizers
{
    UILongPressGestureRecognizer *gesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    gesture.minimumPressDuration = 0;
    [self.containerView addGestureRecognizer:gesture];
}

- (void)tapGesture:(UILongPressGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan) {
        [self highlightWhenSelected:true];
    } else if (gesture.state == UIGestureRecognizerStateCancelled) {
        [self highlightWhenSelected:false];
    } else if (gesture.state == UIGestureRecognizerStateEnded) {
        [self highlightWhenSelected:false];
        if ([(NSObject *)self.delegate respondsToSelector:@selector(selectionTableViewCell:didTapCellGesture:)]) {
            [self.delegate selectionTableViewCell:self didTapCellGesture:gesture];
        }
    }
}

#pragma mark - Public Methods

- (void)highlightWhenSelected:(BOOL)began
{
    if (began) {
        [UIView animateWithDuration:BOTAnimationDuration animations:^{
            [self.selectionLabel setTextColor:BOTBlueColor()];
            self.containerView.layer.borderColor = BOTBlueColor().CGColor;
            self.selectionIcon.image = [UIImage imageNamed:BOTArrowUpImageStringOn];
        }];
    } else {
        [UIView animateWithDuration:BOTAnimationDuration animations:^{
            [self.selectionLabel setTextColor:[UIColor darkGrayColor]];
            self.containerView.layer.borderColor = BOTLightGrayColor().CGColor;
            self.selectionIcon.image = [UIImage imageNamed:BOTArrowUpImageStringOff];
        }];
    }
}

- (NSString *)cellTitle
{
    return self.selectionLabel.text;
}

- (void)updateSelectionText:(NSString *)selectionText
{
    [self.selectionLabel setText:selectionText];
}

+ (CGFloat)cellHeight
{
    return 70;
}

+ (NSString *)reuseIdentifier
{
    return BOTSelectionTableViewCellIdentifier;
}

@end
