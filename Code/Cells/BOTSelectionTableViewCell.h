//
//  BOTSelectionTableViewCell.h
//  iPhoneCFA
//
//  Created by Jayashree on 26/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BOTSelectionTableViewCell;

/**
 @abstract 'BOTSelectionTableViewCellDelegate' tells the reciever that the cell was selected.
 @discussion This method is used along with the normal didSelectRowAtIndexPath: so we can update the cell's UI when custom gestures are hit. Adding gestures to the cell's view blocks touch events from reaching its tableView's delegate.
 */
@protocol BOTSelectionTableViewCellDelegate

- (void)selectionTableViewCell:(BOTSelectionTableViewCell *)cell didTapCellGesture:(UILongPressGestureRecognizer *)gesture;

@end

/**
 @abstract A simple UITableView cell with a build in animation attached to a custom UILongPressGestureRecognizer  
 */
@interface BOTSelectionTableViewCell : UITableViewCell

@property (weak, nonatomic) id<BOTSelectionTableViewCellDelegate> delegate;

- (NSString *)cellTitle;
- (void)updateSelectionText:(NSString *)selectionText;

+ (CGFloat)cellHeight;
+ (NSString *)reuseIdentifier;

@end
