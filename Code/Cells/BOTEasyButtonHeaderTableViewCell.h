
//
//  BOTEasyButtonHeaderTableViewCell.h
//  iPhoneCFA
//
//  Created by Jayashree on 23/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @abstract UITableViewCell to hold the hero image and an instruction label for the View Controller
 */
@interface BOTEasyButtonHeaderTableViewCell : UITableViewCell

+ (CGFloat)cellHeight;
+ (NSString *)reuseIdentifier;

- (void)updateInstructionText:(NSString *)text;

@end
