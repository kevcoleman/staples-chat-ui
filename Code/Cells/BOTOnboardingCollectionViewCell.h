//
//  BOTOnboardingCollectionViewCell.h
//  Staples
//
//  Created by Kevin Coleman on 12/27/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Atlas/Atlas.h>
#import "BOTOnboardingManager.h"

extern NSString *const BOTOnboardingCollectionViewCellReuseIdentifier;

@interface BOTOnboardingCollectionViewCell : UICollectionViewCell <ATLMessagePresenting>

@property (strong, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) IBOutlet UITextField *titleTextField;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UITextView *instructionTextView;

@property (strong, nonatomic) IBOutlet UIButton *moreButton;

+ (CGFloat)cellHeightForMessage:(LYRMessage *)message;

+ (NSString *)reuseIdentifier;

@end
