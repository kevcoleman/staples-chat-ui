//
//  BOTLoginInputTableViewCell.h
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BOTLoginInputTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *inputTextField;

- (void)updateTitleLabel:(NSString *)titleText;
- (void)updateWithPlaceHolder:(NSString *)placeHolder;
- (void)updateWithLeftButtonText:(NSString *)leftButtonText enabled:(BOOL)enabled;
- (void)updateWithRightButtonText:(NSString *)rightButtonText;

+ (CGFloat)cellHeight;
+ (NSString *)reuseIdentifier;

@end
