//
//  BOTExpandedQuestionTableViewCell.m
//  Staples
//
//  Created by Reid Weber on 1/26/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTExpandedQuestionTableViewCell.h"

@interface BOTExpandedQuestionTableViewCell ()

@property (weak, nonatomic) IBOutlet UIButton *questionButton;

@end

@implementation BOTExpandedQuestionTableViewCell

NSString *const BOTExpandedQuestionTableViewCellIdentifier = @"BOTExpandedQuestionTableViewCellIdentifier";

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

+ (NSString *)reuseIdentifier
{
    return BOTExpandedQuestionTableViewCellIdentifier;
}

+ (CGFloat)cellHeight
{
    return 26;
}

- (void)setButtonText:(NSString *)text
{
    [self.questionButton setTitle:text forState:UIControlStateNormal];
}

- (IBAction)questionButtonPressed:(UIButton *)sender
{
    if ([(NSObject *)self.delegate respondsToSelector:@selector(expandedQuestionCell:didTapActionButton:)]) {
        [self.delegate expandedQuestionCell:self didTapActionButton:sender];
    }
}

@end
