//
//  BOTStoreLocationCollectionViewCell.m
//  Staples
//
//  Created by Taylor Halliday on 1/27/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTStoreLocationCollectionViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "BOTUtilities.h"
#import "BOTReceipt.h"
#import "BOTStore.h"

NSString *const BOTStoreLocationCollectionViewCellReuseIdentifier = @"BOTStoreLocationCollectionViewCellCellReuseIdentifier";
NSString *const BOTStoreLocationCollectionViewCellTitle = @"Store Location Cell";
NSString *const BOTStoreLocationMimeType = @"application/json+storelocation";

double const BOTMileMultiplier = .000621371;

@interface BOTStoreLocationCollectionViewCell() <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *phoneButton;
@property (weak, nonatomic) IBOutlet UILabel *openStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *untilCloseLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;


@end

@implementation BOTStoreLocationCollectionViewCell


#pragma mark - Reuse ID / Cell Height

+ (NSString *)reuseIdentifier
{
    return BOTStoreLocationCollectionViewCellReuseIdentifier;
}

+ (CGFloat)cellHeight
{
    return 225;
}

#pragma mark - Init / Layout

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureUI];
    self.mapView.delegate = self;
}

- (void)configureUI
{
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor whiteColor];
    
    self.layer.cornerRadius = 4.0f;
    self.layer.masksToBounds = NO;
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = BOTLightGrayColor().CGColor;
}

- (void)configureMapViewWithLocation:(CLLocationCoordinate2D)location
{
    MKCoordinateSpan span = MKCoordinateSpanMake(1.0, 1.0);
    MKCoordinateRegion region = MKCoordinateRegionMake(location, span);
    
    MKPointAnnotation *pin = [[MKPointAnnotation alloc] init];
    [pin setCoordinate:location];
    [pin setTitle:@"Staples"];
    [self.mapView addAnnotation:pin];
    
    [self.mapView setRegion:region animated:NO];
}

- (void)updateWithStore:(BOTStore *)store
{
    CLLocation *storeLocation = [[CLLocation alloc] initWithLatitude:store.latitude longitude:store.longitude];
    [self configureMapViewWithLocation:[storeLocation coordinate]];
    self.distanceLabel.text = [self getDistanceFromStore:storeLocation];
    self.cityNameLabel.text = store.city;
    self.addressLabel.text = store.addtress;
    [self.phoneButton setTitle:[self formatPhoneNumber:store.phone] forState:UIControlStateNormal];
}

- (NSString *)formatPhoneNumber:(NSString *)phoneNumber
{
    NSMutableString *phoneString = [NSMutableString stringWithString:phoneNumber];
    [phoneString insertString:@"(" atIndex:0];
    [phoneString insertString:@")" atIndex:4];
    [phoneString insertString:@"-" atIndex:5];
    [phoneString insertString:@"-" atIndex:9];
    return phoneString;
}

#pragma mark - Target/Action

- (IBAction)phoneNumberTapped:(UIButton *)sender
{
    NSString *phoneNumber = sender.titleLabel.text;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", phoneNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

#pragma mark - Map Kit Delegate

- (NSString *)getDistanceFromStore:(CLLocation *)storeLocation
{
    CLLocation *currentLocation = [self getDeviceLocation];
    CLLocationDistance meterDistance = [currentLocation distanceFromLocation:storeLocation];
    double milesUnrounded = meterDistance * BOTMileMultiplier;
    return [NSString stringWithFormat:@"%.1f miles away", milesUnrounded];
}

// This is also defined in BOTChat.m... I wasn't sure where it should live so it lives in both for now
- (CLLocation *)getDeviceLocation
{
    CLLocationManager *manager = [[CLLocationManager alloc] init];
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    manager.distanceFilter = kCLDistanceFilterNone;
    [manager startUpdatingLocation];
    CLLocation *location = [manager location];
    //CLLocationCoordinate2D coordinate = [location coordinate];
    [manager stopUpdatingLocation];
    return location;
}

@end
