//
//  BOTLoginHeadlineTableViewCell.h
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BOTLoginHeadlineTableViewCell : UITableViewCell

+ (CGFloat)cellHeight;

+ (NSString *)reuseIdentifier;

@end
