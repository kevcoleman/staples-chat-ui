//
//  BOTStoreLocationCollectionViewCell.h
//  Staples
//
//  Created by Taylor Halliday on 1/27/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOTAddress.h"

@class BOTStore;

/**
 The title of the cell. Used for testing purposes.
 */
extern NSString *const BOTStoreLocationCollectionViewCellTitle;

/**
 The MIMEType used for message parts that should display the cell.
 */
extern NSString *const BOTStoreLocationMimeType;

/**
 The reuse identifier for the cell.
 */
extern NSString *const BOTStoreLocationCollectionViewCellReuseIdentifier;

@interface BOTStoreLocationCollectionViewCell : UICollectionViewCell

/**
 Cell height
 */
+ (CGFloat)cellHeight;

/**
 Reuse Identifier
 */
+ (NSString *)reuseIdentifier;

- (void)updateWithStore:(BOTStore *)store;

@end
