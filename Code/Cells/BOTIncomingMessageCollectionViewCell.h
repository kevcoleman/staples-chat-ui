//
//  BOTIncomingMessageCollectionViewCell.h
//  Staples
//
//  Created by Reid Weber on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <Atlas/Atlas.h>

/**
 @abstract Subclass for 'ATLIncomingMessageCollectionViewCell' in order to move avatarImageView contraints to the top of the cell
 @discussion The only change made at the moment is switching the constraints for the avatarImageView. The bottom constraint was switched to the NSLayoutAttributeTop and set with a constant of 4.
 */
@interface BOTIncomingMessageCollectionViewCell : ATLIncomingMessageCollectionViewCell

+ (NSString *)reuseIdentifier;

@end
