//
//  BOTOnboardingButtonCollectionViewCell.m
//  Staples
//
//  Created by Kevin Coleman on 12/27/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTOnboardingButtonCollectionViewCell.h"
#import "BOTUtilities.h"

@implementation BOTOnboardingButtonCollectionViewCell

NSString *const BOTOnboardingButtonCollectionViewCellReuseIdentifier = @"BOTOnboardingButtonCollectionViewCellReuseIdentifier";

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureUI];
}

- (void)configureUI
{
    self.containerView.layer.borderColor = BOTBlueColor().CGColor;
    self.containerView.layer.borderWidth = 1.0f;
    self.containerView.layer.cornerRadius = 16.0f;
    self.containerView.clipsToBounds = YES;
    
    [self.leftButton setBackgroundColor:[UIColor whiteColor]];
    [self.leftButton setTitleColor:BOTBlueColor() forState:UIControlStateNormal];
    [self.leftButton addTarget:self action:@selector(didTapLeftButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.rightButton setBackgroundColor:BOTBlueColor()];
    [self.rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.rightButton addTarget:self action:@selector(didTapRightButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self configureConstraints];
}

- (void)configureConstraints
{
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.leftButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeWidth multiplier:0.5 constant:0.0]];
    [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.rightButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeWidth multiplier:0.5 constant:0.0]];
}
#pragma mark - Public Methods

+ (NSString *)reuseIdentifier
{
    return BOTOnboardingButtonCollectionViewCellReuseIdentifier;
}

+ (CGFloat)cellHeightForMessage:(LYRMessage *)message;
{
    return 48;
}

#pragma mark - ATLMessagePresenting

- (void)presentMessage:(LYRMessage *)message
{
    NSDictionary *payload = onboardingPartData(message.parts[0]);
    NSString *leftButtonTitle = payload[BOTLeftButtonTitle];
    [self.leftButton setTitle:leftButtonTitle forState:UIControlStateNormal];
    
    NSString *rightButtonTitle = payload[BOTRightButtonTitle];
    [self.rightButton setTitle:rightButtonTitle forState:UIControlStateNormal];
}

- (void)updateWithSender:(nullable id<ATLParticipant>)sender
{
    // Nothing to do.
}

- (void)shouldDisplayAvatarItem:(BOOL)shouldDisplayAvatarItem
{
    // Nothing to do.
}

#pragma mark - Button Actions

- (void)didTapLeftButton:(UIButton *)sender
{
    NSDictionary *userInfo = @{BOTOnboardingActionKey: @(BOTOnboardingActionDeny)};
    [[NSNotificationCenter defaultCenter] postNotificationName:BOTOnboardingActionNotification object:nil userInfo:userInfo];
}

- (void)didTapRightButton:(UIButton *)sender
{
    NSDictionary *userInfo = @{BOTOnboardingActionKey: @(BOTOnboardingActionConfirm)};
    [[NSNotificationCenter defaultCenter] postNotificationName:BOTOnboardingActionNotification object:nil userInfo:userInfo];
}

@end
