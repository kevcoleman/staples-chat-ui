//
//  BOTExpandedSelectionTableViewCell.m
//  Staples
//
//  Created by Reid Weber on 1/26/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTExpandedSelectionTableViewCell.h"
#import "BOTExpandedQuestionTableViewCell.h"
#import "BOTUtilities.h"

CGFloat const questionCellHeight = 28;
CGFloat const standardCellHeight = 30;

@interface BOTExpandedSelectionTableViewCell () <UITableViewDelegate, UITableViewDataSource, BOTExpandedQuestionTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;

@end

@implementation BOTExpandedSelectionTableViewCell

NSString *const BOTExpandedSelectionTableViewCellIdentifier = @"BOTExpandedSelectionTableViewCellIdentifier";

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.containerView.layer.cornerRadius = 10.0f;
    self.containerView.clipsToBounds = YES;
    self.containerView.layer.borderWidth = 1.0;
    self.containerView.layer.borderColor = BOTBorderGreyColor().CGColor;
    
    self.questions = [NSMutableArray arrayWithArray:@[ @"What is the guest WiFi network and password?", @"What is the employee Wifi network and password", @"What is the keycode to the [room/garage]", @"What is [person]'s extension" ]];
    
    UINib *cellNib = [UINib nibWithNibName:@"BOTExpandedQuestionTableViewCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[BOTExpandedQuestionTableViewCell reuseIdentifier]];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.questions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOTExpandedQuestionTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[BOTExpandedQuestionTableViewCell reuseIdentifier]];
    NSString *questionString = self.questions[indexPath.row];
    [cell setButtonText:questionString];
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 26;
}

- (void)expandedQuestionCell:(BOTExpandedQuestionTableViewCell *)cell didTapActionButton:(UIButton *)sender
{
    if ([(NSObject *)self.delegate respondsToSelector:@selector(expandedSelectionTableViewCell:didTapQuestionButton:)]) {
        [self.delegate expandedSelectionTableViewCell:self didTapQuestionButton:sender];
    }
}

- (void)setMainLabelText:(NSString *)text
{
    [self.mainLabel setText:text];
}

- (CGFloat)cellHeight
{
    CGFloat tableHeight = [self.questions count] * questionCellHeight;
    return tableHeight + standardCellHeight;
}

+ (NSString *)reuseIdentifier
{
    return BOTExpandedSelectionTableViewCellIdentifier;
}

@end
