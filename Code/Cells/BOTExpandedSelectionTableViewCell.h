//
//  BOTExpandedSelectionTableViewCell.h
//  Staples
//
//  Created by Reid Weber on 1/26/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BOTExpandedSelectionTableViewCell;

/**
 @abstract 'BOTExpandedSelectionTableViewCelldelegate' passes the selected Answer cell up the delegate chain to the reciever.
 @discussion The reciever will be responsible for saving the selected answer to the local Core Data store and sending it up with the relevant userId to the server.
 */
@protocol BOTExpandedSelectionTableViewCellDelegate

- (void)expandedSelectionTableViewCell:(BOTExpandedSelectionTableViewCell *)cell didTapQuestionButton:(UIButton *)sender;

@end

/**
 @abstract 'BOTExpandedSelectionTableViewCell' appears when the 'BOTSelectionTableViewCell' is selected. The 'BOTExpandedQuestionTableViewCellDelegate' notifies the reciever when one of its cells is selected.
 @discussion The expanded cell owns a tableView that holds all the common answers sent from the server. These answers are sorted into cells that are displayed with a button that will send it's data up a delegate chain to this class' reciever.
 */
@interface BOTExpandedSelectionTableViewCell : UITableViewCell

@property (weak, nonatomic) id<BOTExpandedSelectionTableViewCellDelegate> delegate;

@property (strong, nonatomic) NSMutableArray *questions;

- (void)setMainLabelText:(NSString *)text;

+ (NSString *)reuseIdentifier;
- (CGFloat)cellHeight;

@end
