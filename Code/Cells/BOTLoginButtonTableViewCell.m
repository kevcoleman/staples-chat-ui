//
//  BOTLoginButtonTableViewCell.m
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTLoginButtonTableViewCell.h"
#import "BOTUtilities.h"

@interface BOTLoginButtonTableViewCell ()

@property (strong, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation BOTLoginButtonTableViewCell

NSString * const BOTLoginButtonTableViewCellReuseIdentifier = @"BOTLoginButtonTableViewCellReuseIdentifier";

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.loginButton setBackgroundColor:BOTDarkBlueColor()];
    [self.loginButton setTitle:@"Log In" forState:UIControlStateNormal];
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.loginButton.titleLabel setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightBold]];
    [self.loginButton addTarget:self action:@selector(loginButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginButton.layer setCornerRadius:10.0f];
}

+ (CGFloat)cellHeight
{
    return 72;
}

+ (NSString *)reuseIdentifier
{
    return BOTLoginButtonTableViewCellReuseIdentifier;
}

#pragma mark - Action Handlers

- (void)loginButtonTapped:(UIButton *)sender
{
    if([self.delegate respondsToSelector:@selector(loginButtonTableViewCellDidSelectLogin:)]){
        [self.delegate loginButtonTableViewCellDidSelectLogin:self];
    }
}

@end
