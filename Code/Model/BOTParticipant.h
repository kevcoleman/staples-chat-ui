//
//  BOTParticipant.h
//  Staples
//
//  Created by Reid Weber on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATLParticipant.h"

/**
 @abstract Local model class that conforms to ATLParticipant and ATLAvatarItem.
 @discussion This class is used to identify when the conversation changes from a bot communication to a Customer Service Representitive. When the switch happens, we can change the avatar image to the CSR image.
 */
@interface BOTParticipant : NSObject <ATLParticipant>

+ (nonnull instancetype)participantWithSender:(nonnull id<ATLParticipant>)sender;

@property (nonatomic, readonly, nullable) NSString *firstName;
@property (nonatomic, readonly, nullable) NSString *lastName;
@property (nonatomic, readonly, nullable) NSString *displayName;
@property (nonatomic, readonly, nullable) NSString *userID;

@property (nonatomic, readonly, nullable) NSURL *avatarImageURL;
@property (nonatomic, readonly, nullable) UIImage *avatarImage;
@property (nonatomic, readonly, nullable) NSString *avatarInitials;

@end
