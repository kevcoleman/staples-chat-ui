//
//  BOTStore.h
//  Staples
//
//  Created by Kevin Coleman on 12/27/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BOTStore : NSObject

+ (instancetype)storeLocaitonWithData:(NSDictionary *)data;

@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *addtress;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *openHours;
@property (nonatomic, strong) NSString *closeHours;

@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

@end
