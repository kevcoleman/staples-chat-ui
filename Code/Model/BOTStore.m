//
//  BOTStore.m
//  Staples
//
//  Created by Kevin Coleman on 12/27/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTStore.h"

NSString *const BOTStoreLocationMIMEType = @"application/json+storelocation";

NSString *const BOTStoreLocationCity = @"city";
NSString *const BOTStoreLocationState = @"state";
NSString *const BOTStoreLocationAddress = @"address";
NSString *const BOTStoreLocationAddressLine1 = @"addressLine1";
NSString *const BOTStoreLocationAddressLine2 = @"addressLine2";
NSString *const BOTStoreLocationPhone = @"phoneNumber";

//NSString *const BOTStoreLocationOpenTime = @"locationOpen";
//NSString *const BOTStoreLocationCloseTime = @"locationClose";

NSString *const BOTStoreLocationLatitude = @"latitude";
NSString *const BOTStoreLocaitonLongitude = @"longitude";

@implementation BOTStore

+ (instancetype)storeLocaitonWithData:(NSDictionary *)data
{
    return [[self alloc] initWithStoreLocationData:data];
}

- (id)initWithStoreLocationData:(NSDictionary *)data
{
    self = [super init];
    if (self) {
        NSDictionary *addressData = data[BOTStoreLocationAddress];
        
        NSString *storeCity = addressData[BOTStoreLocationCity];
        NSString *storeState = addressData[BOTStoreLocationState];
        
        self.city = [NSString stringWithFormat:@"%@, %@", storeCity, storeState];
        self.addtress = addressData[BOTStoreLocationAddressLine1];
        self.phone = addressData[BOTStoreLocationPhone];
        
        self.latitude = [data[BOTStoreLocationLatitude] doubleValue];
        self.longitude = [data[BOTStoreLocaitonLongitude] doubleValue];
        
        // Currently not supported in the Staples Store API
        //self.storeOpenHours = data[BOTStoreLocationOpenTime];
        //self.storeCloseHours = data[BOTStoreLocationCloseTime];
    }
    return self;
}

@end
