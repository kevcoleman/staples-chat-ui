//
//  BOTParticipant.m
//  Staples
//
//  Created by Reid Weber on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTParticipant.h"
#import "BOTUtilities.h"

NSString *const BOTIncomingBotAvatar = @"chat_bot_icon";

@implementation BOTParticipant

+ (instancetype)participantWithSender:(id<ATLParticipant>)sender
{
    return [[self alloc] initWithSender:sender];
}

- (instancetype)initWithSender:(id<ATLParticipant>)sender
{
    self = [super init];
    if (self) {
        
        // Check for bot or CSR
        
        _avatarImage = [UIImage imageNamed:BOTIncomingBotAvatar inBundle:StaplesUIBundle() compatibleWithTraitCollection:nil];
        _firstName = @"";
        _lastName = @"";
        _displayName = @"";
        _userID = @"";
    }
    return self;
}

@end
