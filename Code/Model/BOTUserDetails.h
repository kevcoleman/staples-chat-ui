//
//  BOTUserDetails.h
//  Staples
//
//  Created by Kevin Coleman on 2/1/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 `BOTUserDetails` is a convenience model used to contain initialization values for the BOTChat framework.
 */
@interface BOTUserDetails : NSObject

/*
 The userID for the user.
 */
@property (nonatomic, strong) NSString *userID;

/*s
 The companyID for the user.
 */
@property (nonatomic, strong) NSString *companyID;

/*
 The WCToken for the user.
 */
@property (nonatomic, strong) NSString *wcToken;

/*
 The WCTrustedToken for the user.
 */
@property (nonatomic, strong) NSString *wcTrustedToken;

/*
 The zip code for the user.
 */
@property (nonatomic, strong) NSString *zipCode;

/*
 The store id for the user.
 */
@property (nonatomic, strong) NSString *storeID;

/*
 The catalogID for the user.
 */
@property (nonatomic, strong) NSString *catalogID;

/*
 The locale for the user.
 */
@property (nonatomic, strong) NSString *locale;

@end
