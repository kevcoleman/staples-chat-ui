//
//  BOTChat.h
//  Mesh
//
//  Created by Kevin Coleman on 12/06/16.
//  Copyright (c) 2016 Mesh Studios LLC. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <LayerKit/LayerKit.h>
#import "BOTEasyServiceManager.h"
#import "BOTUserDetails.h"

typedef NS_ENUM(NSUInteger, BOTChatError) {
    BOTChatErrorAlreadyAuthenticated = 11001,
    BOTChatErrorNotConfigured = 11002,
    BOTChatErr = 11003,
    BOTChatErrAppIdMissing = 11004
};

extern NSString *const BOTChatEnabledNotification;

extern NSString *const BOTChatDisabledNotification;

extern NSString *const BOTConversationIDKey;

extern NSString *const BOTChatStartSuccessNotification;
extern NSString *const BOTChatStartFailureNotification;

extern NSUInteger const RetryCountExhaustedFailureCode;

/**
 @abstract BOTChat presents an inteface for coordinating interactions needed to prepare the Staples application for messaging via the LayerKit SDK.
 */
@interface BOTChat : NSObject

@property (nonatomic, readonly) BOTEasyServiceManager *serviceManager;
@property (nonatomic, readonly) NSString *easySystemBaseURL;
@property (nonatomic, readonly) NSString *userID;
@property (nonatomic, readonly) NSString *wcToken;
@property (nonatomic, readonly) NSString *wcTrustedToken;
@property (nonatomic, readonly) NSString *zipCode;
@property (nonatomic, readonly) NSString *storeID;
@property (nonatomic, readonly) NSString *catalogID;
@property (nonatomic, readonly) NSString *locale;
@property (nonatomic, readonly) NSNumber *latitude;
@property (nonatomic, readonly) NSNumber *longitude;
@property (atomic, readonly) LYRClient *layerClient;
@property (atomic, readonly) LYRConversation *conversation;

/**
 Returns a singleton w/ the prevously configured environment, or
 
 @param environment BOTEnvironment
 @return BOTChat instance
 */
+ (instancetype)sharedInstance;

/**
 Sets the BOTChat ENV
 
 @param environment BOTEnvironment
 @param completion
 */
- (void)updateEnvironment:(BOTEnvironment)environment completion:(void(^)(NSError *error))completion;

/**
 @abstract Connects the Chat pod for messaging.
 @param (optional) userDetails A `BOTUserDetails` object containing information about the user attempting to connect.
 @discussion Calling this method fetches Chat configuration from the Easy Server and connects the Layer SDK.
 */
- (void)connectWithUserDetails:(BOTUserDetails *)userDetails completion:(void(^)(NSError *error))completion;

/**
 @abstract Connects the Chat pod for messaging.
 @discussion Calling this method fetches Chat configuration from the Easy Server and connects the Layer SDK. baseURL, userName, userID, wcToken and wcTrustedToken values must be updated before calling.
 @param (optional) userID The identifier of the user attempting to chat.
 @param (optional) wcToken The wcToken of the user attempting to chat.
 @param (optional) wcTrustedToken The wcTrustedToken of the user attempting to chat.
 @param (optional) zipCode The zipCode of the user attempting to chat.
 @param (optional) storeID The storeID of the user attempting to chat.
 @param (optional) catalogID The catalogID of the user attempting to chat.
 @param (optional) locale The locale of the user attempting to chat.
 @param (optional) completion The block to be called upon completion of the operation.
 */
- (void)connectWithUserID:(NSString *)userID
                  wcToken:(NSString *)wcToken
           wcTrustedToken:(NSString *)wcTrustedToken
                  zipCode:(NSString *)zipCode
                  storeID:(NSString *)storeID
                catalogID:(NSString *)catalogID
                   locale:(NSString *)locale
               completion:(void(^)(NSError *error))completion;


/**
 Same connection method as above, but added retry functionality
 @param ....
 @param retryCount How many times to attempt reconnect
 @param ....
 */
- (void)connectWithUserID:(NSString *)userID
                  wcToken:(NSString *)wcToken
           wcTrustedToken:(NSString *)wcTrustedToken
                  zipCode:(NSString *)zipCode
                  storeID:(NSString *)storeID
                catalogID:(NSString *)catalogID
                   locale:(NSString *)locale
               retryCount:(NSUInteger)retryCount
               completion:(void(^)(NSError *error))completion;


/**
 @abstract Deauthenticates the Layer SDK.
 @discussion After calling this method, baseURL, userName, userID, wcToken and wcTrustedToken values must be updated before calling `connectWithCompletion:` again.
 @param completion A completion block to be called upon completion of the operation.
 */
- (void)deauthenticateWithCompletion:(void(^)(NSError *error))completion;

//------------------------------------------------------------------------------------
// Token Data
//------------------------------------------------------------------------------------

/**
 @abstract Returns the token data to be used as a message part in Layer messages.
 @return tokenData the messaging data.
 */
- (NSDictionary *)tokenData;

//------------------------------------------------------------------------------------
// Query Helpers
//------------------------------------------------------------------------------------

- (LYRConversation *)existingConversationForIdentifier:(NSURL *)identifier;

@end
