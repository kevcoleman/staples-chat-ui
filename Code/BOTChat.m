//
//  BOTChat.h
//  Mesh
//
//  Created by Kevin Coleman on 12/06/16.
//  Copyright (c) 2016 Mesh Studios LLC. All rights reserved.
//

#import "BOTChat.h"
#import <TransitionKit/TransitionKit.h>
#import <Lockbox/Lockbox.h>
#import <PromiseKit/AnyPromise.h>
#import <MapKit/MapKit.h>
#import "BOTPromiseManager.h"

#define kSNIsGuestSessionForAutoLogin @"SNIsGuestSessionForAutoLogin"
#define kSNWcTokenKey @"SNWcTokenKey"
#define kSNWcTrustedTokenKey @"SNWcTrustedTokenKey"
#define kSNAutoLoginKey @"SNAutoLogin"
#define kSNPasswordKey @"SNPassword"
#define kSNUserNameKey @"SNUserName"
#define kSNUserIdKey @"SNUserIdKey"
#define kSNPersonalizationIdKey @"SNPersonalizationIdKey"
#define kLayerMessagingUserInfoKey @"kLayerMessagingUserInfoKey"

@interface BOTChat () <LYRClientDelegate>

@property (atomic, readwrite) LYRClient *layerClient;

@property (nonatomic, copy, readwrite) NSString *easySystemBaseURL;
@property (nonatomic, copy, readwrite) NSString *userID;
@property (nonatomic, copy, readwrite) NSString *companyID;
@property (nonatomic, copy, readwrite) NSString *wcToken;
@property (nonatomic, copy, readwrite) NSString *wcTrustedToken;
@property (nonatomic, copy, readwrite) NSString *zipCode;
@property (nonatomic, copy, readwrite) NSString *storeID;
@property (nonatomic, copy, readwrite) NSString *catalogID;
@property (nonatomic, copy, readwrite) NSString *locale;
@property (nonatomic, readwrite) BOTEasyServiceManager *serviceManager;
@property (atomic, readwrite) LYRConversation *conversation;

@property (atomic) NSUInteger layerConnectionRetryCount;

@property (nonatomic) dispatch_queue_t chatQueue;

@property (nonatomic) BOTPromiseManager *promiseManager;

@end

@implementation BOTChat

// Errors
NSString *const BOTErrorDomain = @"BOTErrorDomain";

// Retry Ceiling
NSUInteger const MAX_LAYER_RETRY_ATTEMPTS = 15;
NSUInteger const RetryCountExhaustedFailureCode = 39982;

// Notification
NSString *const BOTChatStartSuccessNotification = @"BOTChatStartSuccessNotification";
NSString *const BOTChatStartFailureNotification = @"BOTChatStartFailureNotification";

// JSON Keys
NSString *const BOTNonceKey = @"BOTNonceKey";
NSString *const BOTIdentityTokenKey = @"BOTIdentityTokenKey";
NSString *const BOTConversationIDKey = @"BOTConversationIDKey";
NSString *const BOTChatErrorKey = @"BOTChatErrorKey";

// Singleton Guard
static bool usingSingletonInit;

#pragma mark Singleton functions

+ (id)sharedInstance
{
    static BOTChat *_sharedInstance;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        usingSingletonInit = true;
        _sharedInstance = [[self alloc] init];
    });
    return _sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Do not continue if we are not init'ing through the singleton convenience
        NSAssert(usingSingletonInit, @"Must use singleton constructor");
        usingSingletonInit = false;
        
        // Setting up serial queue for connection and deauth
        _chatQueue = dispatch_queue_create("chat", DISPATCH_QUEUE_SERIAL);
        
        // Defaulting to a production.
        _serviceManager = [BOTEasyServiceManager managerWithEnvironment:BOTEnvironmentProduction];
    }
    return self;
}

#pragma mark - Configuration


/**
 Sets the BOTChat ENV

 @param environment BOTEnvironment
 @param completion
 */
- (void)updateEnvironment:(BOTEnvironment)environment
               completion:(void(^)(NSError *error))completion
{
    // If we switch ENV and we're authenticated, enque a deauth
    if (environment != self.serviceManager.environment && self.layerClient.authenticatedUser) {
        [self deauthenticateWithCompletion:^(NSError *error) {
            if (error) {
                if (completion) {
                    return completion(error);
                }
            }
            
            self.serviceManager = [BOTEasyServiceManager managerWithEnvironment:environment];
            // Reauth w/ previous parameters
            [self connectWithUserID:self.userID
                            wcToken:self.wcToken
                     wcTrustedToken:self.wcTrustedToken
                            zipCode:self.zipCode
                            storeID:self.storeID
                          catalogID:self.catalogID
                             locale:self.locale
                         completion:completion];
        }];
    } else {
        // Set new ENV Manager
        self.serviceManager = [BOTEasyServiceManager managerWithEnvironment:environment];
        if (completion) {
            completion(nil);
        }
    }
}

+ (NSString *)deviceID
{
    // Device ID
    // Note: Per instruction from Staples (Shiv Agarwal), we are to
    // use the device's UUID, and explictly ignore auth state of the user.
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

#pragma mark - Configuration / Connection

#pragma mark - Public Methods

- (void)connectWithUserDetails:(BOTUserDetails *)userDetails completion:(void(^)(NSError *error))completion
{
    [self connectWithUserID:userDetails.userID
                    wcToken:userDetails.wcToken
             wcTrustedToken:userDetails.wcTrustedToken
                    zipCode:userDetails.zipCode
                    storeID:userDetails.storeID
                  catalogID:userDetails.catalogID
                     locale:userDetails.locale
                 completion:^(NSError *error) {
                     completion(error);
    }];
}

- (void)connectWithUserID:(NSString *)userID
                  wcToken:(NSString *)wcToken
           wcTrustedToken:(NSString *)wcTrustedToken
                  zipCode:(NSString *)zipCode
                  storeID:(NSString *)storeID
                catalogID:(NSString *)catalogID
                   locale:(NSString *)locale
               retryCount:(NSUInteger)retryCount
               completion:(void(^)(NSError *error))completion
{
    if (retryCount > 0) {
        NSLog(@"Attempting retry connect with count: %lu", (unsigned long)retryCount);
        [self connectWithUserID:userID
                        wcToken:wcToken
                 wcTrustedToken:wcTrustedToken
                        zipCode:zipCode
                        storeID:storeID
                      catalogID:catalogID
                         locale:locale
                     completion:^(NSError *error) {
                         if (error) {
                             [self connectWithUserID:userID
                                             wcToken:wcToken
                                      wcTrustedToken:wcTrustedToken
                                             zipCode:zipCode
                                             storeID:storeID
                                           catalogID:catalogID
                                              locale:locale
                                          retryCount:retryCount - 1
                                          completion:completion];
                         } else {
                             if (completion) {
                                 completion(nil);
                             }
                         }
        }];
    } else {
        if (completion) {
            NSString *errReason = @"Retry count exhausted";
            NSError *retryError = [NSError errorWithDomain:NSURLErrorDomain code:RetryCountExhaustedFailureCode userInfo:@{NSLocalizedDescriptionKey: errReason}];
            completion(retryError);
        }
    }
}

- (void)connectWithUserID:(NSString *)userID
                  wcToken:(NSString *)wcToken
           wcTrustedToken:(NSString *)wcTrustedToken
                  zipCode:(NSString *)zipCode
                  storeID:(NSString *)storeID
                catalogID:(NSString *)catalogID
                   locale:(NSString *)locale
               completion:(void(^)(NSError *error))completion
{
    // Copy
    NSString *__userID = [userID copy];
    NSString *__wcToken = [wcToken copy];
    NSString *__wcTrustedToken = [wcTrustedToken copy];
    NSString *__zipCode = [zipCode copy];
    NSString *__storeID = [storeID copy];
    NSString *__catalogID = [catalogID copy];
    NSString *__locale  = [locale copy];
    
    // Always fire completion on main thread
    void (^wrappedCompletion)(NSError *error) = ^void(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                self.userID = __userID;
                self.wcToken = __wcToken;
                self.wcTrustedToken = __wcTrustedToken;
                self.zipCode = __zipCode;
                self.storeID = __storeID;
                self.catalogID = __catalogID;
                self.locale  = __locale;
            }

            // Call Completion
            if (completion) {
                completion(error);
            }
        });
    };
    
    // Locally retain the service manager and client when this operation was enqueued
    BOTEasyServiceManager *serviceManager = self.serviceManager;
    
    // Dispath onto serial queue and commence connecting
    dispatch_async(self.chatQueue, ^{
        // Sema n Waittime
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        dispatch_time_t waitTime = dispatch_time(DISPATCH_TIME_NOW, 25.0 * NSEC_PER_SEC);
        
        // Carried Error and Layer Identity
        __block NSError *outerError;
        __block NSURL *outerConversationID;
        
        // Controlling flow with promises
        // Note: `- (AnyPromise *)then` dispatches onto the
        // main queue unless otherwise specified. We want to
        // just be sure that no operation dispatches back onto this
        // queue.
        AnyPromise *connectPromise = [AnyPromise promiseWithValue:nil];
        connectPromise.then(^{
            // Connect to Layer
            return [self connectToLayerWithLayerAppID:[serviceManager layerAppID]];
        }).then(^(BOTAppConfiguration *config) {
            // Carry the credentials
            outerConversationID = config.conversationID;
            
            // If auth'd as the same user, continue. If not, auth as a
            // new use
            LYRIdentity *identity = [self.layerClient authenticatedUser];
            if ([identity.userID isEqualToString:[[self class] deviceID]]) {
                // We're good to go - same user!
                // We assume that if we're authenticated, we have to have a conversation ID.
                return [BOTPromiseManager appConfigurationPromiseWithDeviceID:[[self class] deviceID]
                                                                    staplesUserID:__userID
                                                                            nonce:nil
                                                                           client:self.layerClient
                                                                   serviceManager:serviceManager];
            } else {
                // We need to deauth and reauth :(
                AnyPromise *authPromise = [AnyPromise promiseWithValue:nil];
                return authPromise.then(^{
                    // Deauth the user if they exist
                    return [BOTPromiseManager deauthenticationPromiseWithClient:self.layerClient];
                }).then(^{
                    // Retreive the identity nonce from Layer
                    return [BOTPromiseManager noncePromiseWithClient:self.layerClient];
                }).then(^(NSString *nonce){
                    // Send Identity Nonce to EasySystem for signing
                    return [BOTPromiseManager appConfigurationPromiseWithDeviceID:[[self class] deviceID]
                                                                    staplesUserID:self.userID
                                                                            nonce:nonce
                                                                           client:self.layerClient
                                                                   serviceManager:serviceManager];
                }).then(^(BOTAppConfiguration *config) {
                    outerConversationID = config.conversationID;
                    // Send Signed JWT from EasySystem to Layer
                    return [BOTPromiseManager authenticationPromiseWithDeviceID:[[self class] deviceID]
                                                                  IdentityToken:config.identityToken
                                                                         client:self.layerClient];
                });
            }
        }).then(^(id val) {
            if ([val class] == [LYRIdentity class]) {
                // Create conversation with layer
                return [BOTPromiseManager coversationPromiseWithID:outerConversationID layerIdentity:(LYRIdentity *)val client:self.layerClient];
            } else {
                BOTAppConfiguration *config = (BOTAppConfiguration *)val;
                // Create conversation with layer
                return [BOTPromiseManager coversationPromiseWithID:config.conversationID layerIdentity:self.layerClient.authenticatedUser client:self.layerClient];
            }
        }).then(^(LYRConversation *conversation) {
            // Set the conversation
            self.conversation = conversation;

            // Notify of chat connection success
            [self notifyStartSuccessWithConversation:conversation];
        }).catch(^(NSError *error){
            // We have a error, and connection process failed
            [self notifyStartFailureWithError:error];
            outerError = error;
        }).always(^() {
            // We're done. Free up the pipe
            dispatch_semaphore_signal(sema);
        });
        
        dispatch_semaphore_wait(sema, waitTime);
        wrappedCompletion(outerError);
    });
}

- (void)deauthenticateWithCompletion:(void (^)(NSError *))completion
{
    // Always fire completion on main thread
    void (^wrappedCompletion)(NSError *error) = ^void(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion(error);
            }
        });
    };
    
    // Locally retain the service manager and client when this operation was enqueued
    BOTEasyServiceManager *serviceManager = self.serviceManager;

    dispatch_async(self.chatQueue, ^{
        // Sema n Waittime
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        dispatch_time_t waitTime = dispatch_time(DISPATCH_TIME_NOW, 25.0 * NSEC_PER_SEC);
        
        // Carried Error
        __block NSError *outerError;

        [AnyPromise promiseWithValue:nil].then(^{
            return [self connectToLayerWithLayerAppID:[serviceManager layerAppID]];
        }).then(^{
            return [BOTPromiseManager deauthenticationPromiseWithClient:self.layerClient];
        }).catch(^(NSError *error){
            outerError = error;
        }).always(^() {
            // We're done. Free up the pipe
            dispatch_semaphore_signal(sema);
        });
        
        dispatch_semaphore_wait(sema, waitTime);
        wrappedCompletion(outerError);
    });
}

#pragma mark - Setup

// Connects to layer with a given Layer App ID.
- (AnyPromise *)connectToLayerWithLayerAppID:(NSString *)layerAppID
{
    return [AnyPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        
        // 1. If we have no LYRClient, or a new app ID, we create a LYRClient object.
        if (!self.layerClient || ![[self.layerClient.appID absoluteString] isEqualToString:layerAppID]) {
            NSURL *appID = [NSURL URLWithString:layerAppID];
            LYRClientOptions *options = [LYRClientOptions new];
            options.synchronizationPolicy = LYRClientSynchronizationPolicyCompleteHistory;
            self.layerClient = [LYRClient clientWithAppID:appID delegate:self options:options];
        }
        
        // 2. If Connected, just resolve
        if (self.layerClient.isConnected) {
            return resolve(nil);
        }
        
        // 3. If Connecting (this is so dumb), give it a bit and try again. Max
        // times to do this is set by MAX_LAYER_RETRY_ATTEMPTS
        if (self.layerClient.isConnecting && self.layerConnectionRetryCount < MAX_LAYER_RETRY_ATTEMPTS) {
            // Increment the retry counter
            self.layerConnectionRetryCount++;
            
            // Re-Dispatch, and resolve the resulting promise
            return dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                resolve([self connectToLayerWithLayerAppID:layerAppID]);
            });
        }
        
        // 4. Connect
        [self.layerClient connectWithCompletion:^(BOOL success, NSError * _Nullable error) {
            // We reset the count regardless of error.
            self.layerConnectionRetryCount = 0;
            
            if (error) {
                return resolve(error);
            }
            
            resolve(nil);
        }];
    }];
}

#pragma mark Layer Client Delegate

- (void)layerClient:(LYRClient *)client didReceiveAuthenticationChallengeWithNonce:(NSString *)nonce
{
    NSLog(@"Received Challenge");
    NSLog(@"Auth User Above: %@", client.authenticatedUser);
    AnyPromise *challengePromise = [AnyPromise promiseWithValue:nil];
    challengePromise.then(^(){
        // Send Identity Nonce to EasySystem for signing
        return [BOTPromiseManager appConfigurationPromiseWithDeviceID:[[self class] deviceID]
                                                        staplesUserID:self.userID
                                                                nonce:nonce
                                                               client:client
                                                       serviceManager:self.serviceManager];
    }).then(^(BOTAppConfiguration *appConfiguration){
        NSLog(@"Auth User Below (Should not be nil): %@", client.authenticatedUser);
        // Send Signed JWT from EasySystem to Layer
        [BOTPromiseManager authenticationPromiseWithDeviceID:[[self class] deviceID] IdentityToken:appConfiguration.identityToken client:self.layerClient];
    });
}

#pragma mark - Getters

- (NSURL *)easySystemBaseURL
{
    return self.serviceManager.baseURL;
}

# pragma mark - Dictionary for Messaging

- (NSDictionary *)tokenData
{
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    NSMutableDictionary *wcTokenDic = [NSMutableDictionary new];
    [wcTokenDic setValue:self.wcTrustedToken forKey:@"wcTrustedToken"];
    [wcTokenDic setValue:self.wcToken forKey:@"wcToken"];
    [wcTokenDic setValue:appVersion forKey:@"appVersion"];
    [wcTokenDic setValue:self.serviceManager.tenantID forKey:@"tenantId"];
    [wcTokenDic setValue:self.serviceManager.apiKey forKey:@"apiKey"];    
    [wcTokenDic setValue:self.userID forKey:@"userName"];
    [wcTokenDic setValue:[[self class] deviceID] forKey:@"userID"];
    [wcTokenDic setValue:self.zipCode forKey:@"zipCode"];
    [wcTokenDic setValue:self.catalogID forKey:@"catalogID"];
    [wcTokenDic setValue:self.storeID forKey:@"storeID"];
    [wcTokenDic setValue:self.locale forKey:@"locale"];
//    [wcTokenDic setValue:@"SA" forKey:@"appType"];
//    [wcTokenDic setValue:@"1018242nat" forKey:@"companyID"];
//    [wcTokenDic setValue:@"mesh2" forKey:@"userName"];
    
    CLLocationCoordinate2D coordinate = [self getDeviceLocation];
    [wcTokenDic setValue:[NSNumber numberWithDouble:47.6062] forKey:@"latitude"];
    [wcTokenDic setValue:[NSNumber numberWithDouble:-122.3321] forKey:@"longitude"];
    
    [[NSUserDefaults standardUserDefaults] setObject:wcTokenDic forKey:kLayerMessagingUserInfoKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return wcTokenDic;
}

#pragma mark - Query Helpers

- (NSUInteger)countOfUnreadMessages
{
    NSString *authenticatedUserID = self.layerClient.authenticatedUser.userID;
    LYRQuery *query = [LYRQuery queryWithQueryableClass:[LYRMessage class]];
    LYRPredicate *unreadPred =[LYRPredicate predicateWithProperty:@"isUnread" predicateOperator:LYRPredicateOperatorIsEqualTo value:@(YES)];
    LYRPredicate *userPred = [LYRPredicate predicateWithProperty:@"sender.userID" predicateOperator:LYRPredicateOperatorIsNotEqualTo value:authenticatedUserID];
    query.predicate = [LYRCompoundPredicate compoundPredicateWithType:LYRCompoundPredicateTypeAnd subpredicates:@[unreadPred, userPred]];
    return [self.layerClient countForQuery:query error:nil];
}

- (NSUInteger)countOfMessages
{
    LYRQuery *query = [LYRQuery queryWithQueryableClass:[LYRMessage class]];
    return [self.layerClient countForQuery:query error:nil];
}

- (NSUInteger)countOfConversations
{
    LYRQuery *query = [LYRQuery queryWithQueryableClass:[LYRConversation class]];
    return [self.layerClient countForQuery:query error:nil];
}

- (LYRMessage *)messageForIdentifier:(NSURL *)identifier
{
    LYRQuery *query = [LYRQuery queryWithQueryableClass:[LYRMessage class]];
    query.predicate = [LYRPredicate predicateWithProperty:@"identifier" predicateOperator:LYRPredicateOperatorIsEqualTo value:identifier];
    query.limit = 1;
    return [self.layerClient executeQuery:query error:nil].firstObject;
}

- (LYRConversation *)existingConversationForIdentifier:(NSURL *)identifier
{
    LYRQuery *query = [LYRQuery queryWithQueryableClass:[LYRConversation class]];
    query.predicate = [LYRPredicate predicateWithProperty:@"identifier" predicateOperator:LYRPredicateOperatorIsEqualTo value:identifier];
    query.limit = 1;
    return [self.layerClient executeQuery:query error:nil].firstObject;
}

- (LYRConversation *)existingConversationForParticipants:(NSSet *)participants
{
    LYRQuery *query = [LYRQuery queryWithQueryableClass:[LYRConversation class]];
    query.predicate = [LYRPredicate predicateWithProperty:@"participants" predicateOperator:LYRPredicateOperatorIsEqualTo value:participants];
    query.limit = 1;
    return [self.layerClient executeQuery:query error:nil].firstObject;
}

- (void)notifyStartSuccessWithConversation:(LYRConversation *)conversation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:BOTChatStartSuccessNotification object:conversation];
    });
}

- (void)notifyStartFailureWithError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:BOTChatStartFailureNotification object:error];
    });
}

// This is also defined in BOTStoreLocationCollectionViewCell.m... I wasn't sure where it should live so it lives in both for now
- (CLLocationCoordinate2D)getDeviceLocation
{
    CLLocationManager *manager = [[CLLocationManager alloc] init];
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    manager.distanceFilter = kCLDistanceFilterNone;
    [manager startUpdatingLocation];
    CLLocation *location = [manager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    [manager stopUpdatingLocation];
    return coordinate;
}

@end
