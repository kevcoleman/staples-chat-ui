//
//  STUtilities.h
//  
//
//  Created by Kevin Coleman on 8/19/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Atlas/Atlas.h>

//-------------------------------------------------
// Date Formatting
//-------------------------------------------------

typedef NS_ENUM(NSInteger, STCDateProximity) {
    STCDateProximityToday,
    STCDateProximityYesterday,
    STCDateProximityWeek,
    STCDateProximityYear,
    STCDateProximityOther,
};

NSDateFormatter * STCShortTimeFormatter();

NSDateFormatter * STCDayOfWeekDateFormatter();

NSDateFormatter * STCRelativeDateFormatter();

NSDateFormatter * STCThisYearDateFormatter();

NSDateFormatter * STCDefaultDateFormatter();

STCDateProximity STCProximityToDate(NSDate *date);


//-------------------------------------------------
// Fonts
//-------------------------------------------------

UIFont * BOTLightFont(CGFloat size);

UIFont * BOTRegularFont(CGFloat size);

UIFont * BOTBoldFont(CGFloat size);

//-------------------------------------------------
// Colors
//-------------------------------------------------

UIColor * BOTRedColor();

UIColor * BOTBlueColor();

UIColor * BOTDarkBlueColor();

UIColor * BOTGrayColor();

UIColor * BOTChatTextGrayColor();

UIColor * BOTLightGrayColor();

UIColor * BOTHeadlineGrayColor();

UIColor * BOTInstructionGrayColor();

UIColor * BOTInputGrayColor();

UIColor * BOTBorderGreyColor();

UIColor * BOTToolbarBorderGray();

//-------------------------------------------------
// Bundle
//-------------------------------------------------

NSBundle *StaplesUIBundle();

//-------------------------------------------------
// Message Part
//-------------------------------------------------

NSDictionary *DataForMessagePart(LYRMessagePart *part);

NSDictionary *onboardingPartData(LYRMessagePart *part);

//-------------------------------------------------
// Strings
//-------------------------------------------------

extern NSString *const BOTOptionTrackMyShipment;
extern NSString *const BOTOptionOrderNewSupplies;
extern NSString *const BOTOptionReorderLastShipment;
extern NSString *const BOTOptionScanSchoolSuppliesList;
extern NSString *const BOTOptionReturnItems;
extern NSString *const BOTOptionViewReceipt;
extern NSString *const BOTOptionViewMyRewards;
extern NSString *const BOTOptionViewMyAddress;
extern NSString *const BOTOptionCancelYourShipment;
extern NSString *const BOTOptionViewCouponWallet;
extern NSString *const BOTOptionCheckGiftCardBalance;

extern NSString *const BOTOptionPaper;
extern NSString *const BOTOptionRedSharpies;
extern NSString *const BOTOptionJournals;
extern NSString *const BOTOptionStaplers;

extern NSString *const BOTOnboardingTitle;
extern NSString *const BOTOnboardingImageURL;
extern NSString *const BOTOnboardingInstruction;
extern NSString *const BOTOnboardingButtonText;
extern NSString *const BOTOnboardingPlaceholder;

extern NSString *const BOTLeftButtonTitle;
extern NSString *const BOTRightButtonTitle;

extern NSString *const BOTOnboardingCardMIMEType;
extern NSString *const BOTOnboardingButtonMIMEType;

// Message Data Parsing
extern NSString *const BOTMessagePartDataKey;

@interface BOTUtilities : NSObject

@end
