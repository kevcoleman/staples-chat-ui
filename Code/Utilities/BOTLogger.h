//
//  BOTLogger.h
//  Staples
//
//  Created by Kevin Coleman on 12/9/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BOTLogger : NSObject

+ (void)verbose:(NSString *)formatString, ... NS_FORMAT_FUNCTION(1,2);

@end
