//
//  STUtilities.m
//  
//
//  Created by Kevin Coleman on 8/19/16.
//
//

#import "BOTUtilities.h"

NSDateFormatter *STCShortTimeFormatter()
{
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.timeStyle = NSDateFormatterShortStyle;
    }
    return dateFormatter;
}

NSDateFormatter *STCDayOfWeekDateFormatter()
{
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"EEEE"; // Tuesday
    }
    return dateFormatter;
}

NSDateFormatter *STCRelativeDateFormatter()
{
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateStyle = NSDateFormatterMediumStyle;
        dateFormatter.doesRelativeDateFormatting = YES;
    }
    return dateFormatter;
}

NSDateFormatter *STCThisYearDateFormatter()
{
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"E, MMM dd,"; // Sat, Nov 29,
    }
    return dateFormatter;
}

NSDateFormatter *STCDefaultDateFormatter()
{
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"MMM dd, yyyy,"; // Nov 29, 2013,
    }
    return dateFormatter;
}

STCDateProximity STCProximityToDate(NSDate *date)
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *now = [NSDate date];
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
    NSCalendarUnit calendarUnits = NSEraCalendarUnit | NSYearCalendarUnit | NSWeekOfMonthCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
#pragma GCC diagnostic pop
    NSDateComponents *dateComponents = [calendar components:calendarUnits fromDate:date];
    NSDateComponents *todayComponents = [calendar components:calendarUnits fromDate:now];
    if (dateComponents.day == todayComponents.day &&
        dateComponents.month == todayComponents.month &&
        dateComponents.year == todayComponents.year &&
        dateComponents.era == todayComponents.era) {
        return STCDateProximityToday;
    }
    
    NSDateComponents *componentsToYesterday = [NSDateComponents new];
    componentsToYesterday.day = -1;
    NSDate *yesterday = [calendar dateByAddingComponents:componentsToYesterday toDate:now options:0];
    NSDateComponents *yesterdayComponents = [calendar components:calendarUnits fromDate:yesterday];
    if (dateComponents.day == yesterdayComponents.day &&
        dateComponents.month == yesterdayComponents.month &&
        dateComponents.year == yesterdayComponents.year &&
        dateComponents.era == yesterdayComponents.era) {
        return STCDateProximityYesterday;
    }
    
    if (dateComponents.weekOfMonth == todayComponents.weekOfMonth &&
        dateComponents.month == todayComponents.month &&
        dateComponents.year == todayComponents.year &&
        dateComponents.era == todayComponents.era) {
        return STCDateProximityWeek;
    }
    
    if (dateComponents.year == todayComponents.year &&
        dateComponents.era == todayComponents.era) {
        return STCDateProximityYear;
    }
    
    return STCDateProximityOther;
}

UIFont * BOTLightFont(CGFloat size)
{
    return [UIFont fontWithName:@"SFUIText-Light" size:size];
}

UIFont * BOTRegularFont(CGFloat size)
{
    return [UIFont fontWithName:@"SFUIText-Regular" size:size];
}

UIFont * BOTBoldFont(CGFloat size)
{
    return [UIFont fontWithName:@"SFUIText-Bold" size:size];
}

NSBundle * StaplesUIBundle()
{
    // CocoaPods resource bundlep
    NSBundle *bundlePath = [NSBundle bundleWithIdentifier:@"org.cocoapods.staples-chat-ui"];
    NSString *path = [bundlePath pathForResource:@"StaplesResources" ofType:@"bundle"];
    NSBundle *resourcesBundle = [NSBundle bundleWithPath:path];
    if (resourcesBundle) {
        return resourcesBundle;
    }
    return [NSBundle mainBundle];
}

UIColor * BOTBlueColor()
{
    return [UIColor colorWithRed:74.0/255.0 green:144.0/255.0 blue:226.0/255.0 alpha:1.0];
}

UIColor * BOTRedColor()
{
    return [UIColor colorWithRed:204.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
}

UIColor *BOTChatBlueColor()
{
    return  [UIColor colorWithRed:74/255.0f green:144/255.0f blue:226/255.0f alpha:1.0];
}

UIColor * BOTCTABlueColor()
{
    return  [UIColor colorWithRed:74/255.0f green:144/255.0f blue:226/255.0f alpha:1.0];
}

UIColor * BOTLightGrayColor()
{
    return [UIColor colorWithRed:220/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1.0];
}

UIColor * BOTToolbarBorderGray()
{
    return [UIColor colorWithRed:228/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0];
}

UIColor * BOTGrayColor()
{
    return [UIColor colorWithRed:241/255.0f green:241/255.0f blue:241/255.0f alpha:1.0];
}

UIColor * BOTChatTextGrayColor()
{
    return [UIColor colorWithRed:128.0/255.0f green:128.0/255.0f blue:128.0/255.0f alpha:1.0];
}

UIColor * BOTInstructionGrayColor()
{
    return [UIColor colorWithRed:122.0/255.0f green:122.0/255.0f blue:122.0/255.0f alpha:1.0];
}

UIColor * BOTHeadlineGrayColor()
{
    return [UIColor colorWithRed:74.0/255.0f green:74.0/255.0f blue:74.0/255.0f alpha:1.0];
}

UIColor * BOTDarkBlueColor()
{
    return [UIColor colorWithRed:74/255.0f green:144/255.0f blue:226/255.0f alpha:1.0];
}

UIColor * BOTBorderGreyColor()
{
    return [UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:1];
}

NSDictionary *DataForMessagePart(LYRMessagePart *part)
{
    NSString *dataString = [[NSString alloc] initWithData:part.data encoding:NSUTF8StringEncoding];
    NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    if (error) {
        return nil;
    }
    return json;
}

NSDictionary *onboardingPartData(LYRMessagePart *part)
{
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:part.data options:NSJSONReadingAllowFragments error:&error];
    if (error) {
        return nil;
    }
    return json;
}

// Flows
NSString *const BOTOptionTrackMyShipment = @"Track My Shipment";
NSString *const BOTOptionOrderNewSupplies = @"Order New Supplies";
NSString *const BOTOptionReorderLastShipment = @"Reorder Last Shipment";
NSString *const BOTOptionScanSchoolSuppliesList = @"Scan School Supplies List";
NSString *const BOTOptionReturnItems = @"Return Items";
NSString *const BOTOptionViewReceipt = @"View Receipt";
NSString *const BOTOptionViewMyRewards = @"View My Rewards";
NSString *const BOTOptionViewMyAddress = @"View My Address";
NSString *const BOTOptionCancelYourShipment = @"Cancel Your Shipment";
NSString *const BOTOptionViewCouponWallet = @"View Coupon Wallet";
NSString *const BOTOptionCheckGiftCardBalance = @"Check Giftcard Balance";

NSString *const BOTOptionPaper = @"Paper";
NSString *const BOTOptionRedSharpies = @"Red Sharpies";
NSString *const BOTOptionJournals = @"Journals";
NSString *const BOTOptionStaplers = @"Staplers";

NSString *const BOTMessagePartDataKey = @"data";

// Onboarding
NSString *const BOTOnboardingTitle = @"onboarding-title";
NSString *const BOTOnboardingImageURL = @"onboarding-image-url";
NSString *const BOTOnboardingInstruction = @"onboarding-instruction";
NSString *const BOTOnboardingButtonText = @"onboarding-button-text";
NSString *const BOTOnboardingPlaceholder = @"onboarding-placeholder";

NSString *const BOTLeftButtonTitle = @"left-button-title";
NSString *const BOTRightButtonTitle = @"right-button-title";

NSString *const BOTOnboardingCardMIMEType = @"application/onboarding-card";
NSString *const BOTOnboardingButtonMIMEType = @"application/onboarding-button";

@implementation BOTUtilities

@end
