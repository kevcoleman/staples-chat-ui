//
//  BOTConversationHelper.h
//  Staples
//
//  Created by Kevin Coleman on 12/27/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BOTUtilities.h"
@interface BOTConversationHelper : NSObject

+ (LYRMessagePart *)tokenPayloadMessagePartWithMIMEType:(NSString *)MIMEType;

+ (LYRMessagePart *)tokenPayloadForMessagePartText:(NSString *)text MIMEType:(NSString *)MIMEType;

+ (NSAttributedString *)attributedStringForDisplayOfDate:(NSDate *)date;

+ (NSAttributedString *)attributedStringForDisplayOfRecipientStatus:(NSDictionary *)recipientStatus authenticatedUserID:(NSString *)authenticatedUserID;

@end
