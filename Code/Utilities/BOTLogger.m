//
//  BOTLogger.m
//  Staples
//
//  Created by Kevin Coleman on 12/9/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTLogger.h"

@implementation BOTLogger

+ (void)verbose:(NSString *)format, ...
{
    va_list arguments;
    va_start(arguments, format);
    
    [self log:format withArguments:arguments];
    va_end(arguments);
}

+ (void)log:(NSString *)string withArguments:(va_list)arguments
{
    NSString *log = [[NSString alloc] initWithFormat:string arguments:arguments];
    NSLog(@"%@", log);
}

@end

