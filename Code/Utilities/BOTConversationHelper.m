//
//  BOTConversationHelper.m
//  Staples
//
//  Created by Kevin Coleman on 12/27/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTConversationHelper.h"
#import "BOTChat.h"

@implementation BOTConversationHelper

+ (LYRMessagePart *)tokenPayloadMessagePartWithMIMEType:(NSString *)MIMEType
{
    // Add custom payload for all Staples messages.
    NSDictionary *wcTokenDic = [[BOTChat sharedInstance] tokenData];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:wcTokenDic options:NSJSONWritingPrettyPrinted  error:nil];
    return [LYRMessagePart messagePartWithMIMEType:MIMEType data:data];
}

+ (LYRMessagePart *)tokenPayloadForMessagePartText:(NSString *)text MIMEType:(NSString *)MIMEType
{
    // Add custom payload for all Staples messages.
    NSMutableDictionary *wcTokenDic = [[[BOTChat sharedInstance] tokenData] mutableCopy];
    BOOL isBTS = [[NSUserDefaults standardUserDefaults] boolForKey:@"isTappedBTSBanner"];
    if(isBTS){
        NSDictionary *flag = @{@"imageType":@"BTS"};
        [wcTokenDic addEntriesFromDictionary:flag];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:wcTokenDic options:NSJSONWritingPrettyPrinted  error:nil];
    return [LYRMessagePart messagePartWithMIMEType:MIMEType data:data];
}

+ (NSAttributedString *)attributedStringForDisplayOfDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter;
    STCDateProximity dateProximity = STCProximityToDate(date);
    switch (dateProximity) {
        case STCDateProximityToday:
        case STCDateProximityYesterday:
            dateFormatter = STCRelativeDateFormatter();
            break;
        case STCDateProximityWeek:
            dateFormatter = STCDayOfWeekDateFormatter();
            break;
        case STCDateProximityYear:
            dateFormatter = STCThisYearDateFormatter();
            break;
        case STCDateProximityOther:
            dateFormatter = STCDefaultDateFormatter();
            break;
    }
    
    NSString *dateString = [dateFormatter stringFromDate:date];
    NSString *timeString = [STCShortTimeFormatter() stringFromDate:date];
    
    NSMutableAttributedString *dateAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", dateString, timeString]];
    [dateAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, dateAttributedString.length)];
    [dateAttributedString addAttribute:NSFontAttributeName value:BOTLightFont(11) range:NSMakeRange(0, dateAttributedString.length)];
    [dateAttributedString addAttribute:NSFontAttributeName value:BOTRegularFont(11) range:NSMakeRange(0, dateString.length)];
    return dateAttributedString;
}

+ (NSAttributedString *)attributedStringForDisplayOfRecipientStatus:(NSArray *)recipientStatus authenticatedUserID:(NSString *)authenticatedUserID
{
    NSMutableDictionary *mutableRecipientStatus = [recipientStatus mutableCopy];
    if ([mutableRecipientStatus valueForKey:authenticatedUserID]) {
        [mutableRecipientStatus removeObjectForKey:authenticatedUserID];
    }
    
    NSString *statusString = [NSString new];
    if (mutableRecipientStatus.count > 1) {
        __block NSUInteger readCount = 0;
        __block BOOL delivered = NO;
        __block BOOL sent = NO;
        __block BOOL pending = NO;
        [mutableRecipientStatus enumerateKeysAndObjectsUsingBlock:^(NSString *userID, NSNumber *statusNumber, BOOL *stop) {
            LYRRecipientStatus status = statusNumber.integerValue;
            switch (status) {
                case LYRRecipientStatusInvalid:
                    break;
                case LYRRecipientStatusPending:
                    pending = YES;
                    break;
                case LYRRecipientStatusSent:
                    sent = YES;
                    break;
                case LYRRecipientStatusDelivered:
                    delivered = YES;
                    break;
                case LYRRecipientStatusRead:
                    readCount += 1;
                    break;
            }
        }];
        if (readCount) {
            NSString *participantString = readCount > 1 ? @"Participants" : @"Participant";
            statusString = [NSString stringWithFormat:@"Read by %lu %@", (unsigned long)readCount, participantString];
        } else if (pending) {
            statusString = @"Pending";
        }else if (delivered) {
            statusString = @"Delivered";
        } else if (sent) {
            statusString = @"Sent";
        }
    } else {
        __block NSString *blockStatusString = [NSString new];
        [mutableRecipientStatus enumerateKeysAndObjectsUsingBlock:^(NSString *userID, NSNumber *statusNumber, BOOL *stop) {
            if ([userID isEqualToString:authenticatedUserID]) return;
            LYRRecipientStatus status = statusNumber.integerValue;
            switch (status) {
                case LYRRecipientStatusInvalid:
                    blockStatusString = @"Not Sent";
                    break;
                case LYRRecipientStatusPending:
                    blockStatusString = @"Pending";
                    break;
                case LYRRecipientStatusSent:
                    blockStatusString = @"Sent";
                    break;
                case LYRRecipientStatusDelivered:
                    blockStatusString = @"Delivered";
                    break;
                case LYRRecipientStatusRead:
                    blockStatusString = @"Read";
                    break;
            }
        }];
        statusString = blockStatusString;
    }
    return [[NSAttributedString alloc] initWithString:statusString attributes:@{NSFontAttributeName : BOTLightFont(11)}];
}

@end
