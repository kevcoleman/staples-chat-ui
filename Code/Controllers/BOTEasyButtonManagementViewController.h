//
//  BOTEasyButtonManagementViewController.h
//  iPhoneCFA
//
//  Created by Jayashree on 22/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOTEasyServiceManager.h"

@class BOTEasyButtonManagementViewController;

@protocol BOTEasyButtonManagementDelegate <NSObject>

- (void)controllerDidTapNewButton:(BOTEasyButtonManagementViewController *)controller;

@end

@interface BOTEasyButtonManagementViewController : UIViewController

+ (id)controllerWithEasyServiceManager:(BOTEasyServiceManager *)manager;

@property (nonatomic, weak) id<BOTEasyButtonManagementDelegate> delegate;

@end
