//
//  BOTAudioRecordingsViewController.m
//  Staples
//
//  Created by Taylor Halliday on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTAudioRecordingsViewController.h"
#import "BOTAudioRecordingsDataSource.h"
#import "BOTAudioRecordingSummaryView.h"
#import "BOTUtilities.h"

@interface BOTAudioRecordingsViewController () <UITableViewDelegate, BOTAudioRecordingSummaryViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UILabel *instructionsLabel;
@property (nonatomic, strong) BOTAudioRecordingsDataSource *audioRecordingDataSource;

@end

@implementation BOTAudioRecordingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Voice Orders";
    [self layoutInstructionsLabel];
    [self layoutTableView];
    
    // Set done btn to get out
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(didTapDoneButton:)];
    doneItem.tintColor = [UIColor darkGrayColor];
    self.navigationItem.leftBarButtonItem = doneItem;
}

- (void)layoutInstructionsLabel
{
    // Layout tableview and setup datasource
    self.instructionsLabel = [[UILabel alloc] init];
    self.instructionsLabel.textAlignment = NSTextAlignmentCenter;
    self.instructionsLabel.font = [UIFont fontWithName:@"SFUIText-Light" size:12.0];
    self.instructionsLabel.textColor = [UIColor darkGrayColor];
    self.instructionsLabel.text = @"The items have already been added to your cart\nLet us know if we understood these requests so we\ncan continue to improve your experience.";
    self.instructionsLabel.numberOfLines = 3;
    self.instructionsLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.instructionsLabel];
    [self addInstructionsLabelConstraints];
}

- (void)layoutTableView
{
    // Layout tableview and setup datasource
    self.tableView = [[UITableView alloc] init];
    self.audioRecordingDataSource = [[BOTAudioRecordingsDataSource alloc] initWithTableView:self.tableView];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self.audioRecordingDataSource;
    self.tableView.contentOffset = CGPointMake(0.0, -130.0);
    self.tableView.contentInset = UIEdgeInsetsMake(130.0, 0.0, 0.0, 0.0);
    [self.view addSubview:self.tableView];
    [self addTableViewConstraints];
}

#pragma mark - Target/Action Responders

- (void)didTapDoneButton:(UIBarButtonItem *)btn
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableView Delegate Calls

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BOTAudioRecordingTableViewCell cellHeight];
}

#pragma mark - BOTAudioRecordingSummary Calls

- (void)playAudioWasTapped:(BOTAudioRecording *)audioRecording
{
    
}

- (void)productWasTapped:(BOTProduct *)product
{
    
}

#pragma mark - Layout Contraints

- (void)addInstructionsLabelConstraints
{
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.instructionsLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:60.0];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.instructionsLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:70.0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.instructionsLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.instructionsLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
    [self.view addConstraints:@[top, height, leading, trailing]];
}

- (void)addTableViewConstraints
{
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
    [self.view addConstraints:@[top, bottom, leading, trailing]];
}

@end
