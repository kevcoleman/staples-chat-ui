//
//  BOTAudioRecordingsDataSource.h
//  Staples
//
//  Created by Taylor Halliday on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "BOTAudioRecording.h"
#import "BOTAudioRecordingTableViewCell.h"

@interface BOTAudioRecordingsDataSource : NSObject <UITableViewDataSource>

@property (nonatomic, strong) NSArray <BOTAudioRecording *> *audioRecordings;
@property (nonatomic, weak) id <BOTAudioRecordingTableViewCellDelegate> delegate;

- (instancetype)initWithTableView:(UITableView *)tableView;

@end
