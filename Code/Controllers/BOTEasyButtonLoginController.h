//
//  BOTEasyLoginController.h
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOTEasyServiceManager.h"

@class BOTEasyButtonLoginController;

/**
 * The `BOTEasyButtonLoginControllerDelegate` notifies recievers as to the outcome of a login request.
 */
@protocol BOTEasyButtonLoginControllerDelegate <NSObject>

/**
 * Notifies the receiver of a succesful login with the response data.
 */
- (void)easyButtonLoginController:(BOTEasyButtonLoginController *)easyServerManager didLoginWithData:(NSDictionary *)data;

/**
 * Notifies the receiver of login failure with an error describing the failure.
 */
- (void)easyButtonLoginController:(BOTEasyButtonLoginController *)easyServerManager didFailLoginWithError:(NSError *)error;

@end

/**
 @abstract The `BOTEasyButtonLoginController` presents an interface for logging in users.
 */
@interface BOTEasyButtonLoginController : UITableViewController

/**
 * Designated Initializer.
 */
+ (id)controllerWithEasyServiceManager:(BOTEasyServiceManager *)manager;

/**
 * Convenience method to allow external object to request authentication.
 */
- (void)loginWithUserID:(NSString *)userID accountNumber:(NSString *)accountNumber password:(NSString *)password;

/**
 * The delegate object for the controller.
 */
@property (nonatomic) id<BOTEasyButtonLoginControllerDelegate>delegate;

@end
