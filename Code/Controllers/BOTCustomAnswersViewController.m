//
//  BOTCustomAnswersViewController.m
//  Staples
//
//  Created by Reid Weber on 1/26/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTCustomAnswersViewController.h"
#import "BOTMyCustomAnswersTableViewCell.h"
#import "BOTAddedAnswerTableViewCell.h"
#import "BOTEasyButtonHeaderTableViewCell.h"
#import "BOTCommonLabelTableViewCell.h"
#import "BOTEasyButtonTableViewCell.h"
#import "BOTButtonTableViewCell.h"
#import "BOTLogoutTableViewCell.h"
#import "BOTExpandedSelectionTableViewCell.h"
#import "BOTSelectionTableViewCell.h"
#import "BOTUtilities.h"
#import "BOTMyButtonsHeaderView.h"
#import "BOTLayerManager.h"
#import "BOTChat.h"
#import "STCConversationViewController.h"

typedef NS_ENUM(NSUInteger, BOTCustomAnswersSection) {
    BOTCustomAnswersHeader,
    BOTCustomAnswersMyAnswers,
    BOTCustomAnswersActions
};

typedef NS_ENUM(NSUInteger, BOTCustomAnswersRowActions) {
    BOTCustomAnswersConnectivity,
    BOTCustomAnswersWhosWho,
    BOTCustomAnswersFindIt,
    BOTCustomAnswersAddAnswer
};

@interface BOTCustomAnswersViewController () <UITableViewDelegate, UITableViewDataSource, BOTButtonTableViewCellDelegate, BOTSelectionTableViewCellDelegate, BOTExpandedSelectionTableViewCellDelegate, BOTMyCustomAnswersTableViewCellDelegate, STCConversationViewControllerDelegate>

@property (nonatomic) BOOL wifiCellIsExpanded;
@property (nonatomic) BOOL whoCellIsExpanded;
@property (nonatomic) BOOL findCellIsExpanded;

@property (nonatomic) UITableView *tableView;

@property (strong, nonatomic) NSString *answerTitle;
@property (strong, nonatomic) NSString *answerText;

@property (weak, nonatomic) BOTMyCustomAnswersTableViewCell *answersCell;

@end

@implementation BOTCustomAnswersViewController

NSString *const BOTWiFiandConnectivity = @"WiFi & Connectivity";
NSString *const BOTWhosWhoTitle        = @"Who's Who";
NSString *const BOTWhereToFindIt       = @"Where To Find It";

NSString *const BOTButtonAddAnAnswer = @"Add A Custom Answer";
NSString *const BOTButtonCellTitle   = @"Have a question that is unique to your business?";

NSString *const BOTNavigationAnswersTitle = @"Custom Answers";

NSString *const BOTHeaderViewText = @"The Easy Button is pretty smart.\nTeach it the answers to your most frequently\nasked questions, and put it to work!";
NSString *const BOTNoAnswersText  = @"You have no Custom Answers. \nChoose from the list below or write your own.";

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = BOTGrayColor();
    self.navigationItem.title = BOTNavigationAnswersTitle;
    
    UIImage *backButtonImage = [UIImage imageNamed:@"back_button"];
    UIImage *backButtonRender = [backButtonImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:backButtonRender style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed:)];
    backButton.imageInsets = UIEdgeInsetsMake(8.0, 0.0, 8.0, 16.0);
    [self.navigationItem setLeftBarButtonItem:backButton];
    
    self.wifiCellIsExpanded = false;
    self.whoCellIsExpanded = false;
    self.findCellIsExpanded = false;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.layer.cornerRadius = 10.0f;
    self.tableView.clipsToBounds = YES;
    self.tableView.backgroundColor = BOTGrayColor();
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    
    [self configureConstraints];
    [self registerCells];
}

- (void)configureConstraints
{
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
}

- (void)registerCells
{
    UINib *headerCell = [UINib nibWithNibName:@"BOTEasyButtonHeaderTableViewCell" bundle:nil];
    [self.tableView registerNib:headerCell forCellReuseIdentifier:[BOTEasyButtonHeaderTableViewCell reuseIdentifier]];
    
    UINib *buttonCell = [UINib nibWithNibName:@"BOTButtonTableViewCell" bundle:nil];
    [self.tableView registerNib:buttonCell forCellReuseIdentifier:[BOTButtonTableViewCell reuseIdentifier]];
    
    UINib *selectionCell = [UINib nibWithNibName:@"BOTSelectionTableViewCell" bundle:nil];
    [self.tableView registerNib:selectionCell forCellReuseIdentifier:[BOTSelectionTableViewCell reuseIdentifier]];
    
    UINib *customAnswersCell = [UINib nibWithNibName:@"BOTMyCustomAnswersTableViewCell" bundle:nil];
    [self.tableView registerNib:customAnswersCell forCellReuseIdentifier:[BOTMyCustomAnswersTableViewCell reuseIdentifier]];
    
    UINib *commonLabelCell = [UINib nibWithNibName:@"BOTCommonLabelTableViewCell" bundle:nil];
    [self.tableView registerNib:commonLabelCell forCellReuseIdentifier:[BOTCommonLabelTableViewCell reuseIdentifier]];
    
    UINib *expandedSelectionCell = [UINib nibWithNibName:@"BOTExpandedSelectionTableViewCell" bundle:nil];
    [self.tableView registerNib:expandedSelectionCell forCellReuseIdentifier:[BOTExpandedSelectionTableViewCell reuseIdentifier]];
}

#pragma mark - UITableViewDelegate and UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case BOTCustomAnswersHeader:
            cell = [tableView dequeueReusableCellWithIdentifier:[BOTEasyButtonHeaderTableViewCell reuseIdentifier]];
            [(BOTEasyButtonHeaderTableViewCell *)cell updateInstructionText:BOTHeaderViewText];
            break;
        case BOTCustomAnswersMyAnswers:
            cell = [tableView dequeueReusableCellWithIdentifier:[BOTMyCustomAnswersTableViewCell reuseIdentifier]];
            [(BOTMyCustomAnswersTableViewCell *)cell updateAnswersLabel:BOTNoAnswersText];
            self.answersCell = (BOTMyCustomAnswersTableViewCell *)cell;
            self.answersCell.delegate = self;
            break;
        case BOTCustomAnswersActions:
        default:
            switch (indexPath.row) {
                case BOTCustomAnswersConnectivity:
                    if (self.wifiCellIsExpanded == true) {
                        cell = [tableView dequeueReusableCellWithIdentifier:[BOTExpandedSelectionTableViewCell reuseIdentifier]];
                        BOTExpandedSelectionTableViewCell *selectedCell = (BOTExpandedSelectionTableViewCell *)cell;
                        selectedCell.delegate = self;
                        [selectedCell setMainLabelText:BOTWiFiandConnectivity];
                    } else {
                        cell = [tableView dequeueReusableCellWithIdentifier:[BOTSelectionTableViewCell reuseIdentifier]];
                        BOTSelectionTableViewCell *selectedCell = (BOTSelectionTableViewCell *)cell;
                        selectedCell.delegate = self;
                        [selectedCell updateSelectionText:BOTWiFiandConnectivity];
                    }
                    break;
                case BOTCustomAnswersWhosWho:
                    if (self.whoCellIsExpanded == true) {
                        cell = [tableView dequeueReusableCellWithIdentifier:[BOTExpandedSelectionTableViewCell reuseIdentifier]];
                        BOTExpandedSelectionTableViewCell *selectedCell = (BOTExpandedSelectionTableViewCell *)cell;
                        selectedCell.delegate = self;
                        [selectedCell setMainLabelText:BOTWhosWhoTitle];
                    } else {
                        cell = [tableView dequeueReusableCellWithIdentifier:[BOTSelectionTableViewCell reuseIdentifier]];
                        BOTSelectionTableViewCell *selectedCell = (BOTSelectionTableViewCell *)cell;
                        selectedCell.delegate = self;
                        [selectedCell updateSelectionText:BOTWhosWhoTitle];
                    }
                    break;
                case BOTCustomAnswersFindIt:
                    if (self.findCellIsExpanded == true) {
                        cell = [tableView dequeueReusableCellWithIdentifier:[BOTExpandedSelectionTableViewCell reuseIdentifier]];
                        BOTExpandedSelectionTableViewCell *selectedCell = (BOTExpandedSelectionTableViewCell *)cell;
                        selectedCell.delegate = self;
                        [selectedCell setMainLabelText:BOTWhereToFindIt];
                    } else {
                        cell = [tableView dequeueReusableCellWithIdentifier:[BOTSelectionTableViewCell reuseIdentifier]];
                        BOTSelectionTableViewCell *selectedCell = (BOTSelectionTableViewCell *)cell;
                        selectedCell.delegate = self;
                        [selectedCell updateSelectionText:BOTWhereToFindIt];
                    }
                    break;
                case BOTCustomAnswersAddAnswer:
                default:
                    cell = [tableView dequeueReusableCellWithIdentifier:[BOTButtonTableViewCell reuseIdentifier]];
                    BOTButtonTableViewCell *buttonCell = (BOTButtonTableViewCell *)cell;
                    [buttonCell updateButtonTitle:BOTButtonCellTitle];
                    [buttonCell updateTopLabelTitle:BOTButtonAddAnAnswer];
                    buttonCell.delegate = self;
                    break;
            }
            break;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    
    switch (indexPath.section) {
        case BOTCustomAnswersHeader:
            return [BOTEasyButtonHeaderTableViewCell cellHeight] + 20;
            break;
        case BOTCustomAnswersMyAnswers:
            if (!self.answersCell || ([self.answersCell numberOfAnswers] <= 0)) {
                return [BOTMyCustomAnswersTableViewCell startingCellHeight];
            } else {
                return [self.answersCell cellHeight];
            }
            break;
        case BOTCustomAnswersActions:
        default:
            switch (indexPath.row) {
                case BOTCustomAnswersConnectivity:
                    if (self.wifiCellIsExpanded == true) {
                        return 174;
                    } else {
                        return [BOTSelectionTableViewCell cellHeight];
                    }
                    
                    break;
                case BOTCustomAnswersWhosWho:
                    
                    if (self.whoCellIsExpanded == true) {
                        return 174;
                    } else {
                        return [BOTSelectionTableViewCell cellHeight];
                    }
                    
                    break;
                case BOTCustomAnswersFindIt:
                    
                    if (self.findCellIsExpanded == true) {
                        return 174;
                    } else {
                        return [BOTSelectionTableViewCell cellHeight];
                    }
                    
                    break;
                case BOTCustomAnswersAddAnswer:
                default:
                    return [BOTButtonTableViewCell cellHeight];
                    break;
            }
            break;
    }
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == BOTCustomAnswersActions) {
        UITableViewCell *cell;
        switch (indexPath.row) {
            case BOTCustomAnswersConnectivity:
                self.wifiCellIsExpanded = !self.wifiCellIsExpanded;
                cell = [tableView cellForRowAtIndexPath:indexPath];
                break;
            case BOTCustomAnswersWhosWho:
                self.whoCellIsExpanded = !self.whoCellIsExpanded;
                cell = [tableView cellForRowAtIndexPath:indexPath];
                break;
            case BOTCustomAnswersFindIt:
                self.findCellIsExpanded = !self.findCellIsExpanded;
                cell = [tableView cellForRowAtIndexPath:indexPath];
                break;
            default:
                break;
        }
        
        if (cell) {
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

- (void)selectionTableViewCell:(BOTSelectionTableViewCell *)cell didTapCellGesture:(UILongPressGestureRecognizer *)gesture
{
    NSIndexPath *indexPath;
    
    if ([cell.cellTitle isEqualToString:BOTWiFiandConnectivity]) {
        self.wifiCellIsExpanded = !self.wifiCellIsExpanded;
        indexPath = [NSIndexPath indexPathForRow:BOTCustomAnswersConnectivity inSection:BOTCustomAnswersActions];
    } else if ([cell.cellTitle isEqualToString:BOTWhosWhoTitle]) {
        self.whoCellIsExpanded = !self.whoCellIsExpanded;
        indexPath = [NSIndexPath indexPathForRow:BOTCustomAnswersWhosWho inSection:BOTCustomAnswersActions];
    } else if ([[cell cellTitle] isEqualToString:BOTWhereToFindIt]) {
        self.findCellIsExpanded = !self.findCellIsExpanded;
        indexPath = [NSIndexPath indexPathForRow:BOTCustomAnswersFindIt inSection:BOTCustomAnswersActions];
    }
    
    if (indexPath) {
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)expandedSelectionTableViewCell:(BOTExpandedSelectionTableViewCell *)cell didTapQuestionButton:(UIButton *)sender
{
    self.answerTitle = sender.titleLabel.text;
    NSString *conversation = [BOTChat sharedInstance].conversation.identifier.absoluteString;
    NSString *conversationID = [conversation stringByReplacingOccurrencesOfString:@"layer:///conversations/" withString:@""];
    BOTLayerManager *manager = [BOTLayerManager managerWithAppID:[BOTChat sharedInstance].layerClient.appID];
    [manager sendMessageText:sender.titleLabel.text toConversation:conversationID asSender:@"ESPSystem" completion:^(NSDictionary *data, NSError *error) {
        [self presentConversationViewController];
    }];
}

- (void)presentConversationViewController
{
    LYRClient *client = [BOTChat sharedInstance].layerClient;
    STCConversationViewController *conversationViewController = [STCConversationViewController conversationViewControllerWithLayerClient:client];
    conversationViewController.displaysAddressBar = NO;
    conversationViewController.hidesBottomBarWhenPushed = YES;
    conversationViewController.actionDelegate = self;
    conversationViewController.customAnswerText = self.answerTitle;
    [self.navigationController pushViewController:conversationViewController animated:YES];
}

- (void)conversationViewController:(STCConversationViewController *)conversationViewController didSetAnswer:(NSString *)answer
{
    [self.answersCell addNewAnswer:self.answerTitle];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case BOTCustomAnswersHeader:
            return 1;
            break;
        case BOTCustomAnswersMyAnswers:
            return 1;
            break;
        case BOTCustomAnswersActions:
        default:
            return 4;
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case BOTCustomAnswersMyAnswers: {
            BOTMyButtonsHeaderView *view = [[BOTMyButtonsHeaderView alloc] initWithFrame:CGRectZero];
            [view updateText:@"My Custom Answers"];
            return view;
        }
            break;
            
        case BOTCustomAnswersActions: {
            BOTMyButtonsHeaderView *view = [[BOTMyButtonsHeaderView alloc] initWithFrame:CGRectZero];
            [view updateText:@"Common Questions"];
            return view;
        }
            break;
            
        case BOTCustomAnswersHeader: {
            return [[BOTEasyButtonHeaderTableViewCell alloc] initWithFrame:CGRectZero];
        }
            break;
            
        default:
            break;
    }
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case BOTCustomAnswersHeader:
            return 20;
        
        case BOTCustomAnswersMyAnswers:
            return 40;
            
        case BOTCustomAnswersActions:
            return 40;
            
        default:
            return CGFLOAT_MIN;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

#pragma mark - 'BOTMyCustomAnswersTableViewCell' delegate and data management

- (void)customAnswersTableViewCell:(BOTMyCustomAnswersTableViewCell *)cell answerCellSelectedForEdit:(BOTAddedAnswerTableViewCell *)answerCell
{
    // Add Alert Controller?
}

- (void)customAnswersTableViewCell:(BOTMyCustomAnswersTableViewCell *)cell answerCellSelected:(BOTAddedAnswerTableViewCell *)answerCell
{
    // Add Alert Controller?
}

- (void)customAnswersTableViewCellDeletedTableViewRow:(BOTMyCustomAnswersTableViewCell *)cell
{
    [self.tableView reloadData];
}

- (void)saveTextForAnswer:(NSString *)answerTitle
{
    self.answerTitle = answerTitle;
    [self.answersCell addNewAnswer:self.answerTitle];
    [self.tableView reloadData];
}

- (void)answerTextFieldEditingChanged:(UITextField *)sender
{
    self.answerText = sender.text;
}

- (void)titleTextFieldEditingChanged:(UITextField *)sender
{
    self.answerTitle = sender.text;
}

#pragma mark 'BOTButtonTableViewCell' delegate

- (void)buttonTableViewCell:(BOTButtonTableViewCell *)cell didTapActionButton:(UIButton *)button
{
    [self showAlertController:nil withTitle:@"Add Custom Answer" isCustom:YES];
}

#pragma mark - Alert Controller for custom answer add/edit

- (void)showAlertController:(UIButton *)button withTitle:(NSString *)title isCustom:(BOOL)custom
{
    NSString *message;
    if (!custom) {
        message = button.titleLabel.text;
    }
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    if (custom) {
        [controller addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            [textField setPlaceholder:@"Question"];
            [textField addTarget:self action:@selector(titleTextFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
        }];
    }
    
    [controller addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        [textField setPlaceholder:@"Answer"];
        [textField addTarget:self action:@selector(answerTextFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (custom) {
            [self saveTextForAnswer:self.answerTitle];
        } else {
            [self saveTextForAnswer:button.titleLabel.text];
        }
        [controller dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        self.answerText = nil;
        self.answerTitle = nil;
        [controller dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [controller addAction:okAction];
    [controller addAction:cancel];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
