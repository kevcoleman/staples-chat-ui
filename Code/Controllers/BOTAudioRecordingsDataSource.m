//
//  BOTAudioRecordingsDataSource.m
//  Staples
//
//  Created by Taylor Halliday on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTAudioRecordingsDataSource.h"
#import "BOTAudioRecordingTableViewCell.h"

@interface BOTAudioRecordingsDataSource()

@property (nonatomic, weak) UITableView *tableView;

@end

@implementation BOTAudioRecordingsDataSource

#pragma mark - Init

- (instancetype)initWithTableView:(UITableView *)tableView
{
    self = [super init];
    if (self) {
        self.tableView = tableView;
        UINib *audioRecordingCellNib = [UINib nibWithNibName:@"BOTAudioRecordingTableViewCell" bundle:[NSBundle mainBundle]];
        [self.tableView registerNib:audioRecordingCellNib forCellReuseIdentifier:[BOTAudioRecordingTableViewCell reuseIdentifier]];
        self.audioRecordings = [self seedData];
    }
    return self;
}

#pragma mark - Setters/Getters

- (void)setAudioRecordings:(NSArray<BOTAudioRecording *> *)audioRecordings
{
    _audioRecordings = audioRecordings;
    [self.tableView reloadData];
}

- (void)setDelegate:(id<BOTAudioRecordingTableViewCellDelegate>)delegate
{
    _delegate = delegate;
    for (BOTAudioRecordingTableViewCell *cell in self.tableView.visibleCells) {
        cell.delegate = _delegate;
    }
}

#pragma mark - UITableViewDataSource Calls

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.audioRecordings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOTAudioRecordingTableViewCell *cell = (BOTAudioRecordingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:[BOTAudioRecordingTableViewCell reuseIdentifier]];
    [cell setAudioRecording:self.audioRecordings[indexPath.row]];
    [cell setDelegate:self.delegate];
    return cell;
}

#pragma mark - DEBUG / Delete this ish

- (NSArray <BOTAudioRecording *> *)seedData
{
    NSTimeInterval current = [[NSDate date] timeIntervalSince1970];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    NSDictionary *fakePriceDictData = @{
                                        @"unitOfMeasure": @"someVal",
                                        @"price": @"someVal",
                                        @"finalPrice": @"someVal",
                                        @"displayWasPricing": @(true),
                                        @"displayRegularPricing": @(true),
                                        @"buyMoreSaveMoreImage": @"someVal"
                                        };
    NSDictionary *fakeProductDictData = @{
                                   @"quantity": @"someVale",
                                   @"productImage": @"someVale",
                                   @"skuNo": @"someVale",
                                   @"sku": @"someVale",
                                   @"price": fakePriceDictData,
                                   @"productName": @"someVale",
                                   @"title": @"someVale"
                                   };
    for (int i = 0; i < 100; i++) {
        NSDictionary *btnData = @{
            @"buttonName" : @"Tay's Button",
            @"audioCreatedAt" : @(current - i * 234),
            @"product" : fakeProductDictData
            };
        [arr addObject:[BOTAudioRecording audioRecordingWithData:btnData]];
    }
    
    return [arr copy];
}

@end
