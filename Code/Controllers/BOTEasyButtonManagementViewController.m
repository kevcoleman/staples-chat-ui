//
//  BOTEasyButtonManagementViewController.m
//  iPhoneCFA
//
//  Created by Jayashree on 22/12/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import "BOTEasyButtonManagementViewController.h"
#import "BOTCustomAnswersViewController.h"
#import "BOTEasyButtonHeaderTableViewCell.h"
#import "BOTEasyButtonTableViewCell.h"
#import "BOTButtonTableViewCell.h"
#import "BOTLogoutTableViewCell.h"
#import "BOTSelectionTableViewCell.h"
#import "BOTUtilities.h"
#import "BOTMyButtonsHeaderView.h"

typedef NS_ENUM(NSUInteger, BOTEasyButtonSection) {
    BOTEasyButtonSectionHeader,
    BOTEasyButtonSectionButtons,
    BOTEasyButtonSectionActions,
};

typedef NS_ENUM(NSUInteger, BOTEasyButtonRowActions) {
    BOTEasyButtonRowAddNewButton,
    BOTEasyButtonRowCustomAnswers,
    BOTEasyButtonRowHelpCenter,
    BOTEasyButtonRowLogout,
};

@interface BOTEasyButtonManagementViewController () <UITableViewDelegate, UITableViewDataSource, BOTButtonTableViewCellDelegate, BOTSelectionTableViewCellDelegate>

@property (nonatomic) BOTEasyServiceManager *manager;
@property (nonatomic) UITableView *tableView;
@property (nonatomic) NSArray *easyButtons;

@end

@implementation BOTEasyButtonManagementViewController

NSString *const BOTCustomAnswers =  @"Custom Answers";
NSString *const BOTHelpCenter    = @"Help Center";

+ (id)controllerWithEasyServiceManager:(BOTEasyServiceManager *)manager
{
    return [[self alloc] initWithEasyServiceManager:manager];
}

- (id)initWithEasyServiceManager:(BOTEasyServiceManager *)manager
{
    self = [super init];
    if (self) {
        _manager = manager;
        _easyButtons = @[@"Test 1", @"Testing 5"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = BOTGrayColor();
    self.navigationController.navigationBar.hidden = NO;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.layer.cornerRadius = 10.0f;
    self.tableView.clipsToBounds = YES;
    self.tableView.backgroundColor = BOTGrayColor();
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.tableView];
    
    [self configureConstraints];
    [self registerCells];
}

- (void)configureConstraints
{
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
    
}

- (void) registerCells
{
    UINib *headerCell = [UINib nibWithNibName:@"BOTEasyButtonHeaderTableViewCell" bundle:nil];
    [self.tableView registerNib:headerCell forCellReuseIdentifier:[BOTEasyButtonHeaderTableViewCell reuseIdentifier]];
    
    UINib *easyButtonCell = [UINib nibWithNibName:@"BOTEasyButtonTableViewCell" bundle:nil];
    [self.tableView registerNib:easyButtonCell forCellReuseIdentifier:[BOTEasyButtonTableViewCell reuseIdentifier]];
    
    UINib *buttonCell = [UINib nibWithNibName:@"BOTButtonTableViewCell" bundle:nil];
    [self.tableView registerNib:buttonCell forCellReuseIdentifier:[BOTButtonTableViewCell reuseIdentifier]];
    
    UINib *selectionCell = [UINib nibWithNibName:@"BOTSelectionTableViewCell" bundle:nil];
    [self.tableView registerNib:selectionCell forCellReuseIdentifier:[BOTSelectionTableViewCell reuseIdentifier]];
    
    UINib *logoutCell = [UINib nibWithNibName:@"BOTLogoutTableViewCell" bundle:nil];
    [self.tableView registerNib:logoutCell forCellReuseIdentifier:[BOTLogoutTableViewCell reuseIdentifier]];
}

/**
 *   didSelectClose - tap of back button will dismiss the screen
 */
- (void)didSelectClose
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark : Tableview datasource methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case BOTEasyButtonSectionHeader:
            return 1;
            break;
            
        case BOTEasyButtonSectionButtons:
            return self.easyButtons.count;
            break;
            
        case BOTEasyButtonSectionActions:
            return 4;
            break;
            
        default:
            break;
    }
    return 0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case BOTEasyButtonSectionHeader:
            return [BOTEasyButtonHeaderTableViewCell cellHeight];
            break;
        case BOTEasyButtonSectionButtons:
            return [BOTEasyButtonTableViewCell cellHeight];
            break;
        case BOTEasyButtonSectionActions:
            switch (indexPath.row) {
                case BOTEasyButtonRowAddNewButton:
                    return [BOTButtonTableViewCell cellHeight];
                    break;
                    
                case BOTEasyButtonRowCustomAnswers:
                    return [BOTSelectionTableViewCell cellHeight];
                    break;
                    
                case BOTEasyButtonRowHelpCenter:
                    return [BOTSelectionTableViewCell cellHeight];
                    break;
                    
                case BOTEasyButtonRowLogout:
                    return [BOTLogoutTableViewCell cellHeight];
                    break;
                    
                default:
                    break;
            }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    switch (indexPath.section) {
        case BOTEasyButtonSectionHeader:
            cell = [tableView dequeueReusableCellWithIdentifier:[BOTEasyButtonHeaderTableViewCell reuseIdentifier]];
            break;
        case BOTEasyButtonSectionButtons:
            cell = [tableView dequeueReusableCellWithIdentifier:[BOTEasyButtonTableViewCell reuseIdentifier]];
            [(BOTEasyButtonTableViewCell *)cell updateButtonName:@"Test Name"];
            break;
        case BOTEasyButtonSectionActions:
            switch (indexPath.row) {
                case BOTEasyButtonRowCustomAnswers: {
                    cell = [tableView dequeueReusableCellWithIdentifier:[BOTSelectionTableViewCell reuseIdentifier]];
                    BOTSelectionTableViewCell *selectionCell = (BOTSelectionTableViewCell *)cell;
                    [selectionCell updateSelectionText:BOTCustomAnswers];
                    selectionCell.delegate = self;
                    break;
                }
                case BOTEasyButtonRowHelpCenter: {
                    cell = [tableView dequeueReusableCellWithIdentifier:[BOTSelectionTableViewCell reuseIdentifier]];
                    BOTSelectionTableViewCell *selectionCell = (BOTSelectionTableViewCell *)cell;
                    [selectionCell updateSelectionText:@"Help Center"];
                    selectionCell.delegate = self;
                    break;
                }
                case BOTEasyButtonRowLogout:
                    cell = [tableView dequeueReusableCellWithIdentifier:[BOTLogoutTableViewCell reuseIdentifier]];
                    break;
                case BOTEasyButtonRowAddNewButton:
                default:
                    cell = [tableView dequeueReusableCellWithIdentifier:[BOTButtonTableViewCell reuseIdentifier]];
                    BOTButtonTableViewCell *buttonCell = (BOTButtonTableViewCell *)cell;
                    buttonCell.delegate = self;
                    break;
            }
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case BOTEasyButtonSectionHeader:
            break;
        case BOTEasyButtonSectionButtons:
            break;
        case BOTEasyButtonSectionActions:
            switch (indexPath.row) {
                case BOTEasyButtonRowAddNewButton:
                    break;
                case BOTEasyButtonRowCustomAnswers: {
                    BOTCustomAnswersViewController *viewController = [[BOTCustomAnswersViewController alloc] init];
                    [self.navigationController pushViewController:viewController animated:YES];
                    break;
                }
                case BOTEasyButtonRowHelpCenter:
                    break;
                case BOTEasyButtonRowLogout:
                default:
                    break;
            }
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case BOTEasyButtonSectionHeader:
            return 20;
            break;
        case BOTEasyButtonSectionActions:
            break;
        case BOTEasyButtonSectionButtons:
            return 40;
            break;
        default:
            break;
    }
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case BOTEasyButtonSectionHeader:
        case BOTEasyButtonSectionActions:
            break;
        case BOTEasyButtonSectionButtons: {
            return [[BOTMyButtonsHeaderView alloc] initWithFrame:CGRectZero];
        }
            break;
        default:
            break;
    }
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

#pragma mark - Actions

- (void)selectionTableViewCell:(BOTSelectionTableViewCell *)cell didTapCellGesture:(UILongPressGestureRecognizer *)gesture
{
    if ([[cell cellTitle] isEqualToString:BOTCustomAnswers]) {
        BOTCustomAnswersViewController *viewController = [[BOTCustomAnswersViewController alloc] init];
        [self.navigationController pushViewController:viewController animated:YES];
    } else if ([[cell cellTitle] isEqualToString:BOTHelpCenter]) {
        // Implement Help Center
    }
}

- (void)didTapDoneButton:(UIButton *)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)buttonTableViewCell:(BOTButtonTableViewCell *)cell didTapActionButton:(UIButton *)button
{
    if ([self.delegate respondsToSelector:@selector(controllerDidTapNewButton:)]) {
        [self.delegate controllerDidTapNewButton:self];
    }
}

@end
