//
//  STCConversationViewController.h
//  MyLayerChat
//
//  Created by Mindstix Software on 11/02/16.
//  Copyright © 2016 Mindstix Software. All rights reserved.
//

#import <Atlas/Atlas.h>
#import "BOTShipment.h"
#import "BOTReward.h"
#import "BOTProduct.h"
#import "BOTStore.h"
#import "BOTUserDetails.h"

typedef NS_ENUM(NSUInteger, BOTLogType) {
    BOTLogTypeError,
    BOTLogTypeInfo,
    BOTLogTypeDebug,
};

@class STCConversationViewController;

/**
 The `STCConversationViewControllerDataSource` protocol requests data from its data source for display in the STCConversationViewController.  
 */
@protocol STCConversationViewControllerDataSource <NSObject>

@optional

/**
 Asks the data source for a badge count;
 */
- (NSUInteger)cartBadgeCountForconversationViewController:(STCConversationViewController *)conversationViewController;

@end

/**
 The `STCConversationViewControllerDelegate` protocol informs its delegate when tap actions have occured with in the STCCOnversationViewController.
 */
@protocol STCConversationViewControllerDelegate <NSObject>

@optional

/**
 * Notifies the receiver that a chat `Rewards` card has been tapped.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController didTapRewardCard:(BOTReward *)rewardNumber;

/**
 * Notifies the receiver that the `View All Products` button was tapped in a product carousel.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController didTapViewAllProducts:(NSArray<BOTProduct *> *)products;

/**
 * Notifies the receiver that the Add To Cart button was tapped in a product cell.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController didTapAddProductToCart:(BOTProduct *)product;

/**
 * Notifies the receiver that a `Product` card was tapped.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController didTapProductCard:(BOTProduct *)product;

/**
 * Notifies the receiver that the shipment tracking card was tapped.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController didTapShipmentCard:(BOTShipment *)shipment;

/**
 * Notifies the receiver that the view cart button was tapped.
 */
- (void)conversationViewControllerDidTapViewCart:(STCConversationViewController *)conversationViewController;

/**
 * Notifies the receiver that the 'View All' button of the shipment tracking card was tapped.
 */
- (void)conversationViewControllerDidTapViewAllOrders:(STCConversationViewController *)conversationViewController;

/**
 * Notifies the receiver that the 'Track my shipment' button of the shipment tracking card was tapped.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController didTapTrackOrderShipmentData:(BOTShipment *)shipment;
    
/**
 * Notifies the receiver that the 'Add to Apple Wallet' button of the rewards card was tapped.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController didTapAddToAppleWallet:(BOTReward *)reward;

/**
 * Notifies the receiver that the 'Add with Store' button of the Add to apple wallet popup.
 */
- (void)conversationViewControllerDidSelectRewardsAddWithStore:(STCConversationViewController *)conversationViewController;
    
/**
 * Notifies the receiver that the 'Add without Store' button of the Add to apple wallet popup.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController didSelectRewardsAddWithoutStore:(BOTStore *)store;

/**
 *  Notifies the receiver that it should leave a bread crumb.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController leaveBreadCrumb:(NSString *)breadCrumb;

/**
 *  Notifies the receiver that it should refresh tokesn for the user.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController shouldRefreshTokens:(NSDictionary *)tokens;

/**
 * Notifies the receiver that an analytics track event occured.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController trackAnalyticsState:(NSString *)state isReactive:(BOOL)reactive;

/**
 *  Notifies the receiver that it should show a spinner in the supplied view.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController showSpinner:(BOOL)showSpinner InView:(UIView *)view;

/**
 *  Notifies the receiver that it should log a message with the specified level.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController logMesssage:(NSString *)message withType:(BOTLogType)type;

/**
 *  Notifies the receiver that a custom answer has been set.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController didSetAnswer:(NSString *)answer;

/**
 *  Notifies the receiver that store card has been tapped.
 */
- (void)conversationViewController:(STCConversationViewController *)conversationViewController didTapStore:(BOTStore *)store;

@end

/**
 *  Declaration to make superclass methods available
 */
@interface ATLConversationViewController ()

- (void)updateBottomCollectionViewInset;

- (void)scrollToBottomAnimated:(BOOL)animated;

@end

/**
 @abstract Subclass of the `STCConversationViewController`. Presents a user interface for displaying and sending messages.
 */
@interface STCConversationViewController : ATLConversationViewController<ATLConversationViewControllerDataSource, ATLConversationViewControllerDelegate, UIAlertViewDelegate>

/**
 *  Update the user details of the conversation view controller.
 */
- (void)updateUserDetails:(BOTUserDetails *)userDetails;

/**
 *  Convenience to tell the controller to scroll to the bottom of its conversation
 */
- (void)scrollToBottomOfConversation;

@property (nonatomic, weak) id<STCConversationViewControllerDelegate> actionDelegate;

@property (nonatomic, weak) id<STCConversationViewControllerDataSource> actionDataSource;

@property (nonatomic) NSString *customAnswerText;

@end

