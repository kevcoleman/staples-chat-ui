//
//  BOTEasyLoginController.m
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTEasyButtonLoginController.h"
#import "BOTLoginHeroImageTableViewCell.h"
#import "BOTLoginHeadlineTableViewCell.h"
#import "BOTLoginInstructionsTableViewCell.h"
#import "BOTCustomInputAccessoryView.h"
#import "BOTLoginInputTableViewCell.h"
#import "BOTLoginButtonTableViewCell.h"
#import "BOTUtilities.h"
#import "BOTEasyButtonManagementViewController.h"

typedef NS_ENUM(NSUInteger, BOTLoginRow) {
    BOTLoginRowHeroImage,
    BOTLoginRowHeadline,
    BOTLoginRowInstructions,
    BOTLoginRowAccountNumber,
    BOTLoginRowUserID,
    BOTLoginRowPassword,
    BOTLoginRowButton
};

typedef NS_ENUM(NSUInteger, BOTTextFieldIndex) {
    BOTTextFieldIndexAccountNumber,
    BOTTextFieldIndexUserID,
    BOTTextFieldIndexPassword
};

@interface BOTEasyButtonLoginController ()<BOTLoginButtonTableViewCellDelegate, BOTLoginHeroImageTableViewCellDelegate, BOTCustomInputAccesoryViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) UITextField *accountTextField;
@property (weak, nonatomic) UITextField *userIDTextField;
@property (weak, nonatomic) UITextField *passwordTextField;
@property (nonatomic) BOTTextFieldIndex currentIndex;
@property (nonatomic) BOTEasyServiceManager *manager;
@property (nonatomic) UIView *spinnerView;

@end

@implementation BOTEasyButtonLoginController

CGFloat const BOTContentOffsetY = 100;

+ (id)controllerWithEasyServiceManager:(BOTEasyServiceManager *)manager
{
    return [[self alloc] initWithEasyServiceManager:manager];
}

- (id)initWithEasyServiceManager:(BOTEasyServiceManager *)manager
{
    self = [super init];
    if (self) {
        _manager = manager;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerTableViewCells];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.allowsSelection = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.scrollEnabled = NO;
    [self.tableView setContentInset:UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)registerTableViewCells
{
    UINib *heroCell = [UINib nibWithNibName:@"BOTLoginHeroImageTableViewCell" bundle:StaplesUIBundle()];
    [self.tableView registerNib:heroCell forCellReuseIdentifier:[BOTLoginHeroImageTableViewCell reuseIdentifier]];
    
    UINib *headlineCell = [UINib nibWithNibName:@"BOTLoginHeadlineTableViewCell" bundle:StaplesUIBundle()];
    [self.tableView registerNib:headlineCell forCellReuseIdentifier:[BOTLoginHeadlineTableViewCell reuseIdentifier]];
    
    UINib *instructionCell = [UINib nibWithNibName:@"BOTLoginInstructionsTableViewCell" bundle:StaplesUIBundle()];
    [self.tableView registerNib:instructionCell forCellReuseIdentifier:[BOTLoginInstructionsTableViewCell reuseIdentifier]];
    
    UINib *inputCell = [UINib nibWithNibName:@"BOTLoginInputTableViewCell" bundle:StaplesUIBundle()];
    [self.tableView registerNib:inputCell forCellReuseIdentifier:[BOTLoginInputTableViewCell reuseIdentifier]];
    
    UINib *buttonCell = [UINib nibWithNibName:@"BOTLoginButtonTableViewCell" bundle:StaplesUIBundle()];
    [self.tableView registerNib:buttonCell forCellReuseIdentifier:[BOTLoginButtonTableViewCell reuseIdentifier]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    switch (indexPath.row) {
        case BOTLoginRowHeroImage:
            height = [self heightForHeroCell];
            break;
            
        case BOTLoginRowHeadline:
            height = [BOTLoginHeadlineTableViewCell cellHeight];
            break;
            
        case BOTLoginRowInstructions:
            height = [BOTLoginInstructionsTableViewCell cellHeight];
            break;
            
        case BOTLoginRowAccountNumber:
        case BOTLoginRowUserID:
        case BOTLoginRowPassword:
            height = [BOTLoginInputTableViewCell cellHeight];
            break;
            
        case BOTLoginRowButton:
            height = [BOTLoginButtonTableViewCell cellHeight];
            break;
            
        default:
            break;
    }
    return height;
}

- (CGFloat)heightForHeroCell
{
    return self.tableView.bounds.size.height -
    [BOTLoginHeadlineTableViewCell cellHeight] -
    [BOTLoginInstructionsTableViewCell cellHeight] -
    ([BOTLoginInputTableViewCell cellHeight] * 3.0) -
    [BOTLoginButtonTableViewCell cellHeight] -
    44.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.row) {
        case BOTLoginRowHeroImage: {
            NSString *identifier = [BOTLoginHeroImageTableViewCell reuseIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
            BOTLoginHeroImageTableViewCell *loginHeroImageTableViewCell = (BOTLoginHeroImageTableViewCell *)cell;
            loginHeroImageTableViewCell.delegate = self;
        }
            break;
            
        case BOTLoginRowHeadline:{
            NSString *identifier = [BOTLoginHeadlineTableViewCell reuseIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        }
            break;
            
        case BOTLoginRowInstructions: {
            NSString *identifier = [BOTLoginInstructionsTableViewCell reuseIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        }
            break;
        
        case BOTLoginRowAccountNumber: {
            NSString *identifier = [BOTLoginInputTableViewCell reuseIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
            [(BOTLoginInputTableViewCell *)cell updateTitleLabel:@"Account Number"];
            [(BOTLoginInputTableViewCell *)cell updateWithLeftButtonText:@"What is this?" enabled:YES];
            [(BOTLoginInputTableViewCell *)cell updateWithRightButtonText:@"Forgot Account Number"];
            BOTLoginInputTableViewCell *loginInputTableViewCell = (BOTLoginInputTableViewCell *)cell;
            loginInputTableViewCell.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
            self.accountTextField = loginInputTableViewCell.inputTextField;
            loginInputTableViewCell.inputTextField.delegate = self;
        }
            break;
            
        case BOTLoginRowUserID: {
            NSString *identifier = [BOTLoginInputTableViewCell reuseIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
            [(BOTLoginInputTableViewCell *)cell updateTitleLabel:@"User ID"];
            [(BOTLoginInputTableViewCell *)cell updateWithLeftButtonText:@"" enabled:NO];
            [(BOTLoginInputTableViewCell *)cell updateWithRightButtonText:@"Forgot User ID"];
            BOTLoginInputTableViewCell *loginInputTableViewCell = (BOTLoginInputTableViewCell *)cell;
            self.userIDTextField = loginInputTableViewCell.inputTextField;
            loginInputTableViewCell.inputTextField.delegate = self;
        }
            break;
            
        case BOTLoginRowPassword: {
            NSString *identifier = [BOTLoginInputTableViewCell reuseIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
            [(BOTLoginInputTableViewCell *)cell updateTitleLabel:@"Password"];
            [(BOTLoginInputTableViewCell *)cell updateWithLeftButtonText:@"Password is case sensitive" enabled:NO];
            [(BOTLoginInputTableViewCell *)cell updateWithRightButtonText:@"Forgot Password"];
            BOTLoginInputTableViewCell *loginInputTableViewCell = (BOTLoginInputTableViewCell *)cell;
            self.passwordTextField = loginInputTableViewCell.inputTextField;
            loginInputTableViewCell.inputTextField.secureTextEntry = YES;
            loginInputTableViewCell.inputTextField.delegate = self;
        }
            break;
            
        case BOTLoginRowButton: {
            NSString *identifier = [BOTLoginButtonTableViewCell reuseIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
            BOTLoginButtonTableViewCell *loginButtonTableViewCell = (BOTLoginButtonTableViewCell *)cell;
            loginButtonTableViewCell.delegate = self;
        }
            break;
            
        default:
            break;
    }
    return cell;
}

#pragma mark - BOTLoginButtonTableViewCellDelegate methods

- (void)loginButtonTableViewCellDidSelectLogin:(BOTLoginButtonTableViewCell *)cell
{
    NSString *accountNumber = [self loginValueForRow:BOTLoginRowAccountNumber];
    NSString *userID = [self loginValueForRow:BOTLoginRowUserID];
    NSString *password = [self loginValueForRow:BOTLoginRowPassword];
    [self displaySpinningMask];
    [self loginWithUserID:@"mesh2" accountNumber:@"1018242nat" password:@"Mesh123!"];
//    [self loginWithUserID:userID accountNumber:accountNumber password:password];
}

- (void)displaySpinningMask
{
    self.spinnerView = [[UIView alloc] initWithFrame:self.view.frame];
    self.spinnerView.backgroundColor = [UIColor whiteColor];
    self.spinnerView.alpha = 0.9f;
    [self.view addSubview:self.spinnerView];
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = self.spinnerView.center;
    [self.spinnerView addSubview:spinner];
    [spinner startAnimating];
}

- (void)loginWithUserID:(NSString *)userID accountNumber:(NSString *)accountNumber password:(NSString *)password
{
    [self.manager loginSAtoEasySystem:accountNumber userID:userID password:password completion:^(NSDictionary *data, NSError *error) {
        [self.spinnerView removeFromSuperview];
        if(error){
            if ([self.delegate respondsToSelector:@selector(easyButtonLoginController:didFailLoginWithError:)]) {
                [self.delegate easyButtonLoginController:self didFailLoginWithError:error];
            }
        } else {
            if ([self.delegate respondsToSelector:@selector(easyButtonLoginController:didLoginWithData:)]) {
                [self.delegate easyButtonLoginController:self didLoginWithData:data];
            }
        }
    }];
}

#pragma mark - BOTLoginHeroImageTableViewCellDelegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOTCustomInputAccessoryView *accessoryView = [[BOTCustomInputAccessoryView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44.0)];
    accessoryView.delegate = self;
    textField.inputAccessoryView = accessoryView;
    
    if (textField == self.accountTextField) {
        [accessoryView setEnableUpButton:NO];
        [accessoryView setEnableDownButton:YES];
        self.currentIndex = BOTTextFieldIndexAccountNumber;
    } else if (textField == self.userIDTextField) {
        [accessoryView setEnableUpButton:YES];
        [accessoryView setEnableDownButton:YES];
        self.currentIndex = BOTTextFieldIndexUserID;
    } else if (textField == self.passwordTextField) {
        [accessoryView setEnableUpButton:YES];
        [accessoryView setEnableDownButton:NO];
        self.currentIndex = BOTTextFieldIndexPassword;
    }
    
    return YES;
}

- (void)loginHeroImageTableViewCellDidSelectClose:(BOTLoginHeroImageTableViewCell *)cell
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)loginValueForRow:(BOTLoginRow)row
{
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:0];
    BOTLoginInputTableViewCell *cell = [self.tableView cellForRowAtIndexPath:path];
    return cell.inputTextField.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.tableView setScrollEnabled:NO];
    [self.tableView setContentOffset:CGPointMake(0, BOTContentOffsetY) animated:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.tableView setScrollEnabled:YES];
}

#pragma mark - BOTCustomInputAccessoryView delegate

- (void)inputAccessoryView:(BOTCustomInputAccessoryView *)view upArrowAction:(UIButton *)sender
{
    switch (self.currentIndex) {
        case BOTTextFieldIndexUserID:
            [self.userIDTextField resignFirstResponder];
            self.currentIndex = BOTTextFieldIndexAccountNumber;
            [self.accountTextField becomeFirstResponder];
            break;
        case BOTTextFieldIndexPassword:
            [self.passwordTextField resignFirstResponder];
            self.currentIndex = BOTTextFieldIndexUserID;
            [self.userIDTextField becomeFirstResponder];
            break;
        case BOTTextFieldIndexAccountNumber:
        default:
            break;
    }
}

- (void)inputAccessoryView:(BOTCustomInputAccessoryView *)view downArrowAction:(UIButton *)sender
{
    switch (self.currentIndex) {
        case BOTTextFieldIndexAccountNumber:
            [self.accountTextField resignFirstResponder];
            self.currentIndex = BOTTextFieldIndexUserID;
            [self.userIDTextField becomeFirstResponder];
            break;
        case BOTTextFieldIndexUserID:
            [self.userIDTextField resignFirstResponder];
            self.currentIndex = BOTTextFieldIndexPassword;
            [self.passwordTextField becomeFirstResponder];
            break;
        case BOTTextFieldIndexPassword:
        default:
            break;
    }
}

- (void)inputAccessoryView:(BOTCustomInputAccessoryView *)view closeButtonAction:(UIButton *)sender
{
    switch (self.currentIndex) {
        case BOTTextFieldIndexUserID:
            [self.userIDTextField resignFirstResponder];
            break;
        case BOTTextFieldIndexPassword:
            [self.passwordTextField resignFirstResponder];
            break;
        case BOTTextFieldIndexAccountNumber:
            [self.accountTextField resignFirstResponder];
        default:
            break;
    }
}

@end
