//
//  STCConversationViewController.m
//
//  Created by Mindstix Software on 11/02/16.
//  Copyright © 2016 Mindstix Software. All rights reserved.
//

#import "STCConversationViewController.h"
#import "BOTMessageInputToolbar.h"
#import "BOTMultipleActionInputView.h"
#import "STCartButton.h"
#import "BOTConversationHelper.h"
#import "BOTLayerManager.h"
#import "BOTOnboardingManager.h"
#import "BOTChatActionCollectionView.h"
#import "BOTEasyButtonManagementViewController.h"
#import "BOTAudioRecordingsViewController.h"

// Custom Cells
#import "BOTMultipleCardBaseCollectionViewCell.h"
#import "BOTProductCollectionViewCell.h"
#import "BOTRewardCollectionViewCell.h"
#import "BOTActionCollectionViewCell.h"
#import "BOTOrderStatusCollectionViewCell.h"
#import "BOTOnboardingCollectionViewCell.h"
#import "BOTOnboardingButtonCollectionViewCell.h"
#import "BOTOutgoingMessageCollectionViewCell.h"
#import "BOTIncomingMessageCollectionViewCell.h"
#import "BOTStoreLocationCollectionViewCell.h"

#import "BOTUtilities.h"
#import "BOTMessageInputToolbar.h"
#import "BOTChat.h"

#import <QuartzCore/QuartzCore.h>

@interface ATLConversationViewController ()

- (void)updateBottomCollectionViewInset;
- (void)scrollToBottomAnimated:(BOOL)animated;

@end

@interface STCConversationViewController () <BOTMessageInputToolbarDelegate, BOTMessageInputToolbarDelegate, BOTCollectionViewActionDelegate, BOTEasyButtonManagementDelegate>

@property (nonatomic, strong) BOTMultipleActionInputView *multiInputView;
@property (nonatomic) ATLConversationDataSource *conversationDataSource;
@property (nonatomic) BOOL isInitialLoad;
@property (nonatomic) CGFloat keyboardHeight;
@property (strong, nonatomic) UIBarButtonItem *negativeSpacer;
@property (nonatomic) UIView *rootWindow;
@property (nonatomic) STCartButton *cartButton;
@property (nonatomic) BOTLayerManager *layerManager;
@property (nonatomic) BOTOnboardingManager *onboardingManager;
@property (nonatomic) BOTUserDetails *userDetails;
@property (nonatomic) BOOL shouldDisplayRecordingIcon;

@end

@implementation STCConversationViewController

@synthesize layerClient = _layerClient;

NSString *const BOTRefreshCartNotification = @"SNCartModelControllerDidLoadCartNotification";
NSString *const STCConversationViewControllerAccessibilityLabel = @"Conversation View Controller";
NSString *const STCDetailsButtonAccessibilityLabel = @"Details Button";
NSString *const STCDetailsButtonLabel = @"Details";
NSString *const STCConversationMetadataNameKey = @"conversationName";
NSString *const STCCustomAnswerMIMEType = @"application/json+customanswer";

// KC - This is a hack to allow for nil LYRClient init.
+ (instancetype)conversationViewControllerWithLayerClient:(LYRClient *)layerClient;
{
    return [[self alloc] initWithLayerClient:layerClient];
}
    
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.shouldDisplayAvatarItemForOneOtherParticipant = YES;
    self.avatarItemDisplayFrequency = ATLAvatarItemDisplayFrequencyAll;
    
    // Fire up Chat
    [self startChat];
    [self reportAnalyticsState:@"Chat Initiated" isReactive:YES];
    
    self.view.accessibilityLabel = STCConversationViewControllerAccessibilityLabel;
    [self leaveBreadcrumb:@"viewDidLoad method in STCConversationViewController is called"];
    
    // Atlas Setup
    self.dataSource = self;
    self.delegate = self;
    
    // Register Observers
    [self registerNotificationObservers];
    
    // Register Cells
    [self registerCustomCollectionViewCells];
    
    // Setup Buttons
    [self setupNavButtons];
    [self setupBackButton];
    
    self.messageInputToolbar.displaysRightAccessoryImage= NO;
    self.messageInputToolbar.textInputView.placeholder = @"Type a request";
    
    [self.view setNeedsLayout];

    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    self.navigationController.navigationBar.hidden = NO;
    
    self.isInitialLoad = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self leaveBreadcrumb:@"viewWillAppear method in STCConversationViewController is called"];
    
    // KC: Bit of a hack here. The Key Window of the application can change from time to time. So it is better
    // to capture the Window on first load and keep a consistent reference.
    if (!self.rootWindow) {
        self.rootWindow = [UIApplication sharedApplication].keyWindow;
    }
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    if (!self.conversation) {
        [self showLoadingMask];
    }
    
    self.title = @"Let's Chat";
    [self.cartButton setBadgeValue:[NSString stringWithFormat:@"%lu", (unsigned long)[self cartBadgeNumber]]];
   
    [self configureUserInterfaceAttributes];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self leaveBreadcrumb:@"viewDidAppear method in STCConversationViewController is called"];
    
    [self configureForInitialLoad];
    
    if (self.conversation) {
        [self hideSpinner];
    }
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isCartFetched"];
    
    [self configureBTSSelectionBarVisibility];
    [self showMessageInputToolbar];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self leaveBreadcrumb:@"viewWillDisappear method in STCConversationViewController is called"];
    
    if (![self isMovingFromParentViewController]) {
        [self.view resignFirstResponder];
    }
    
    // Remove Multi-selection bar if on screen
    BOTMessageInputToolbar *bar = (BOTMessageInputToolbar *)self.messageInputToolbar;
    [bar displayMultiSelectionInputBar:NO];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ATLConversationViewControllerDelegate

- (CGFloat)conversationViewController:(ATLConversationViewController *)viewController heightForMessage:(LYRMessage *)message withCellWidth:(CGFloat)cellWidth
{
    [self leaveBreadcrumb:@"heightForMessage method in STCConversationViewController is called"];
   
    LYRMessagePart *part = message.parts[0];
    
    if ([part.MIMEType isEqualToString:ATLMIMETypeTextPlain] && (message.sender == self.layerClient.authenticatedUser)) {
        return [BOTOutgoingMessageCollectionViewCell cellHeightForMessage:message inView:self.collectionView];
    } else if ([part.MIMEType isEqualToString:BOTProductListMIMEType]) {
        return [BOTMultipleCardBaseCollectionViewCell cellHeightForMessage:message];
    } else if ([part.MIMEType isEqualToString:BOTShipmentMIMEType]) {
        return [BOTMultipleCardBaseCollectionViewCell cellHeightForMessage:message];
    } else if ([part.MIMEType isEqualToString:BOTRewardMIMEType]) {
        return [BOTMultipleCardBaseCollectionViewCell cellHeightForMessage:message];
    } else if ([part.MIMEType isEqualToString:BOTActionMIMEType]) {
        return [BOTActionCollectionViewCell cellHeightForMessage:message inView:self.collectionView];
    } else if ([part.MIMEType isEqualToString:BOTOnboardingCardMIMEType]) {
        return [BOTOnboardingCollectionViewCell cellHeightForMessage:message];
    } else if ([part.MIMEType isEqualToString:BOTOnboardingButtonMIMEType]) {
        return [BOTOnboardingButtonCollectionViewCell cellHeightForMessage:message];
    } else if ([part.MIMEType isEqualToString:BOTStoreLocationMimeType]) {
        return [BOTMultipleCardBaseCollectionViewCell cellHeightForMessage:message];
    }
    return 0;
}

- (void)conversationViewController:(ATLConversationViewController *)conversationViewController configureCell:(UICollectionViewCell<ATLMessagePresenting> *)cell forMessage:(LYRMessage *)message
{
    [self leaveBreadcrumb:@"configureCell method in STCConversationViewController is called"];
    [self configureBTSSelectionBarVisibility];
    [self inspectMessageForSessionExpired:message];
}

- (void)conversationViewController:(ATLConversationViewController *)viewController didSelectMessage:(LYRMessage *)message
{
    [self leaveBreadcrumb:@"conversationViewController : didSelectMessage method in STCConversationViewController is called"];
    
    if(message.parts.count>1){
        NSString *jsonResponse = [[NSString alloc] initWithData:message.parts[1].data encoding:NSUTF8StringEncoding];
        if([jsonResponse containsString:@"cardType"] && [jsonResponse containsString:@"CHECKOUT"]){
            [self openCart];
        }
    }
}

- (NSOrderedSet <LYRMessage*> *)conversationViewController:(ATLConversationViewController *)viewController messagesForMediaAttachments:(NSArray <ATLMediaAttachment*> *)mediaAttachments
{
    if (!mediaAttachments.count) {
        return nil;
    }
    
    ATLMediaAttachment *attachment = mediaAttachments[0];
    if ([[attachment.textRepresentation lowercaseString] isEqualToString:@"onboard"]) {
        [self onboard];
        return nil;
    }
    
    [self leaveBreadcrumb:@"messagesForMediaAttachments method in STCConversationViewController is called"];
    NSMutableArray *messageParts = [NSMutableArray new];
    for (ATLMediaAttachment *attachment in mediaAttachments) {
        [messageParts addObjectsFromArray:ATLMessagePartsWithMediaAttachment(attachment)];
    }
    
    // Custom Answer support.
    if (self.customAnswerText) {
        NSData *data = [mediaAttachments[0].textRepresentation dataUsingEncoding:NSUTF8StringEncoding];
        [messageParts addObject:[LYRMessagePart messagePartWithMIMEType:STCCustomAnswerMIMEType data:data]];
        if ([self.actionDelegate respondsToSelector:@selector(conversationViewController:didSetAnswer:)]) {
            [self.actionDelegate conversationViewController:self didSetAnswer:mediaAttachments[0].textRepresentation];
        }
        self.customAnswerText = nil;
    } else {
        [messageParts addObject:[BOTConversationHelper tokenPayloadMessagePartWithMIMEType:mediaAttachments[0].mediaMIMEType]];
    }
    
    LYRMessage *message = [self.layerClient newMessageWithParts:messageParts options:nil error:nil];
    return [NSOrderedSet orderedSetWithObject:message];
}

- (void)conversationViewController:(ATLConversationViewController *)viewController didSendMessage:(LYRMessage *)message
{
    [self leaveBreadcrumb:@"conversationViewController : didSendMessage method in STCConversationViewController is called"];
}

- (void)conversationViewController:(ATLConversationViewController *)viewController didFailSendingMessage:(LYRMessage *)message error:(NSError *)error;
{
    [self leaveBreadcrumb:@"conversationViewController : didFailSendingMessage method in STCConversationViewController is called"];
    
    NSString *logMessage = [NSString stringWithFormat:@"[SCTCConversationViewController]: didFailSendingMessage - Message Send Failed with Error: %@", error ];
    [self logMessage:logMessage withType:BOTLogTypeError];
    
    [self displayMessagingAlertControllerWithError:error];
}

- (void)displayMessagingAlertControllerWithError:(NSError *)error
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Messaging Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction  actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self showMessageInputToolbar];
    }];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - ATLConversationViewControllerDataSource

- (nullable NSString *)conversationViewController:(ATLConversationViewController *)viewController reuseIdentifierForMessage:(LYRMessage *)message
{
    [self leaveBreadcrumb:@"reuseIdentifierForMessage method in STCConversationViewController is called"];
    LYRMessagePart *part = message.parts[0];
    if ([part.MIMEType isEqualToString:ATLMIMETypeTextPlain]) {
        if (message.sender == self.layerClient.authenticatedUser) {
            return [BOTOutgoingMessageCollectionViewCell reuseIdentifier];
        } else {
            return [BOTIncomingMessageCollectionViewCell reuseIdentifier];
        }
    } else if ([part.MIMEType isEqualToString:BOTProductListMIMEType]) {
        return [BOTMultipleCardBaseCollectionViewCell reuseIdentifier];
    } else if ([part.MIMEType isEqualToString:BOTShipmentMIMEType]) {
        return [BOTMultipleCardBaseCollectionViewCell reuseIdentifier];
    } else if ([part.MIMEType isEqualToString:BOTRewardMIMEType]) {
        return [BOTMultipleCardBaseCollectionViewCell reuseIdentifier];
    } else if ([part.MIMEType isEqualToString:BOTActionMIMEType]) {
        return [BOTActionCollectionViewCell reuseIdentifier];
    } else if ([part.MIMEType isEqualToString:BOTOnboardingCardMIMEType]) {
        return [BOTOnboardingCollectionViewCell reuseIdentifier];
    } else if ([part.MIMEType isEqualToString:BOTOnboardingButtonMIMEType]) {
        return [BOTOnboardingButtonCollectionViewCell reuseIdentifier];
    } else if ([part.MIMEType isEqualToString:BOTStoreLocationMimeType]) {
        return [BOTMultipleCardBaseCollectionViewCell reuseIdentifier];
    }
    return nil;
}

- (id<ATLParticipant>)conversationViewController:(ATLConversationViewController *)conversationViewController participantForIdentity:(nonnull LYRIdentity *)identity
{
    [self leaveBreadcrumb:@"conversationViewController : participantForIdentifier method in STCConversationViewController is called"];
    return (id<ATLParticipant>)identity;
}

- (NSAttributedString *)conversationViewController:(ATLConversationViewController *)conversationViewController attributedStringForDisplayOfDate:(NSDate *)date
{
    [self leaveBreadcrumb:@"conversationViewController : attributedStringForDisplayOfDate method in STCConversationViewController is called"];
    
    return [BOTConversationHelper attributedStringForDisplayOfDate:date];
}

- (NSAttributedString *)conversationViewController:(ATLConversationViewController *)conversationViewController attributedStringForDisplayOfRecipientStatus:(NSDictionary *)recipientStatus
{
    [self leaveBreadcrumb:@"conversationViewController : attributedStringForDisplayOfRecipientStatus method in STCConversationViewController is called"];
  
    return [BOTConversationHelper attributedStringForDisplayOfRecipientStatus:recipientStatus authenticatedUserID:self.layerClient.authenticatedUser.userID];
}

#pragma mark - ATLMessageIntpuToolbar Override and Delegate Calls

NSString *const BOTListActionTrackMyShipment = @"Track My Shipment";
NSString *const BOTListActionRewardsSummary = @"Rewards Summary";
NSString *const BOTListActionScanSchoolSupplies = @"Scan Supplies List";

- (ATLMessageInputToolbar *)initializeMessageInputToolbar
{
    [self leaveBreadcrumb:@"initializeMessageInputToolbar method in STCConversationViewController is called"];
    
    NSArray *selectionTitles = [self messageInputToolbarItemsForKey:@"title"];
    NSArray *selectionActions = [self messageInputToolbarItemsForKey:@"action"];
    
    [self.multiInputView setSelectionTitles:selectionTitles actions:selectionActions];
    
    // Create Toolbar
    BOTMessageInputToolbar *toolbar = [[BOTMessageInputToolbar alloc] init];
    toolbar.customDelegate = self; 
    toolbar.multiInputView = self.multiInputView;
    toolbar.textInputView.inputView = self.multiInputView;
    toolbar.chatActionCollectionView.delegate = self;
    
    return toolbar;
}

- (NSArray *)messageInputToolbarItemsForKey:(NSString *)key
{
    NSArray *guidedList = [[[BOTChat sharedInstance] serviceManager] guidedListItems];
    if (guidedList.count == 0) {
        if ([key isEqualToString:@"title"]) {
            return @[BOTListActionTrackMyShipment, BOTListActionRewardsSummary, BOTListActionScanSchoolSupplies];
        } else {
            return @[];
        }
    }
    
    NSMutableArray *items = [NSMutableArray new];
    for (NSDictionary *listItem in guidedList) {
        NSString *item = listItem[key];
        if (!item) {
            item = @"";
        }
        [items addObject:item];
    }
    return [items copy];
}
    
- (void)messageInputToolbar:(BOTMessageInputToolbar *)messageInputToolbar didTapListAccessoryButton:(UIButton *)listAccessoryButton
{
    [self leaveBreadcrumb:@"didTapListAccessoryButton method in STCConversationViewController is called"];
}

- (void)messageInputToolbar:(BOTMessageInputToolbar *)messageInputToolbar multiSelectionBarTappedWithTitle:(NSString *)title
{
    [self leaveBreadcrumb:@"multiSelectionBarTappedWithTitle method in STCConversationViewController is called"];
    if ([title isEqualToString:@"Continue shopping"]) {
        // Nothing
        BOTMessageInputToolbar *toolbar = (BOTMessageInputToolbar *)self.messageInputToolbar;
        
        // If keyboard is not showing, and this button is tapped,
        // pop the multi selection input view. If it is showing, just toggle
        if (!toolbar.textInputView.isFirstResponder) {
            [toolbar.textInputView becomeFirstResponder];
            toolbar.textInputView.inputView = self.multiInputView;
            [toolbar.textInputView reloadInputViews];
            return;
        }
        
        // Toggle
        if (toolbar.textInputView.inputView != self.multiInputView) {
            toolbar.textInputView.inputView = self.multiInputView;
            [toolbar.textInputView reloadInputViews];
        } else {
            toolbar.textInputView.inputView = nil;
            [toolbar.textInputView reloadInputViews];
        }
    } else if ([title isEqualToString:@"Customize in cart"]) {
        [self openCart];
    }
}

#pragma mark - Custom Keyboard Title Setter

- (void)setTitlesForMultiSelectionInput:(NSArray <NSString *> *)titles
{
    [self leaveBreadcrumb:@"setTitlesForMultiSelectionInput method in STCConversationViewController is called"];
   
    NSArray *selectionActions = [self messageInputToolbarItemsForKey:@"title"];
    [self.multiInputView setSelectionTitles:titles actions:selectionActions];
}

#pragma mark - BOTChatActionCollectionViewDelegate

- (void)chatActionCollectionView:(BOTChatActionCollectionView *)chatActionCollectionView didTapAction:(BOTChatAction)action withText:(NSString *)text
{
    switch (action) {
        case BOTChatActionTrackShipment:
            [self sendMessageWithText:text];
            break;
            
        case BOTChatActionRewardSummary:
            [self sendMessageWithText:text];
            break;
            
        case BOTChatActionSupplyList: {
            [super messageInputToolbar:self.messageInputToolbar didTapLeftAccessoryButton:self.messageInputToolbar.leftAccessoryButton];
            BOTMessageInputToolbar *toolbar = (BOTMessageInputToolbar *)self.messageInputToolbar;
            [toolbar.textInputView resignFirstResponder];
        }
            break;
            
        case BOTChatActionContinueShopping: {
            BOTMessageInputToolbar *bar = (BOTMessageInputToolbar *)self.messageInputToolbar;
            [bar displayMultiSelectionInputBar:NO];
        }
            break;
            
        case BOTChatActionCustomizeCart:
            [self cartIconTapped:nil];
            break;
            
        default:
            break;
    }
}

- (void)sendMessageWithText:(NSString *)text
{
    [self leaveBreadcrumb:@"sendMessageWithText method in STCConversationViewController is called"];
    
    LYRMessagePart *textPart = [LYRMessagePart messagePartWithText:text];
    LYRMessagePart *tokenPart = [BOTConversationHelper tokenPayloadForMessagePartText:text MIMEType:ATLMIMETypeTextPlain];
    
    NSError *messageCreationError;
    LYRMessage *message = [self.layerClient newMessageWithParts:@[textPart, tokenPart] options:0 error:&messageCreationError];
    if (messageCreationError) {
        NSString *logMessage = [NSString stringWithFormat:@"[SCTCConversationViewController]: showBTSInstructionMessage - Message Creation Error: %@", messageCreationError];
        [self logMessage:logMessage withType:BOTLogTypeError];
        return;
    }
    // Send down the pipe
    [self sendMessage:message];
}

#pragma mark - ATLMessageInputToolbarDelegate

- (void)messageInputToolbarDidType:(ATLMessageInputToolbar *)messageInputToolbar
{
    [self leaveBreadcrumb:@"messageInputToolbarDidType method in STCConversationViewController is called"];
    BOTMessageInputToolbar *toolbar = (BOTMessageInputToolbar *)self.messageInputToolbar;
    [toolbar resizeTextViewAndFrame];
}

#pragma mark - Inspect Message for Additional Actions To Show

- (void)configureBTSSelectionBarVisibility
{
    // First inspect Mime
    
    // KC: Bugfix to prevent display of selection bar.
    BOTMessageInputToolbar *bar = (BOTMessageInputToolbar *)self.messageInputToolbar;
    if (self.navigationController.topViewController != self) {
        [bar displayMultiSelectionInputBar:NO];
        return;
    }
    
    [self leaveBreadcrumb:@"configureBTSSelectionBarVisibility method in STCConversationViewController is called"];
    LYRMessage *lastMessage = [self lastLayerMessage];
    if (!lastMessage) {
        return;
    }
    
    LYRMessagePart *part = lastMessage.parts[0];
    if ([part.MIMEType isEqualToString:ATLMIMETypeTextPlain] && (lastMessage.sender != self.layerClient.authenticatedUser)) {
        [bar displayMultiSelectionInputBar:YES];
        
        NSArray *titles = [[BOTChat sharedInstance].serviceManager guidedListTitles];
        [bar.chatActionCollectionView updateWithSelectionTitle:titles];
    }
    
    for (LYRMessagePart *part in lastMessage.parts) {
        if ([part.MIMEType isEqualToString:BOTActionMIMEType]) {
            
            NSError *error;
            NSString *dataString = [[NSString alloc] initWithData:part.data encoding:NSUTF8StringEncoding];
            NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
            if (error) {
                NSString *logMessage = [NSString stringWithFormat:@"[SCTCConversationViewController]: configureBTSSelectionBarVisibility -  JSON Parsing Failure:: %@", error];
                [self logMessage:logMessage withType:BOTLogTypeError];
                return;
            }
            id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            if (error) {
                NSString *logMessage = [NSString stringWithFormat:@"[SCTCConversationViewController]: configureBTSSelectionBarVisibility -  JSON Parsing Failure:: %@", error];
                [self logMessage:logMessage withType:BOTLogTypeError];
                return;
            }
            
            if (![jsonResponse isKindOfClass:[NSArray class]]) {
                return;
            }
            
            [bar displayMultiSelectionInputBar:YES];
            [bar.chatActionCollectionView updateWithSelectionTitle:jsonResponse];
        }
    }
}

- (LYRMessage *)lastLayerMessage
{
    [self leaveBreadcrumb:@"lastLayerMessage method in STCConversationViewController is called"];
    
    NSUInteger messageCount = [self.queryController numberOfObjectsInSection:0];
    if (!messageCount) {
        return nil;
    }
    return [self.queryController objectAtIndexPath:[NSIndexPath indexPathForRow:(messageCount - 1) inSection:0]];
}

- (void)inspectMessageForSessionExpired:(LYRMessage *)message
{
    [self leaveBreadcrumb:@"inspectMessageForSessionExpired method in STCConversationViewController is called"];
    
    NSString *jsonResponse;
    if (message.parts.count>1)
        jsonResponse = [[NSString alloc] initWithData:message.parts[1].data encoding:NSUTF8StringEncoding];
    
    if ([jsonResponse containsString:@"session"] && [jsonResponse containsString:@"EXPIRED"]){
        [self refreshTokens];
    }
}

- (void)inspectMessageForCheckoutButton:(LYRMessage *)message
{
    [self leaveBreadcrumb:@"inspectMessageForCheckoutButton method in STCConversationViewController is called"];
   
    NSString *jsonResponse;
    if(message.parts.count>1)
        jsonResponse = [[NSString alloc] initWithData:message.parts[1].data encoding:NSUTF8StringEncoding];
    if([jsonResponse containsString:@"cardType"] && [jsonResponse containsString:@"CHECKOUT"]){
        [[ATLIncomingMessageCollectionViewCell appearance] setBubbleViewColor:[UIColor colorWithRed:204/255.0 green:0/255.0 blue:0/255.0 alpha:1]];
    }else{
        [[ATLIncomingMessageCollectionViewCell appearance] setBubbleViewColor:[UIColor colorWithRed:229/255.0f green:229/255.0f blue:234/255.0f alpha:1]];
    }
}

#pragma mark - Registration 

- (void)registerNotificationObservers
{
    [self leaveBreadcrumb:@"registerRemoteNotifications method of STCConversationViewController is called"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidTapLink:) name:ATLUserDidTapLinkNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    // Cart Notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveRefreshCartNotification:) name:BOTRefreshCartNotification object:nil];
    
    // Bot Actions
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectBOTStoreCard:) name:BOTStoreSelectedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectBOTRewardCard:) name:BOTRewardSelectedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectBOTBackToSchoolViewAll:) name:BOTBackToSchoolViewAllSelectedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectBOTBackToSchoolCard:) name:BOTBackToSchoolItemSelectedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectBOTShipmentCard:) name:BOTShipmentSelectedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectBOTActionButton:) name:BOTActionCollectionViewCellButtonTapped object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectBOTViewCartCard:) name:BOTBackToSchoolViewCartButtonTappedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectBOTAddToCartButton:) name:BOTAddToCartNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectViewAllOrdersButton:) name:BOTViewAllOrdersButtonTapNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectTrackOrderShipmentButton:) name:BOTTrackOrderShipmentButtonTapNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectAddToAppleWallet:) name:BOTRewardAddtoAppleWalletSelectionNotification object:nil];

    // KC - Bugfix for when ConversationVC would not scroll to newest messages if coming from the background.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollToBottomOfConversation) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollToBottomOfConversation) name:UIKeyboardDidShowNotification object:nil];
    
    // Onboarding Actions
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTapOnboardingAction:) name:BOTOnboardingActionNotification object:nil];
}

- (void)registerCustomCollectionViewCells
{
    [self leaveBreadcrumb:@"registerCustomCollectionViewCells method of STCConversationViewController is called"];
    
    [self registerClass:[BOTMultipleCardBaseCollectionViewCell class] forMessageCellWithReuseIdentifier:[BOTMultipleCardBaseCollectionViewCell reuseIdentifier]];
    [self registerClass:[BOTActionCollectionViewCell class] forMessageCellWithReuseIdentifier:BOTActionCollectionViewCellReuseIdentifier];
    [self registerClass:[BOTOutgoingMessageCollectionViewCell class] forMessageCellWithReuseIdentifier:[BOTOutgoingMessageCollectionViewCell reuseIdentifier]];
    [self registerClass:[BOTIncomingMessageCollectionViewCell class] forMessageCellWithReuseIdentifier:[BOTIncomingMessageCollectionViewCell reuseIdentifier]];
    
    UINib *onboardingCell = [UINib nibWithNibName:@"BOTOnboardingCollectionViewCell" bundle:StaplesUIBundle()];
    [self.collectionView registerNib:onboardingCell forCellWithReuseIdentifier:[BOTOnboardingCollectionViewCell reuseIdentifier]];
    
    UINib *onboardingButtonCell = [UINib nibWithNibName:@"BOTOnboardingButtonCollectionViewCell" bundle:StaplesUIBundle()];
    [self.collectionView registerNib:onboardingButtonCell forCellWithReuseIdentifier:[BOTOnboardingButtonCollectionViewCell reuseIdentifier]];
}

- (void)configureUserInterfaceAttributes
{
    [self leaveBreadcrumb:@"configureUserInterfaceAttributes method in STCConversationViewController is called"];
    
    [[BOTActionCollectionViewCell appearance] setMessageTextFont:[UIFont systemFontOfSize:17]];
    
    [[ATLAvatarImageView appearance] setAvatarImageViewDiameter:36.0f];

    [[ATLIncomingMessageCollectionViewCell appearance] setBubbleViewColor:[UIColor clearColor]];
    [[ATLIncomingMessageCollectionViewCell appearance] setMessageTextFont:[UIFont systemFontOfSize:16 weight:UIFontWeightLight]];
    [[ATLIncomingMessageCollectionViewCell appearance] setMessageTextColor:BOTChatTextGrayColor()];
    [[ATLIncomingMessageCollectionViewCell appearance] setMessageLinkTextColor:ATLRedColor()];
    
    [[BOTOutgoingMessageCollectionViewCell appearance] setBubbleViewColor:[UIColor clearColor]];
    [[BOTOutgoingMessageCollectionViewCell appearance] setMessageTextFont:[UIFont systemFontOfSize:16 weight:UIFontWeightLight]];
    [[BOTOutgoingMessageCollectionViewCell appearance] setMessageTextColor:BOTRedColor()];
    [[BOTOutgoingMessageCollectionViewCell appearance] setMessageLinkTextColor:[UIColor whiteColor]];
    [[BOTOutgoingMessageCollectionViewCell appearance] setBackgroundColor:[UIColor clearColor]];
}

- (void)setConversation:(LYRConversation *)conversation
{
    [self leaveBreadcrumb:@"setConversation method in STCConversationViewController is called"];
    [super setConversation:conversation];

}

- (void)configureForInitialLoad
{
    // Only pop on initial load
    if (self.isInitialLoad) {
        self.isInitialLoad = NO;
        
        // Don't pop the keyboard if we are showing an action cell as the last message.
        LYRMessage *lastMessage = [self lastLayerMessage];
        if (lastMessage) {
            LYRMessagePart *part = lastMessage.parts[0];
            if (![part.MIMEType isEqualToString:BOTActionMIMEType]) {
                [self.messageInputToolbar.textInputView becomeFirstResponder];
            }
        }
    }
}

#pragma mark - Conversation Setup

- (void)updateUserDetails:(BOTUserDetails *)userDetails
{
    self.userDetails = userDetails;
}

- (void)startChat
{
    [[BOTChat sharedInstance] connectWithUserDetails:self.userDetails completion:^(NSError *error) {
        if (!error) {
            // KC - This is somewhat of a hack until we can move this class into the chat pod.
            if (!self.layerClient) {
                _layerClient = [BOTChat sharedInstance].layerClient;
            }
            [self setupChatWithConversationID:[[BOTChat sharedInstance] conversation].identifier];
            [self hideSpinner];
        } else {
            [self hideSpinner];
            [self chatSetupFailed];
        }
    }];
}

- (void)setupChatWithConversationID:(NSURL *)conversationID
{
    LYRClient *layerClient = [BOTChat sharedInstance].layerClient;
    
    LYRQuery *query = [LYRQuery queryWithQueryableClass:[LYRConversation class]];
    query.predicate = [LYRPredicate predicateWithProperty:@"identifier" predicateOperator:LYRPredicateOperatorIsEqualTo value:conversationID];
    
    NSError *error;
    NSOrderedSet *set = [layerClient executeQuery:query error:&error];
    [self setConversation:set.firstObject];
    
    self.layerManager = [BOTLayerManager managerWithAppID:[BOTChat sharedInstance].layerClient.appID];
    self.onboardingManager = [BOTOnboardingManager managerWithLayerManager:self.layerManager conversationID:conversationID];
}

- (void)chatSetupFailed
{
    [self showAlertWithTitle:@"Chat Connection Error"
                errorMessage:@"It looks like we are having trouble connecting to our servers. Please check your connection and try again."];
}

-(void)showAlertWithTitle:(NSString *)title errorMessage:(NSString*)errorMessage
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:errorMessage
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)scrollToBottomOfConversation
{
    [super updateBottomCollectionViewInset];
    [super scrollToBottomAnimated:NO];
}


#pragma mark - Spinner Handling

- (void)showLoadingMask
{
    [self leaveBreadcrumb:@"showLoadingMask method of STCConversationViewController is called"];
    [self showSpinnerInView:self.rootWindow];
    [self showSpinnerInView:self.messageInputToolbar];
}

- (void)hideSpinner
{
    [self leaveBreadcrumb:@"hideSpinner method of STCConversationViewController is called"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissSpinner];
    });
}

- (void)dismissSpinner
{
    [self dismissSpinnerInView:self.rootWindow];
    [self dismissSpinnerInView:self.messageInputToolbar];
}

#pragma mark - Notification Handlers

- (void)userDidTapLink:(NSNotification *)notification
{
    [self leaveBreadcrumb:@"userDidTapLink method in STCConversationViewController is called"];
    
    [[UIApplication sharedApplication] openURL:notification.object];
}

- (void)didReceiveRefreshCartNotification:(NSNotification *)notification
{
    [self leaveBreadcrumb:@"cartRefresh method of STCConversationViewController is called"];
    
    typeof(self) __weak weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSNumber *success = notification.object;
        if (success.boolValue && [weakSelf.navigationItem.rightBarButtonItem isKindOfClass:[STCartButton class]]) {
            STCartButton *button = (STCartButton *)weakSelf.navigationItem.rightBarButtonItem;
            if ([button isKindOfClass:[STCartButton class]]) {
                [button setBadgeValue:[NSString stringWithFormat:@"%lu", (unsigned long)[self cartBadgeNumber]]];
                [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isCartFetched"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
        }
    });
}

# pragma mark - STCConversationViewControllerDelegate Actions 

- (void) openCart
{
    if([self.actionDelegate respondsToSelector:@selector(conversationViewControllerDidTapViewCart:)]){
        [self.actionDelegate conversationViewControllerDidTapViewCart:self];
    }
}

- (void)didSelectBOTStoreCard:(NSNotification *)notification
{
    [self leaveBreadcrumb:@"didSelectBOTStoreCard method in STCConversationViewController is called"];
   
    BOTStore *store = notification.object;
    if ([self.actionDelegate respondsToSelector:@selector(conversationViewController:didTapStore:)]) {
        [self.actionDelegate conversationViewController:self didTapStore:store];
    }
}

- (void)didSelectBOTRewardCard:(NSNotification *)notification
{
    [self leaveBreadcrumb:@"didSelectBOTRewardCard method in STCConversationViewController is called"];
    
    BOTReward *reward = notification.object;
    if([self.actionDelegate respondsToSelector:@selector(conversationViewController:didTapRewardCard:)]){
        [self.actionDelegate conversationViewController:self didTapRewardCard:reward];
    }
}

- (void)didSelectBOTBackToSchoolViewAll:(NSNotification *)notification
{
    [self leaveBreadcrumb:@"didSelectBOTBackToSchoolViewAll method in STCConversationViewController is called"];
    
    NSArray *products = notification.object;
    if([self.actionDelegate respondsToSelector:@selector(conversationViewController:didTapViewAllProducts:)]){
        [self.actionDelegate conversationViewController:self didTapViewAllProducts:products];
    }
}

- (void)didSelectBOTBackToSchoolCard:(NSNotification *)notification
{
    [self leaveBreadcrumb:@"didSelectBOTBackToSchoolCard method in STCConversationViewController is called"];
    
    BOTProduct *product = notification.object;
    if([self.actionDelegate respondsToSelector:@selector(conversationViewController:didTapProductCard:)]){
        [self.actionDelegate conversationViewController:self didTapProductCard:product];
    }
}

- (void)didSelectBOTShipmentCard:(NSNotification *)notification
{
    [self leaveBreadcrumb:@"didSelectOpenShipmentDetail method in STCConversationViewController is called"];
    
    BOTShipment *shipment = notification.object;    
    if([self.actionDelegate respondsToSelector:@selector(conversationViewController:didTapShipmentCard:)]){
        [self.actionDelegate conversationViewController:self didTapShipmentCard:shipment];
    }
}

- (void)didSelectBOTActionButton:(NSNotification *)notification
{
    [self openCart];
}

- (void)didSelectBOTViewCartCard:(NSNotification *)notification
{
    [self openCart];
}

- (void)didSelectBOTAddToCartButton:(NSNotification *)notification
{
    BOTProduct *product = notification.object;
    if ([self.actionDelegate respondsToSelector:@selector(conversationViewController:didTapAddProductToCart:)]) {
        [self.actionDelegate conversationViewController:self didTapAddProductToCart:product];
    }
}

- (void)didSelectViewAllOrdersButton:(NSNotification *)notification
{
    [self leaveBreadcrumb:@"didSelectBOTViewCartCard method in STCConversationViewController is called"];
    if([self.actionDelegate respondsToSelector:@selector(conversationViewControllerDidTapViewAllOrders:) ]){
        [self.actionDelegate conversationViewControllerDidTapViewAllOrders:self];
    }
}

- (void)didSelectTrackOrderShipmentButton:(NSNotification *)notification
{
    [self leaveBreadcrumb:@"didSelectTrackOrderShipmentButton method in STCConversationViewController is called"];
    
    // KC: Bugfix to prevent order details overlay being presented if there is another view controller on screen.
    if (self.navigationController.topViewController != self) {
        return;
    }
    
    [self.messageInputToolbar.textInputView resignFirstResponder];
    [UIView animateWithDuration:0.5 animations:^{
        self.messageInputToolbar.alpha = 0.0f;
    }];

    BOTShipment *shipment = notification.object;
    if([self.actionDelegate respondsToSelector:@selector(conversationViewController:didTapTrackOrderShipmentData:)]){
        [self.actionDelegate conversationViewController:self didTapTrackOrderShipmentData:shipment];
    }
}

- (void)didSelectAddToAppleWallet:(NSNotification *)notification
{
    BOTReward *reward = notification.object;
    if([self.actionDelegate respondsToSelector:@selector(conversationViewController:didTapAddToAppleWallet:) ]){
        [self.actionDelegate conversationViewController:self didTapAddToAppleWallet:reward];
    }
}

#pragma mark - Onboarding Actions

- (void)didTapOnboardingAction:(NSNotification *)notification
{
    NSMutableDictionary *data = [NSMutableDictionary new];
    if (notification.userInfo[BOTOnboardingButtonNameKey]) {
        data[@"button_name"] = notification.userInfo[BOTOnboardingButtonNameKey];
    }
    [self.onboardingManager executeOnboardingPhase:nil data:data completion:^(BOOL success, NSError *error) {
        //
    }];
}

- (void)didTapOnboardingActionButton:(NSNotification *)notification
{
    // Nothing to do
}

- (void)didTapOnboardingLeftButton:(NSNotification *)notification
{
    // Nothing to do
}

- (void)didTapOnboardingRightButton:(NSNotification *)notification
{
    // Nothing to do.
}

#pragma mark - ImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [super imagePickerControllerDidCancel:picker];
}

- (void) hideMessageInputToolbar
{
    [self.messageInputToolbar.textInputView resignFirstResponder];
    [UIView animateWithDuration:0.5 animations:^{
        self.messageInputToolbar.alpha = 0.0f;
    }];
}

- (void) showMessageInputToolbar
{
    [self.messageInputToolbar.textInputView resignFirstResponder];
    [UIView animateWithDuration:0.5 animations:^{
        self.messageInputToolbar.alpha = 1.0f;
        [(BOTMessageInputToolbar *)self.messageInputToolbar displayMultiSelectionInputBar:YES];
    }];
}

#pragma mark - Helpers

- (BOOL)message:(LYRMessage *)message containsMIMETypes:(NSArray *)MIMETypes
{
    for (NSString *MIMEType in MIMETypes) {
        LYRMessagePart *messagePart = ATLMessagePartForMIMEType(message, MIMEType);
        if (messagePart) {
            return YES;
        }
    }
    return NO;
}

- (void)deviceOrientationDidChange:(NSNotification *)notification
{
    [self leaveBreadcrumb:@"deviceOrientationDidChange method in STCConversationViewController is called"];
    [self.collectionView.collectionViewLayout invalidateLayout];
}

- (void)refreshTokens
{
    if ([self.actionDelegate respondsToSelector:@selector(conversationViewController:shouldRefreshTokens:)]) {
        [self.actionDelegate conversationViewController:self shouldRefreshTokens:[[BOTChat sharedInstance] tokenData]];
    }
}

- (void)setupNavButtons
{
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [spacer setWidth:7.0];
    
    NSMutableArray *barButtons = [NSMutableArray new];
    
    // Cart
    [self leaveBreadcrumb:@"setupCartButton method in STCConversationViewController is called"];
    UIImage *cartImage = [UIImage imageNamed:@"chat_bar_button_cart" inBundle:StaplesUIBundle() compatibleWithTraitCollection:nil];
    UIBarButtonItem *cartButton = [[UIBarButtonItem alloc] initWithImage:cartImage style:UIBarButtonItemStylePlain target:self action:@selector(cartIconTapped:)];
    cartButton.imageInsets = UIEdgeInsetsMake(8.0, 8.0, 8.0, 8.0);
    [barButtons addObject:cartButton];
    
    // Recordings
    if (self.shouldDisplayRecordingIcon) {
        [self leaveBreadcrumb:@"setupAudioRecording method in STCConversationViewController is called"];
        UIImage *playImage = [UIImage imageNamed:@"arrow_right" inBundle:StaplesUIBundle() compatibleWithTraitCollection:nil];
        UIBarButtonItem *playButton = [[UIBarButtonItem alloc] initWithImage:playImage style:UIBarButtonItemStylePlain target:self action:@selector(presentVoiceRecordingsViewController:)];
        [barButtons addObject:playButton];
    }
    
    // Set Bar Buttons
    self.navigationItem.rightBarButtonItems = barButtons;
}

- (void)setupBackButton
{
    [self leaveBreadcrumb:@"setupBackButton method in STCConversationViewController is called"];
    if (self.negativeSpacer == nil) {
        self.negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        [self.negativeSpacer setWidth:-7];
    }
    
    UIImage *cancelImage = [UIImage imageNamed:@"chat_bar_button_cancel" inBundle:StaplesUIBundle() compatibleWithTraitCollection:nil];
    UIBarButtonItem *cancelBarButton = [[UIBarButtonItem alloc] initWithImage:cancelImage style:UIBarButtonItemStylePlain target:self action:@selector(didSelectClose)];
    cancelBarButton.imageInsets = UIEdgeInsetsMake(8.0, 8.0, 8.0, 8.0);
    self.navigationItem.leftBarButtonItems = @[self.negativeSpacer, cancelBarButton];
}

- (void)didSelectClose
{
    [self leaveBreadcrumb:@"didSelectClose method in STCConversationViewController is called"];
    
    if (self.navigationController.viewControllers[0] != self) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [self reportAnalyticsState:@"Chat Completed" isReactive:YES];
    [self reportAnalyticsState:@"Chat Experience" isReactive:NO];
}

#pragma mark - New Functions

- (NSUInteger)cartBadgeNumber
{
    if ([self.actionDataSource respondsToSelector:@selector(cartBadgeCountForconversationViewController:)]) {
        return [self.actionDataSource cartBadgeCountForconversationViewController:self];
    }
    return 0;
}

- (void)leaveBreadcrumb:(NSString *)breadcrumb
{
    if ([self.actionDelegate respondsToSelector:@selector(conversationViewController:leaveBreadCrumb:)]) {
        [self.actionDelegate conversationViewController:self leaveBreadCrumb:breadcrumb];
    }
}

- (void)reportAnalyticsState:(NSString *)state isReactive:(BOOL)isReactive
{
    if ([self.delegate respondsToSelector:@selector(conversationViewController:trackAnalyticsState:isReactive:)]) {
        [self.actionDelegate conversationViewController:self trackAnalyticsState:state isReactive:isReactive];
    }
}

- (void)showSpinnerInView:(UIView *)view
{
    if ([self.actionDelegate respondsToSelector:@selector(conversationViewController:showSpinner:InView:)]) {
        [self.actionDelegate conversationViewController:self showSpinner:YES InView:view];
    }
}

- (void)dismissSpinnerInView:(UIView *)view
{
    if ([self.actionDelegate respondsToSelector:@selector(conversationViewController:showSpinner:InView:)]) {
        [self.actionDelegate conversationViewController:self showSpinner:NO InView:view];
    }
}

- (void)logMessage:(NSString *)message withType:(BOTLogType)logType
{
    if ([self.actionDelegate respondsToSelector:@selector(conversationViewController:logMesssage:withType:)]) {
        [self.actionDelegate conversationViewController:self logMesssage:message withType:logType];
    }
}

#pragma mark - Voice Recording 

- (void)presentVoiceRecordingsViewController:(UIButton *)button
{
    BOTAudioRecordingsViewController *audioVC = [[BOTAudioRecordingsViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:audioVC];
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

- (void)cartIconTapped:(UIButton *)button
{
    if ([self.actionDelegate respondsToSelector:@selector(conversationViewControllerDidTapViewCart:)]) {
        [self.actionDelegate conversationViewControllerDidTapViewCart:self];
    } else {
        BOTEasyServiceManager *manager = [BOTEasyServiceManager managerWithEnvironment:BOTEnvironmentDevelopment];
        BOTEasyButtonManagementViewController *controller = [BOTEasyButtonManagementViewController controllerWithEasyServiceManager:manager];
		controller.delegate = self;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - BOTEasyButtonMangementDelegate

- (void)controllerDidTapNewButton:(BOTEasyButtonManagementViewController *)controller
{
    [self onboard];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onboard
{
    [self.onboardingManager executeOnboardingPhase:@"onboarding-phase-welcome" data:@{} completion:^(BOOL success, NSError *error) {
        //
    }];
}

@end
