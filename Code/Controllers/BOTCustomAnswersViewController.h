//
//  BOTCustomAnswersViewController.h
//  Staples
//
//  Created by Reid Weber on 1/26/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @abstract This view controller handles all of the custom answers functionality for the app
 @discussion All of the views contained in ths class are custom tableViewCells. Most of these tableViewCells have delegates that send relevant information to the 'BOTCustomAnswersViewController'
 */

@interface BOTCustomAnswersViewController : UIViewController

@end
