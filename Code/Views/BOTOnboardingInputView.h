//
//  BOTOnboardingInputView.h
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BOTOnboardingInputView;

@protocol BOTOnboardingInputViewDelegate <NSObject>

- (void)onboardingInputView:(BOTOnboardingInputView *)onboardingInputView didTapConfirmationButtonWithText:(NSString *)text;

@end

@interface BOTOnboardingInputView : UIView

- (void)updatePlaceholderText:(NSString *)placeholderText;

@property (nonatomic, weak) id<BOTOnboardingInputViewDelegate>delegate;

@end
