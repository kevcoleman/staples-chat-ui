//
//  BOTOnboardingInputView.m
//  Staples
//
//  Created by Kevin Coleman on 12/28/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTOnboardingInputView.h"
#import "BOTUtilities.h"

@interface BOTOnboardingInputView ()

@property (nonatomic) UITextField *textInputField;
@property (nonatomic) UIButton *confirmationButton;

@end

@implementation BOTOnboardingInputView

- (id)init
{
    self = [super init];
    if (self) {
        [self configureUI];
    }
    return self;
}

#pragma - Public Methods

- (void)updatePlaceholderText:(NSString *)placeholderText
{
    self.textInputField.placeholder = placeholderText;
}

#pragma mark - Actions

- (void)confirmationButtonTapped:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(onboardingInputView:didTapConfirmationButtonWithText:)]) {
        [self.delegate onboardingInputView:self didTapConfirmationButtonWithText:self.textInputField.text];
    }
}

- (void)configureUI
{
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = BOTLightGrayColor().CGColor;
    
    self.textInputField = [UITextField new];
    self.textInputField.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.textInputField];
    
    self.confirmationButton = [UIButton new];
    self.confirmationButton.backgroundColor = BOTBlueColor();
    self.confirmationButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.confirmationButton setTitle:@"OK" forState:UIControlStateNormal];
    [self.confirmationButton addTarget:self action:@selector(confirmationButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.confirmationButton];
    
    [self configureConstraints];
}

- (void)configureConstraints
{
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.textInputField attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:12.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.textInputField attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.textInputField attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.confirmationButton attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.textInputField attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.confirmationButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.confirmationButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.confirmationButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.confirmationButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:60.0]];
}

@end
