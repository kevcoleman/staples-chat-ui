//
//  STCartButton.m
//  iPhoneCFA
//
//  Created by Amruta Hoke on 30/06/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import "STCartButton.h"

@implementation STCartButton

+ (STCartButton *) buttonWithTarget:(id)target selector:(SEL)selector{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = CGRectMake(0, 0, 40, 40);
    [button setImage:[UIImage imageNamed:@"ico_cart"] forState:UIControlStateNormal];
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    STCartButton *cartBarButton = [[STCartButton alloc] initWithCustomView:button];
    return cartBarButton;
}

- (UILabel *)badgeLabel {
    if (_badgeLabel == nil) {
        _badgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(19.4, 11.6, 16, 16)];
        _badgeLabel.font = [UIFont fontWithName:@"SFUIText-Bold" size:12];
        _badgeLabel.layer.cornerRadius = 8;
        _badgeLabel.layer.masksToBounds = YES;
        _badgeLabel.textAlignment = NSTextAlignmentCenter;
        _badgeLabel.adjustsFontSizeToFitWidth = YES;
        _badgeLabel.minimumScaleFactor = 0.5;
        [self.customView addSubview:_badgeLabel];
    }
    _badgeLabel.textColor = [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0];
    return _badgeLabel;
}


- (void)setBadgeValue:(NSString *)badgeValue {
    self.badgeLabel.text = badgeValue;
    if (self.badgeLabel.hidden == NO) {
        [self bounceBadge];
    }
}

- (void)bounceBadge {
    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    [animation setFromValue:[NSNumber numberWithFloat:1.5]];
    [animation setToValue:[NSNumber numberWithFloat:1]];
    [animation setDuration:0.2];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithControlPoints:.4 :1.3 :1 :1]];
    [self.badgeLabel.layer addAnimation:animation forKey:@"bounceAnimation"];
}

@end
