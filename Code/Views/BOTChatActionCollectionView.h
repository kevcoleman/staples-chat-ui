//
//  BOTChatActionCollectionView.h
//  Staples
//
//  Created by Kevin Coleman on 10/31/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LayerKit/LayerKit.h>

@class BOTChatActionCollectionView;

extern NSString * _Nonnull const BOTCollectionViewFooterItemSelectedNotification;
extern NSString * _Nonnull const BOTCollectionViewFooterReuseIdentifier;

/**
 @abstract Constants for actions supported by the BOTActionCollectionView.
 @discussion The following string are all of the possible actions provided by the action collection view. Each string has a corresponding action that is defined in the BOTChatAction enum. The enum value is then passed thru the the delegate method.
 */
extern NSString * _Nonnull const BOTActionTrackShipment;
extern NSString * _Nonnull const BOTActionRewardsSummary;
extern NSString * _Nonnull const BOTActionScanSupplyList;
extern NSString * _Nonnull const BOTActionContinueShopping;
extern NSString * _Nonnull const BOTActionCustomizeCart;

typedef NS_ENUM(NSUInteger, BOTChatAction) {
    BOTChatActionTrackShipment,
    BOTChatActionRewardSummary,
    BOTChatActionSupplyList,
    BOTChatActionContinueShopping,
    BOTChatActionCustomizeCart
};

@protocol BOTCollectionViewActionDelegate

/**
 @abstract Passes the selected action to the relevent observer.
 @param action The enum value that corresponds with the string selection made in the collection view
 */
- (void)chatActionCollectionView:(nonnull BOTChatActionCollectionView *)chatActionCollectionView didTapAction:(BOTChatAction)action withText:(NSString *)text;

@end

@interface BOTChatActionCollectionView : UIView

/**
 @abstract Delegate will handle the action selected action
 */
@property (nonatomic, nullable, weak) id<BOTCollectionViewActionDelegate> delegate;
@property (nonatomic, nonnull) LYRMessage *message;
@property (nonatomic) NSArray *selectionTitles;

/**
 @abstract Class method initializer to ensure non-nil titles
 @params titles Defines the actions provided by the chat action collection view
 */
+ (nonnull instancetype)chatActionCollectionViewWithTitles:(nonnull NSArray *)titles;

/**
 @abstract returns the constant BOTCollectionViewFooterReuseIdentifier
 */
+ (nonnull NSString *)reuseIdentifier;

/**
 @abstract Updates and reloads the collection view to reflect the new set of actions
 @params selectionTitles Provides the titles for the chat actions
 */
- (void)updateWithSelectionTitle:(nonnull NSArray *)selectionTitles;

@end
