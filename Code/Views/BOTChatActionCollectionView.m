//
//  BOTChatActionCollectionView.m
//  Staples
//
//  Created by Kevin Coleman on 10/31/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTChatActionCollectionView.h"
#import "BOTChatActionCollectionViewCell.h"
#import "BOTChatActionCollectionViewLayout.h"
#import "BOTUtilities.h"

NSString *const BOTCollectionViewFooterItemSelectedNotification = @"BOTCollectionViewFooterItemSelectedNotification";
NSString *const BOTCollectionViewFooterReuseIdentifier = @"BOTCollectionViewFooterReuseIdentifier";

NSString *const BOTActionTrackShipment    = @"Track My Shipment";
NSString *const BOTActionRewardsSummary   = @"Rewards Summary";
NSString *const BOTActionScanSupplyList   = @"Scan Supply List";
NSString *const BOTActionContinueShopping = @"Continue shopping";
NSString *const BOTActionCustomizeCart    = @"Customize in cart";

@interface BOTChatActionCollectionView () <BOTCollectionViewFooterCellDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic) UICollectionViewFlowLayout *collectionViewLayout;

@end

@implementation BOTChatActionCollectionView

#pragma mark - Initializers / Common Init

+ (instancetype)chatActionCollectionViewWithTitles:(NSArray *)titles
{
    return [[BOTChatActionCollectionView alloc] initWithFrame:CGRectZero andTitles:titles];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder andTitles:(NSArray *)titles
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit:titles];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame andTitles:(NSArray *)titles
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit:titles];
    }
    return self;
}

- (void)commonInit:(NSArray *)titles
{
    self.selectionTitles = titles;
    [self layoutCollectionView];
    [self.collectionView registerClass:[BOTChatActionCollectionViewCell class] forCellWithReuseIdentifier:BOTCollectionViewFooterCellReuseIdentifier];
}

- (void)updateWithSelectionTitle:(NSArray *)selectionTitles
{
    if ([selectionTitles isEqual:self.selectionTitles]) {
        return;
    }
    self.selectionTitles = selectionTitles;
    [self.collectionView reloadData];
}

- (void)layoutCollectionView
{
    self.collectionViewLayout = [BOTChatActionCollectionViewLayout new];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.collectionViewLayout];
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    
    [self addSubview:self.collectionView];
    [self configureCollectionViewConstraints];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = self.selectionTitles[indexPath.row];
    CGRect buttonFrame = [BOTChatActionCollectionViewCell sizeForCellWithText:title];
    return buttonFrame.size;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.selectionTitles.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selection = self.selectionTitles[indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:BOTCollectionViewFooterItemSelectedNotification object:selection];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier = BOTCollectionViewFooterCellReuseIdentifier;
    BOTChatActionCollectionViewCell *cell = (BOTChatActionCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell updateWithTitle:self.selectionTitles[indexPath.row]];
    cell.delegate = self;
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (void)collectionViewFooterCell:(BOTChatActionCollectionViewCell *)cell actionPressed:(NSString *)actionTitle
{
    if ([(NSObject *)self.delegate respondsToSelector:@selector(chatActionCollectionView:didTapAction:withText:)]) {
        
        BOTChatAction action;
        if ([actionTitle isEqualToString:BOTActionTrackShipment]) {
            action = BOTChatActionTrackShipment;
        } else if ([actionTitle isEqualToString:BOTActionRewardsSummary]) {
            action = BOTChatActionRewardSummary;
        } else if ([actionTitle isEqualToString:BOTActionScanSupplyList]) {
            action = BOTChatActionSupplyList;
        } else if ([actionTitle isEqualToString:BOTActionContinueShopping]) {
            action = BOTChatActionContinueShopping;
        } else if ([actionTitle isEqualToString:BOTActionCustomizeCart]) {
            action = BOTChatActionCustomizeCart;
        }
        [self.delegate chatActionCollectionView:self didTapAction:action withText:actionTitle];
    }
}

- (void)configureCollectionViewConstraints
{
    // Collection View Constraints
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
}

+ (NSString *)reuseIdentifier
{
    return BOTCollectionViewFooterReuseIdentifier;
}

@end
