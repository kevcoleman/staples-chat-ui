//
//  BOTCustomInputAccessoryView.h
//  Staples
//
//  Created by Reid Weber on 2/2/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BOTCustomInputAccessoryView;

/**
 @abstract 'BOTCustomInputAccessorViewDelegate' notifies the reciever when any of the control buttons are selected
 @discussion The reciever is responsible for resigning or activating first responders accordingly.
 */
@protocol BOTCustomInputAccesoryViewDelegate

@required
- (void)inputAccessoryView:(BOTCustomInputAccessoryView *)view upArrowAction:(UIButton *)sender;
- (void)inputAccessoryView:(BOTCustomInputAccessoryView *)view downArrowAction:(UIButton *)sender;
- (void)inputAccessoryView:(BOTCustomInputAccessoryView *)view closeButtonAction:(UIButton *)sender;

@end

/**
 @abstract 'BOTCustomInputAccessoryView' is an accessory control for the top a keyboard that allows navigation between UITextFields in a given ViewController
 @discussion The ViewController associated with an instance of this class will need to hold references to each of the UITextFields that are a part of the navigation.
 */
@interface BOTCustomInputAccessoryView : UIView

@property (weak, nonatomic) id<BOTCustomInputAccesoryViewDelegate> delegate;

/**
 @abstract Enable or disable the arrow buttons.
 @discussion Since the images used for each button are set to UIImageRenderingModeAlwaysTemplate the button's tint color is used to update the color of the image. Enabling the button will change its color to BOTBlueColor() and disabling it will change its color to [UIColor darkGrayColor]
 */
- (void)setEnableDownButton:(BOOL)enabled;
- (void)setEnableUpButton:(BOOL)enabled;

@end
