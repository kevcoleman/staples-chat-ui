//
//  BOTEasyButtonIconView.h
//  Staples
//
//  Created by Kevin Coleman on 12/29/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BOTEasyButtonIconView : UIView

+ (instancetype)initWithSize:(CGFloat)size;

@end
