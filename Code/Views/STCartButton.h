//
//  STCartButton.h
//  iPhoneCFA
//
//  Created by Amruta Hoke on 30/06/16.
//  Copyright © 2016 Staples. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STCartButton : UIBarButtonItem

@property (nonatomic, strong) UILabel *badgeLabel;
+ (STCartButton *) buttonWithTarget:(id)target selector:(SEL)selector;

- (void)setBadgeValue:(NSString *)badgeValue;

//- (void)setButtonImage:(NSString*)buttonImage;

@end
