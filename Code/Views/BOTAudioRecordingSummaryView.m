//
//  BOTAudioRecordingSummaryView.m
//  Staples
//
//  Created by Taylor Halliday on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTAudioRecordingSummaryView.h"

@interface BOTAudioRecordingSummaryView()

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *audioTranscriptLabel;
@property (weak, nonatomic) IBOutlet UILabel *buttonNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *audioRecordingTimeLabel;

@end

@implementation BOTAudioRecordingSummaryView


@end
