//
//  BOTCustomInputAccessoryView.m
//  Staples
//
//  Created by Reid Weber on 2/2/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTCustomInputAccessoryView.h"
#import "BOTUtilities.h"

CGFloat const standardPadding = 10.0;
CGFloat const lineViewPadding = 12.0;
CGFloat const imageSize       = 30.0;
CGFloat const arrowTopInset   = 6.0;
CGFloat const arrowSideInset  = 8.0;
CGFloat const closeEdgeInset  = 10.0;

@interface BOTCustomInputAccessoryView ()

@property (strong, nonatomic) UIButton *upArrowButton;
@property (strong, nonatomic) UIButton *downArrowButton;
@property (strong, nonatomic) UIButton *closeActionButton;

@property (strong, nonatomic) UIView *lineView;

@end

@implementation BOTCustomInputAccessoryView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [self setBackgroundColor:[UIColor whiteColor]];
    [[self layer] setBorderColor:[UIColor colorWithWhite:0.5 alpha:0.5].CGColor];
    [[self layer] setBorderWidth:0.5];
    
    self.lineView = [UIView new];
    [self.lineView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
    self.lineView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.lineView];
    
    self.upArrowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *upArrowImage = [[UIImage imageNamed:@"arrow_up_blue"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.upArrowButton setImage:upArrowImage forState:UIControlStateNormal];
    [self.upArrowButton addTarget:self action:@selector(upArrowAction:) forControlEvents:UIControlEventTouchUpInside];
    self.upArrowButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.upArrowButton setContentMode:UIViewContentModeScaleAspectFit];
    [self.upArrowButton setImageEdgeInsets:UIEdgeInsetsMake(arrowSideInset, arrowTopInset, arrowSideInset, arrowTopInset)];
    
    [self.upArrowButton setTintColor:BOTBlueColor()];
    [self addSubview:self.upArrowButton];
    
    self.downArrowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *downArrowImage = [[UIImage imageNamed:@"arrow_down_blue"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.downArrowButton setImage:downArrowImage forState:UIControlStateNormal];
    [self.downArrowButton addTarget:self action:@selector(downArrowAction:) forControlEvents:UIControlEventTouchUpInside];
    self.downArrowButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.downArrowButton setContentMode:UIViewContentModeScaleAspectFit];
    [self.downArrowButton setImageEdgeInsets:UIEdgeInsetsMake(arrowSideInset, arrowTopInset, arrowSideInset, arrowTopInset)];
    [self.downArrowButton setTintColor:BOTBlueColor()];
    [self addSubview:self.downArrowButton];
    
    self.closeActionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *closeImage = [[UIImage imageNamed:@"close_button_blue"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeActionButton setImage:closeImage forState:UIControlStateNormal];
    [self.closeActionButton addTarget:self action:@selector(closeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.closeActionButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.closeActionButton setContentMode:UIViewContentModeScaleAspectFit];
    [self.closeActionButton setImageEdgeInsets:UIEdgeInsetsMake(closeEdgeInset, closeEdgeInset, closeEdgeInset, closeEdgeInset)];
    [self.closeActionButton setTintColor:BOTBlueColor()];
    [self addSubview:self.closeActionButton];
    
    [self addButtonConstraints];
}

- (void)addButtonConstraints
{
    NSLayoutConstraint *closeHeight = [NSLayoutConstraint constraintWithItem:self.closeActionButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:imageSize];
    
    NSLayoutConstraint *closeWidth = [NSLayoutConstraint constraintWithItem:self.closeActionButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:imageSize];
    
    NSLayoutConstraint *closeTrailing = [NSLayoutConstraint constraintWithItem:self.closeActionButton attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-standardPadding];
    
    NSLayoutConstraint *closeCenterY = [NSLayoutConstraint constraintWithItem:self.closeActionButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    
    NSLayoutConstraint *downArrowHeight = [NSLayoutConstraint constraintWithItem:self.closeActionButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:imageSize];
    
    NSLayoutConstraint *downArrowWidth = [NSLayoutConstraint constraintWithItem:self.closeActionButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:imageSize];
    
    NSLayoutConstraint *downArrowTrailing = [NSLayoutConstraint constraintWithItem:self.downArrowButton attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.closeActionButton attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-standardPadding];
    
    NSLayoutConstraint *downArrowCenterY = [NSLayoutConstraint constraintWithItem:self.downArrowButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    
    NSLayoutConstraint *upArrowHeight = [NSLayoutConstraint constraintWithItem:self.closeActionButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:imageSize];
    
    NSLayoutConstraint *upArrowWidth = [NSLayoutConstraint constraintWithItem:self.closeActionButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:imageSize];
    
    NSLayoutConstraint *upArrowTrailing = [NSLayoutConstraint constraintWithItem:self.upArrowButton attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.downArrowButton attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-standardPadding];
    
    NSLayoutConstraint *upArrowCenterY = [NSLayoutConstraint constraintWithItem:self.upArrowButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    
    NSLayoutConstraint *lineViewTrailing = [NSLayoutConstraint constraintWithItem:self.lineView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.upArrowButton attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-lineViewPadding];
    
    NSLayoutConstraint *lineViewTop = [NSLayoutConstraint constraintWithItem:self.lineView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:8.0];
    
    NSLayoutConstraint *lineViewBottom = [NSLayoutConstraint constraintWithItem:self.lineView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-8.0];
    
     NSLayoutConstraint *lineViewWidth = [NSLayoutConstraint constraintWithItem:self.lineView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:1.0];
    
    [self addConstraints:@[ closeHeight, closeWidth, closeTrailing, closeCenterY, downArrowHeight, downArrowWidth, downArrowTrailing, downArrowCenterY, upArrowHeight, upArrowWidth, upArrowTrailing, upArrowCenterY, lineViewTrailing, lineViewTop, lineViewBottom, lineViewWidth ]];
}

- (void)setEnableDownButton:(BOOL)enabled
{
    [self.downArrowButton setEnabled:enabled];
    if (enabled) {
        [self.downArrowButton setTintColor:BOTBlueColor()];
    } else {
        [self.downArrowButton setTintColor:[UIColor darkGrayColor]];
    }
}

- (void)setEnableUpButton:(BOOL)enabled
{
    [self.upArrowButton setEnabled:enabled];
    if (enabled) {
        [self.upArrowButton setTintColor:BOTBlueColor()];
    } else {
        [self.upArrowButton setTintColor:[UIColor darkGrayColor]];
    }
}

- (void)upArrowAction:(UIButton *)sender
{
    if ([(NSObject *)self.delegate respondsToSelector:@selector(inputAccessoryView:upArrowAction:)]) {
        [self.delegate inputAccessoryView:self upArrowAction:sender];
    }
}

- (void)downArrowAction:(UIButton *)sender
{
    if ([(NSObject *)self.delegate respondsToSelector:@selector(inputAccessoryView:downArrowAction:)]) {
        [self.delegate inputAccessoryView:self downArrowAction:sender];
    }
}

- (void)closeButtonAction:(UIButton *)sender
{
    if ([(NSObject *)self.delegate respondsToSelector:@selector(inputAccessoryView:closeButtonAction:)]) {
        [self.delegate inputAccessoryView:self closeButtonAction:sender];
    }
}

@end
