//
//  BOTOutgoingMessageBubbleView.h
//  Staples
//
//  Created by Reid Weber on 1/24/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <Atlas/Atlas.h>

extern NSUInteger const BOTBubbleSpikeHeight;

/**
 @abstract Overrides ATLMessageBubbleView for custom drawing.
 @discussion The only purpose of this subclass is to override drawRect for the custom drawing of the chat bubble. It is important to note that the ATLMessBubbleView corner radius must always remain at 0 or it can interfere with the custom drawing this class performs.
 */

@interface BOTOutgoingMessageBubbleView : ATLMessageBubbleView

@end
