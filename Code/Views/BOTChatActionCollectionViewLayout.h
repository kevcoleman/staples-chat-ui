//
//  BOTCollectionViewFooterLayout.h
//  Staples
//
//  Created by Kevin Coleman on 10/31/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BOTChatActionCollectionViewLayout : UICollectionViewFlowLayout

@end
