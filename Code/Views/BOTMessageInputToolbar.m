//
//  STMessageInputToolbar.m
//  Staples
//
//  Created by Kevin Coleman on 8/19/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTMessageInputToolbar.h"
#import "BOTChatActionCollectionView.h"
#import "BOTMultipleActionInputView.h"
#import "EDColor.h"
#import "BOTUtilities.h"
#import "BOTChat.h"
#import <QuartzCore/QuartzCore.h>

extern NSString *const ATLMessageInputToolbarCameraButton;

// KVO Text Input View Frame Context
static void *TextInputViewFrameKVOCtx = &TextInputViewFrameKVOCtx;

// Kevin Coleman: Redeclaring these constants here, as they are private to the Atlas superclass.
// Note, these values are subject to change in the super.
static CGFloat const ATLLeftButtonHorizontalMargin = 11.0f;
static CGFloat const ATLRightAccessoryButtonDefaultWidth = 46.0f;
static CGFloat const ATLButtonHeight = 28.0f;
static CGFloat const STMultiActionToolbarDefaultHeight = 65.0f;

NSString *const RightMultiActionInputViewButtonTapped = @"RightMultiActionInputViewButtonTapped";
NSString *const BOTMessageInputToolbarSendButton  = @"Message Input Toolbar Send Button";

@interface ATLMessageInputToolbar ()

@property (nonatomic) CGFloat buttonCenterY;
@property (nonatomic) CGFloat textViewMaxHeight;
@property (nonatomic) UITextView *dummyTextView;
@property (nonatomic) BOOL firstAppearance;

- (void)configureRightAccessoryButtonState;

@end

@interface BOTMessageInputToolbar ()

@property (nonatomic) UIImage *listAccessoryButtonImage;

@property (nonatomic, strong) UIView *shadowContainerView;
@property (nonatomic, strong) UIBezierPath *shadowPath;

@property (nonatomic, strong) NSLayoutConstraint *inputTextViewHeightConstraint;
@property (nonatomic, strong) NSLayoutConstraint *multiSelectionHeightConstraint;
@property (nonatomic, strong) NSLayoutConstraint *collectionViewHeightConstraint;

@end

@implementation BOTMessageInputToolbar

- (id)init
{
    self = [super init];
    if (self) {
        
        // Setup trigger for post initial pass
        self.firstAppearance = YES;
        
        // Register for text change note
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeTextViewAndFrame) name:UITextViewTextDidChangeNotification object:self.textInputView];
        
        // Adding target for right accessory btn
        [self.rightAccessoryButton addTarget:self action:@selector(rightAccessoryButtonTappedEvent) forControlEvents:UIControlEventAllTouchEvents];
        
        // Collection View
        [self setupChatActionCollectionView];
        [self setupCollectionViewInputContraints];
        [self setupBorderView];
        
        // Init some layout
        [self resizeTextViewAndFrame];
        
        // Custom images
        [self setCustomAccessoryButtonImages];
        
        // Turn off the auto suggestion bar
        self.textInputView.autocorrectionType = UITextAutocorrectionTypeYes;
        self.textInputView.spellCheckingType = UITextSpellCheckingTypeYes;
        [self.textInputView setFont:BOTLightFont(16)];
        [self.textInputView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:TextInputViewFrameKVOCtx];
        
        // KC BugFix for iOS 10.
        if ([[[UIDevice currentDevice] systemVersion] intValue] >= 10.0) {
            self.backgroundColor = [UIColor whiteColor];
        }
        [[self.textInputView layer] setBorderColor:[UIColor whiteColor].CGColor];
        [[self.textInputView layer] setBorderWidth:0.0];
    }
    return self;
}

- (void)setupChatActionCollectionView
{
    NSArray *titles = [[BOTChat sharedInstance].serviceManager guidedListTitles];
    self.chatActionCollectionView = [BOTChatActionCollectionView chatActionCollectionViewWithTitles:titles];
    self.chatActionCollectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.chatActionCollectionView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.chatActionCollectionView];
    [self sendSubviewToBack:self.chatActionCollectionView];
}

- (void)setupBorderView
{
    UIView *borderView = [UIView new];
    borderView.translatesAutoresizingMaskIntoConstraints = NO;
    borderView.layer.borderWidth = 0.5f;
    borderView.layer.borderColor = BOTToolbarBorderGray().CGColor;
    borderView.backgroundColor = [UIColor whiteColor];
    borderView.layer.shadowOffset = CGSizeMake(0, 0);
    borderView.layer.shadowRadius = 3;
    borderView.layer.shadowOpacity = 0.1;
    [self addSubview:borderView];
    [self sendSubviewToBack:borderView];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:borderView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:borderView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:borderView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.textInputView attribute:NSLayoutAttributeTop multiplier:1.0 constant:-8]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:borderView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
}

- (void)displayMultiSelectionInputBar:(BOOL)displayBar;
{
    self.isShowingMultiSelectionBar = displayBar;
}

- (void)setIsShowingMultiSelectionBar:(BOOL)isShowingMultiSelectionBar
{
    if (_isShowingMultiSelectionBar != isShowingMultiSelectionBar) {
        _isShowingMultiSelectionBar = isShowingMultiSelectionBar;
        CGFloat toHeight = STMultiActionToolbarDefaultHeight;
        CGFloat toAlpha = 1.0f;
        CGFloat speed = 0.3f;
        if (!isShowingMultiSelectionBar) {
            toHeight = 0.0f;
            toAlpha = 0.0f;
            speed = 0.1f;
        }
        
        // Animate to the position
        [self layoutIfNeeded];
        [UIView animateWithDuration:speed
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.collectionViewHeightConstraint.constant = toHeight;
                             self.chatActionCollectionView.alpha = toAlpha;
                             
                             [self layoutIfNeeded];
                         }
                         completion:^(BOOL finished) {
                             [[NSNotificationCenter defaultCenter] postNotificationName:ATLMessageInputToolbarDidChangeHeightNotification object:self];
                         }];
    }
}

#pragma mark - Target / Action Reveivers

- (void)rightAccessoryButtonTappedEvent
{
    [self resizeTextViewAndFrame];
}

- (void)listButtonTapped:(UIButton *)sender
{
    if (self.textInputView.inputView && self.textInputView.isFirstResponder) {
        [sender setSelected:NO];
    } else {
        [sender setSelected:YES];
    }
    
    // If keyboard is not showing, and this button is tapped,
    // pop the multi selection input view. If it is showing, just toggle
    if (!self.textInputView.isFirstResponder) {
        [self.textInputView becomeFirstResponder];
        self.textInputView.inputView = self.multiInputView;
        [self.textInputView reloadInputViews];
        return;
    }
    
    // Toggle
    if (self.textInputView.inputView != self.multiInputView) {
        self.textInputView.inputView = self.multiInputView;
        [self.textInputView reloadInputViews];
    } else {
        self.textInputView.inputView = nil;
        [self.textInputView reloadInputViews];
    }
    
    if ([self.customDelegate respondsToSelector:@selector(messageInputToolbar:didTapListAccessoryButton:)]) {
        [self.customDelegate messageInputToolbar:self didTapListAccessoryButton:sender];
    }
}

#pragma mark - Accessory Button Images

- (void)setCustomAccessoryButtonImages
{
    UIImage *plusGlyph = [UIImage imageNamed:@"toolbar_plus_sign" inBundle:StaplesUIBundle() compatibleWithTraitCollection:nil];
    [self.leftAccessoryButton setImage:plusGlyph forState:UIControlStateNormal];
    [self.leftAccessoryButton setContentEdgeInsets:UIEdgeInsetsMake(4.0, 4.0, 4.0, 4.0)];
}

- (void)configureRightAccessoryButtonState
{
    UIImage *arrowImage = [UIImage imageNamed:@"toolbar_arrow" inBundle:StaplesUIBundle() compatibleWithTraitCollection:nil];
    
    self.rightAccessoryButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.rightAccessoryButton.accessibilityLabel = BOTMessageInputToolbarSendButton;
    [self.rightAccessoryButton setImage:arrowImage forState:UIControlStateNormal];
    self.rightAccessoryButton.contentEdgeInsets = UIEdgeInsetsMake(6.0, 6.0, 6.0, 6.0);
    self.rightAccessoryButton.titleLabel.font = self.rightAccessoryButtonFont;
    [self.rightAccessoryButton setTitle:nil forState:UIControlStateNormal];
    [self.rightAccessoryButton setTitleColor:self.rightAccessoryButtonActiveColor forState:UIControlStateNormal];
    [self.rightAccessoryButton setTitleColor:self.rightAccessoryButtonDisabledColor forState:UIControlStateDisabled];
    if (!self.displaysRightAccessoryImage && !self.textInputView.text.length) {
        self.rightAccessoryButton.enabled = NO;
    } else {
        self.rightAccessoryButton.enabled = YES;
    }
}

#pragma mark - Subview Layouts

- (void)layoutSubviews
{
    [self setBarTintColor:[UIColor whiteColor]];
    // Turn off the auto resizing mask for all manually laid out buttons
    for (UIButton *btn in @[self.rightAccessoryButton, self.leftAccessoryButton]) {
        for (NSLayoutConstraint *constraint in btn.constraints) {
            [btn removeConstraint:constraint];
        }
    }
    
    // Disable if no text
    self.rightAccessoryButton.enabled = self.textInputView.text.length;
    
    // Remove the layout constraint for height attempting to lock this at 44.0
    for (NSLayoutConstraint *constraint in self.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight && constraint.constant == 44.0 && constraint.firstItem == self.chatActionCollectionView) {
            [self removeConstraint:constraint];
        }
    }
    
    // First apperance layout. Look for absent text on send button
    if (!self.rightAccessoryButton.titleLabel.text) {
        [self configureRightAccessoryButtonState];
    }
    
    CGRect leftButtonToRect;
    CGRect rightButtonToRect;
    
    // Configure leading edge buttons
    // Note: Horiz Order -> [LeftButton] [TextView] [RightButton]
    
    // Const Bottom Padding
    const CGFloat vertPadding = 10.0f;
    
    // Left Button
    leftButtonToRect.size.height = ATLButtonHeight;
    leftButtonToRect.size.width  = ATLButtonHeight;
    leftButtonToRect.origin.x    = ATLLeftButtonHorizontalMargin;
    leftButtonToRect.origin.y    = (self.bounds.origin.y + self.bounds.size.height) - ATLButtonHeight - vertPadding;
    
    // Right Button
    rightButtonToRect.size.height = ATLButtonHeight;
    rightButtonToRect.size.width  = ATLRightAccessoryButtonDefaultWidth;
    rightButtonToRect.origin.x    = CGRectGetMaxX(self.bounds) - ATLRightAccessoryButtonDefaultWidth;
    rightButtonToRect.origin.y    = (self.bounds.origin.y + self.bounds.size.height) - ATLButtonHeight - vertPadding;
    
    // Set All Frames
    self.leftAccessoryButton.frame  = leftButtonToRect;
    self.rightAccessoryButton.frame = rightButtonToRect;
    
    // Getting the text input view frame here so we can bypass the call to super
    CGRect textViewRect = self.textInputView.frame;
    
    // Calc TextView Horiz Area
    CGFloat textViewToX      = CGRectGetMaxX(self.leftAccessoryButton.frame);
    CGFloat textViewToWidth  = CGRectGetMinX(self.rightAccessoryButton.frame) - textViewToX;
    textViewRect.origin.x    = textViewToX;
    textViewRect.size.width  = MAX(textViewToWidth, 0.0f);
    self.textInputView.frame = textViewRect;
    
    // Add Text View Constraints after we've positioned our buttons
    if (self.firstAppearance) {
        self.firstAppearance = NO;
        [self setupTextInputViewConstraints];
        [self resizeTextViewAndFrame];
    }
}

- (void)resizeTextViewAndFrame
{
    // Getting the text input view frame here so we can bypass the call to super
    CGRect textViewRect = self.textInputView.frame;
    CGRect prevTextViewRect = textViewRect;
    
    // Calc TextView Vert Area
    [self.textInputView sizeToFit];
    [self.textInputView layoutIfNeeded];
    CGSize toSize            = [self.textInputView sizeThatFits:CGSizeMake(textViewRect.size.width, MAXFLOAT)];
    CGFloat toHeight         = fmax(toSize.height, 30.0f);
    textViewRect.size.height = toHeight;
    
    // Set Text View Frame
    [self layoutIfNeeded];
    [UIView performWithoutAnimation:^{
        self.inputTextViewHeightConstraint.constant = textViewRect.size.height;
    }];

    // Increase frame if need be
    CGRect toFrame      = self.frame;
    toFrame.size.height = toHeight + (2.0f * 8.0f);
    
    // Notify of height change
    if (prevTextViewRect.size.height != toFrame.size.height) {
        [[NSNotificationCenter defaultCenter] postNotificationName:ATLMessageInputToolbarDidChangeHeightNotification object:self];
    }
}

#pragma mark - Touch Overrides For UI Interaction w/ View Outside of frame

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    // Look for taps in the text view
    if (CGRectContainsPoint(self.textInputView.frame, point) && self.textInputView.inputView) {
        self.textInputView.inputView = nil;
        [self.textInputView reloadInputViews];
    }
    
    // Return YES if the touch is within bounds or multiaction view
    if (CGRectContainsPoint(self.bounds, point) || CGRectContainsPoint(self.chatActionCollectionView.frame, point)) {
        return YES;
    }
    return NO;
}

#pragma mark - Chat Action Delegate

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    CGPoint pointForTargetView = [self.chatActionCollectionView convertPoint:point fromView:self];
    if (CGRectContainsPoint(self.chatActionCollectionView.bounds, pointForTargetView)) {
        return [self.chatActionCollectionView hitTest:pointForTargetView withEvent:event];
    }
    return [super hitTest:point withEvent:event];
}

#pragma mark - Layout Constraints

- (void)setupCollectionViewInputContraints
{
    [self.chatActionCollectionView setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.chatActionCollectionView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.chatActionCollectionView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.chatActionCollectionView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    self.collectionViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.chatActionCollectionView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0f];
    
    [self addConstraints:@[leading, trailing, top, self.collectionViewHeightConstraint]];
}

- (void)setupTextInputViewConstraints
{
    [self.textInputView setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.textInputView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.leftAccessoryButton attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:8.0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.textInputView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.rightAccessoryButton attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-8.0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.textInputView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-8.0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.textInputView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.chatActionCollectionView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:8.0];
    self.inputTextViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.textInputView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40.0];
    
    // In inital layout, leading edge will get compressed
    // enough to eliminate width. Setting a lower priority to
    // address the issue.
    leading.priority = UILayoutPriorityDefaultLow;
    [self addConstraints:@[leading, trailing, top, bottom, self.inputTextViewHeightConstraint]];
}

#pragma mark - ATLMessageInputToolbarDelegate

- (void)messageInputToolbarDidType:(ATLMessageInputToolbar *)messageInputToolbar
{
    [self resizeTextViewAndFrame];
}

#pragma mark - Dealloc

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.textInputView removeObserver:self forKeyPath:@"frame"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if (context == TextInputViewFrameKVOCtx) {
        [[NSNotificationCenter defaultCenter] postNotificationName:UITextViewTextDidBeginEditingNotification object:self.textInputView];
    }
}

@end
