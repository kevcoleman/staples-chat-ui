//
//  BOTOutgoingMessageBubbleView.m
//  Staples
//
//  Created by Reid Weber on 1/24/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTOutgoingMessageBubbleView.h"
#import "BOTUtilities.h"

NSUInteger const BOTBubbleStartingWidth = 8.0;
NSUInteger const BOTBubbleSpikeHeight   = 8.0;
NSUInteger const BOTBubbleCornerRadius  = 8.0;


@interface BOTOutgoingMessageBubbleView ()

@property (strong, nonatomic) UIImageView *bubbleImageView;

@end

@implementation BOTOutgoingMessageBubbleView

@synthesize bubbleImageView = _bubbleImageView;

- (void)drawRect:(CGRect)rect
{
    CGRect bubbleRect = CGRectMake(rect.origin.x + 1, rect.origin.y + 1, rect.size.width - 2, rect.size.height - (BOTBubbleSpikeHeight * 1.5));
    
    CGPoint corner1 = CGPointMake(bubbleRect.origin.x, bubbleRect.origin.y);
    CGPoint corner2 = CGPointMake(bubbleRect.size.width, bubbleRect.origin.y);
    CGPoint corner4 = CGPointMake(bubbleRect.origin.x, bubbleRect.size.height);
    
    CGPoint point1 = CGPointMake(bubbleRect.origin.x + BOTBubbleStartingWidth, bubbleRect.origin.y);
    CGPoint point2 = CGPointMake(bubbleRect.size.width - BOTBubbleStartingWidth, bubbleRect.origin.y);
    CGPoint point3 = CGPointMake(bubbleRect.size.width, bubbleRect.origin.y + BOTBubbleStartingWidth);
    CGPoint point4 = CGPointMake(bubbleRect.size.width, rect.size.height - 2);
    CGPoint point5 = CGPointMake(bubbleRect.size.width - (BOTBubbleSpikeHeight + 2), bubbleRect.size.height);
    CGPoint point6 = CGPointMake(bubbleRect.origin.x + BOTBubbleStartingWidth, bubbleRect.size.height);
    CGPoint point7 = CGPointMake(bubbleRect.origin.x, bubbleRect.size.height - BOTBubbleStartingWidth);
    CGPoint point8 = CGPointMake(bubbleRect.origin.x, bubbleRect.origin.y + BOTBubbleStartingWidth);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // The CGColor componants were calculated from the rgb values for BOTRedColor()
    CGContextSetRGBStrokeColor(context, 0.8, 0.0, 0.0, 1.0);
    CGContextSetLineWidth(context, 1.0);
    
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, point1.x, point1.y);
    CGContextAddLineToPoint(context, point2.x, point2.y);
    CGContextAddArcToPoint(context, corner2.x, corner2.y, point3.x, point3.y, BOTBubbleCornerRadius);
    
    CGContextAddLineToPoint(context, point4.x, point4.y);
    CGContextAddLineToPoint(context, point5.x, point5.y);
    CGContextAddLineToPoint(context, point6.x, point6.y);
    CGContextAddArcToPoint(context, corner4.x, corner4.y, point7.x, point7.y, BOTBubbleCornerRadius);
    
    CGContextAddLineToPoint(context, point7.x, point7.y);
    CGContextAddLineToPoint(context, point8.x, point8.y);
    CGContextAddArcToPoint(context, corner1.x, corner1.y, point1.x, point1.y, BOTBubbleCornerRadius);
    
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathStroke);
}

@end
