//
//  BOTMyButtonsHeaderView.h
//  Staples
//
//  Created by Kevin Coleman on 12/30/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BOTMyButtonsHeaderView : UIView

- (void)updateText:(NSString *)text;

@end
