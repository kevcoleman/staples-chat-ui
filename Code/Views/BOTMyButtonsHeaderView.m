//
//  BOTMyButtonsHeaderView.m
//  Staples
//
//  Created by Kevin Coleman on 12/30/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTMyButtonsHeaderView.h"
#import "BOTUtilities.h"

@interface BOTMyButtonsHeaderView ()

@property (nonatomic) UILabel *myButtonLabel;

@end

@implementation BOTMyButtonsHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configureUI];
    }
    return self;
}

- (void)configureUI
{
    self.backgroundColor = [UIColor whiteColor];
    
    self.myButtonLabel = [UILabel new];
    self.myButtonLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.myButtonLabel.text = @"My Easy Buttons";
    self.myButtonLabel.font = BOTLightFont(24);
    self.myButtonLabel.textColor = [UIColor darkGrayColor];
    [self.myButtonLabel sizeToFit];
    [self addSubview:self.myButtonLabel];
    
    [self configureConstraints];
}

- (void)configureConstraints
{
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.myButtonLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:20]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.myButtonLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.myButtonLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.myButtonLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
}

- (void)updateText:(NSString *)text
{
    self.myButtonLabel.text = text;
}

@end
