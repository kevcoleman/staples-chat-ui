//
//  BOTEasyButtonIconView.m
//  Staples
//
//  Created by Kevin Coleman on 12/29/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "BOTEasyButtonIconView.h"
#import "BOTUtilities.h"

@interface BOTEasyButtonIconView ()

@property (nonatomic) UIView *borderView;
@property (nonatomic) UIView *redCircleView;
@property (nonatomic) UILabel *easyLetter;
@property (nonatomic) CGFloat size;

@end

@implementation BOTEasyButtonIconView

+ (instancetype)initWithSize:(CGFloat)size
{
    return [[super alloc] initWithSize:size];
}

- (id)initWithSize:(CGFloat)size
{
    self = [super initWithFrame:CGRectMake(0, 0, size, size)];
    if (self) {
        _size = size;
        [self configureUIWithSize:size];
    }
    return self;
}

- (CGSize)intrinsicContentSize
{
    return CGSizeMake(self.size, self.size);
}

- (void)configureUIWithSize:(CGFloat)size
{
    self.borderView = [UIView new];
    self.borderView.translatesAutoresizingMaskIntoConstraints = NO;
    self.borderView.backgroundColor = [UIColor clearColor];
    self.borderView.layer.borderWidth = 1.0f;
    self.borderView.layer.borderColor = BOTLightGrayColor().CGColor;
    self.borderView.layer.cornerRadius = size / 2;
    [self addSubview:self.borderView];
    
    CGFloat circleSize = size * .75;
    self.redCircleView = [UIView new];
    self.redCircleView.translatesAutoresizingMaskIntoConstraints = NO;
    self.redCircleView.backgroundColor = BOTRedColor();
    self.redCircleView.layer.cornerRadius = circleSize / 2;
    [self addSubview:self.redCircleView];
    
    self.easyLetter = [UILabel new];
    self.easyLetter.layoutMargins = UIEdgeInsetsZero;
    self.easyLetter.translatesAutoresizingMaskIntoConstraints = NO;
    self.easyLetter.text = @"e";
    [self.easyLetter setFont:BOTRegularFont(16.0f)];
    [self.easyLetter setTextColor:[UIColor whiteColor]];
    [self.easyLetter sizeToFit];
    [self addSubview:self.easyLetter];
    
    [self configureConstraints];
}

- (void)configureConstraints
{
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.borderView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0f constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.borderView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0f constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.borderView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0f constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.borderView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.redCircleView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.borderView attribute:NSLayoutAttributeWidth multiplier:0.75f constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.redCircleView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.borderView attribute:NSLayoutAttributeHeight multiplier:0.75f constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.redCircleView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.redCircleView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.easyLetter attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.easyLetter attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0]];
}

@end
