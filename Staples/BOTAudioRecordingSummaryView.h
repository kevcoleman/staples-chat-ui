//
//  BOTAudioRecordingSummaryView.h
//  Staples
//
//  Created by Taylor Halliday on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOTProduct.h"
#import "BOTAudioRecording.h"

@protocol BOTAudioRecordingSummaryViewDelegate <NSObject>

- (void)playAudioWasTapped:(BOTAudioRecording *)audioRecording;
- (void)productWasTapped:(BOTProduct *)product;

@end

@interface BOTAudioRecordingSummaryView : UIView

@property (nonatomic, strong) BOTAudioRecording *audioRecording;
@property (nonatomic, weak) id <BOTAudioRecordingSummaryViewDelegate> delegate;

@end
