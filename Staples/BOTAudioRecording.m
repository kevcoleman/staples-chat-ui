//
//  BOTAudioRecording.m
//  Staples
//
//  Created by Taylor Halliday on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTAudioRecording.h"

NSString *const BOTAudioRecordingButtonNameKey = @"buttonName";
NSString *const BOTAudioRecordingCreatedAtKey = @"audioCreatedAt";
NSString *const BOTAudioRecordingProductKey = @"product";

@interface BOTAudioRecording ()

@property (nonatomic, strong) BOTProduct *product;
@property (nonatomic, strong) NSString *buttonName;
@property (nonatomic, strong) NSDate *audioCreatedAt;

@end

@implementation BOTAudioRecording

+ (instancetype)audioRecordingWithData:(NSDictionary *)data
{
    return [[self alloc] initWithData:data];
}

- (id)initWithData:(NSDictionary *)data
{
    self = [super init];
    if (self) {
        _buttonName = data[BOTAudioRecordingButtonNameKey];
        _product = [BOTProduct productWithData:data[BOTAudioRecordingProductKey]];
        _audioCreatedAt = [NSDate dateWithTimeIntervalSince1970:[data[BOTAudioRecordingCreatedAtKey] integerValue]];
    }
    return self;
}

@end
