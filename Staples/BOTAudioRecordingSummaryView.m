//
//  BOTAudioRecordingSummaryView.m
//  Staples
//
//  Created by Taylor Halliday on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import "BOTAudioRecordingSummaryView.h"

@interface BOTAudioRecordingSummaryView()

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *audioTranscriptLabel;
@property (weak, nonatomic) IBOutlet UILabel *buttonNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *audioRecordingTimeLabel;

@end

@implementation BOTAudioRecordingSummaryView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"BOTAudioRecordingSummaryView" owner:self options:nil];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGRect topLineRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, 0.5);
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:topLineRect];
    [[UIColor lightGrayColor] setFill];
    [path fill];
}

@end
