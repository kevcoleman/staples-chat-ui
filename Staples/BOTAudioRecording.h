//
//  BOTAudioRecording.h
//  Staples
//
//  Created by Taylor Halliday on 1/30/17.
//  Copyright © 2017 Mesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BOTProduct.h"

@interface BOTAudioRecording : NSObject

@property (readonly, nonatomic, strong) BOTProduct *product;
@property (readonly, nonatomic, strong) NSString *buttonName;
@property (readonly, nonatomic, strong) NSDate *audioCreatedAt;

/**
 Returns an new `BOTAudioRecording` object hydratd with the supplied data.
 
 @param data An `NSDictionary` containing the shipment data.
 */
+ (instancetype)audioRecordingWithData:(NSDictionary *)data;

@end
