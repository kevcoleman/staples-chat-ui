//
//  AppDelegate.m
//  Staples
//
//  Created by Kevin Coleman on 8/18/16.
//  Copyright © 2016 Mesh. All rights reserved.
//

#import "AppDelegate.h"
#import <LayerKit/LayerKit.h>
#import "STLayerService.h"
#import "BOTChat.h"
#import "STCConversationViewController.h"
#import "BOTEasyButtonLoginController.h"
#import "BOTLayerManager.h"
#import "BOTOnboardingManager.h"
#import "BOTEasyButtonManagementViewController.H"
#import "BOTUserDetails.h"

@import SystemConfiguration.CaptiveNetwork;

BOOL BOTIsRunningTests()
{
    return (NSClassFromString(@"XCTestCase") || [[[NSProcessInfo processInfo] environment] valueForKey:@"XCInjectBundle"]);
}

@interface AppDelegate () <STCConversationViewControllerDelegate, BOTEasyButtonLoginControllerDelegate>

@property (nonatomic) STLayerService *layerService;
@property (nonatomic) NSDictionary *data;
@property (nonatomic) BOTUserDetails *userDetails;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[BOTChat sharedInstance] updateEnvironment:BOTEnvironmentDevelopment completion:^(NSError *error) {
        if (error) {
            NSLog(@"Failed to configure environment");
        }
        [self presentEasyButtonLoginViewController];
    }];
    
    return YES;
}

- (void)startChatWithData:(NSDictionary *)data
{
    self.data = data;
    self.userDetails = [BOTUserDetails new];
    self.userDetails.userID = data[@"emailId"];
    self.userDetails.wcToken = data[@"wcToken"];
    self.userDetails.wcTrustedToken = data[@"wcTrustedToken"];
    
    [[BOTChat sharedInstance] connectWithUserDetails:self.userDetails completion:^(NSError *error) {
        [self presentConversationViewController];
    }];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSError *error;
    BOOL success = [[BOTChat sharedInstance].layerClient updateRemoteNotificationDeviceToken:deviceToken error:&error];
    if (success) {
        NSLog(@"Application did register for remote notifications");
    } else {
        NSLog(@"Error updating Layer device token for push:%@", error);
    }
}


- (void)layerClient:(LYRClient *)client didAuthenticateAsUserID:(NSString *)userID
{
    [self presentConversationViewController];
}

- (void)easyButtonLoginController:(BOTEasyButtonLoginController *)easyServerManager didLoginWithData:(NSDictionary *)data
{
    [self startChatWithData:data];
}

- (void)easyButtonLoginController:(BOTEasyButtonLoginController *)easyServerManager didFailLoginWithError:(NSError *)error
{
    
}

- (void)presentConversationViewController
{
    LYRClient *client = [BOTChat sharedInstance].layerClient;
    STCConversationViewController *conversationViewController = [STCConversationViewController conversationViewControllerWithLayerClient:client];
    [conversationViewController updateUserDetails:self.userDetails];
    conversationViewController.displaysAddressBar = NO;
    conversationViewController.hidesBottomBarWhenPushed = YES;
    conversationViewController.actionDelegate = self;
    
    [(UINavigationController *)self.window.rootViewController pushViewController:conversationViewController animated:YES];
}

- (void)presentEasyButtonLoginViewController
{
    BOTEasyServiceManager *manager = [[BOTChat sharedInstance] serviceManager];
    BOTEasyButtonLoginController *controller = [BOTEasyButtonLoginController controllerWithEasyServiceManager:manager];
    controller.delegate = self;
    
    UINavigationController *root = [[UINavigationController alloc] initWithRootViewController:controller];
    
    self.window = [UIWindow new];
    self.window.frame = [[UIScreen mainScreen] bounds];
    self.window.rootViewController = root;
    [self.window makeKeyAndVisible];
}

@end
