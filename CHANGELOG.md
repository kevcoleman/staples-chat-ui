# Staples Chat UI

## 0.5.0

* Big overhaul of connection and authentication logic.
  [Kevin Coleman](https://github.com/kcoleman731)

## 0.4.9

* Enabled auto correct and spell check in the chat message input toolbar.
  [Kevin Coleman](https://github.com/kcoleman731)

## 0.4.8

* Fixed UI bugs of Order status card and Rewards card
  [Jayashree Varpe](https://github.com/jayashree)

## 0.4.7

* Changed `BOTServiceManager` methods to pass errors instead of use assertions.
  [Kevin Coleman](https://github.com/kcoleman731)

## 0.4.6

* Fixes issues with how chat cards are laid out.
  [Kevin Coleman](https://github.com/kcoleman731)

## 0.4.5

* Moves all Layer authentication logic into the chat pod.
* Introduces the `BOTChat` interface to allow applications to configure and authenticate the pod.
  [Kevin Coleman](https://github.com/kcoleman731)

## 0.3.9

* New designs for List Picker.
  [Kevin Coleman](https://github.com/kcoleman731)

## 0.3.7

### Enhancements

* Preventing multi-selection bar buttons from staying highlighted.
  [Kevin Coleman](https://github.com/kcoleman731)

## 0.3.6

### Enhancements

* Changing product cell height.
  [Kevin Coleman](https://github.com/kcoleman731)

## 0.3.5

### Enhancements

* Added logic to handle `actions` for the guided list items.
  [Jayashree Varpe](https://github.com/jayashree)

## 0.3.4

### Bug Fixes

* Fixed issue that would cause `BOTMessageInputToolbar` to have transparent background in iOS10.
  [Kevin Coleman](https://github.com/kcoleman731)

## 0.3.3

### Bug Fixes

* Fixed issue that would cause `BOTOrderStatusCollectionViewCell` to show the wrong number of additional items.
  [Kevin Coleman](https://github.com/kcoleman731)

## 0.3.2

### Bug Fixes  

* The list icon now properly reflects the state of the alternate user input keyboard.
  [Taylor Halliday](https://github.com/tayhalla)
